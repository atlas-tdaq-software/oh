tdaq_package()

#remove_definitions(-DERS_NO_DEBUG)

tdaq_add_header_directory(oh PATTERN *.i)

tdaq_add_library(oh src/common/*.cxx src/core/*.cxx
  LINK_LIBRARIES is ipc omniORB4 omnithread owl)

tdaq_add_library(ohroot src/root/*.cxx 
  LINK_LIBRARIES oh ROOT::Hist ROOT::Graf ROOT::Gpad ROOT::RIO ROOT::Thread ROOT::MathCore ROOT::Matrix ROOT::Core Freetype)

tdaq_add_executable(oh_rm bin/oh_rm.cxx LINK_LIBRARIES oh cmdline)

tdaq_add_executable(oh_cmd bin/oh_cmd.cxx LINK_LIBRARIES oh cmdline)
tdaq_add_executable(oh_ls bin/oh_ls.cxx LINK_LIBRARIES oh cmdline)

tdaq_add_executable(oh_cp bin/oh_cp.cxx DEFINITIONS -DOH_COPY LINK_LIBRARIES oh cmdline RCInfo_ISINFO ohroot)
tdaq_add_executable(oh_mv bin/oh_cp.cxx DEFINITIONS -DOH_MOVE LINK_LIBRARIES oh cmdline RCInfo_ISINFO ohroot)

tdaq_add_executable(oh_data_test test/oh_data_test.cxx LINK_LIBRARIES oh cmdline)
tdaq_add_executable(oh_raw_provider examples/raw_provider/raw_provider.cxx LINK_LIBRARIES oh cmdline)
tdaq_add_executable(oh_multi_provider test/multi_provider.cxx LINK_LIBRARIES oh cmdline)
tdaq_add_executable(oh_test_provider test/test_provider.cxx LINK_LIBRARIES oh cmdline)
tdaq_add_executable(oh_test_receiver test/test_receiver.cxx LINK_LIBRARIES ohroot cmdline)
tdaq_add_executable(oh_test_iterator test/test_iterator.cxx LINK_LIBRARIES ohroot cmdline)

tdaq_add_executable(oh_root_provider examples/root_provider/root_provider.cxx LINK_LIBRARIES ohroot cmdline)
tdaq_add_executable(oh_graph_provider examples/root_provider/graph_provider.cxx LINK_LIBRARIES ohroot cmdline)
tdaq_add_executable(oh_root_receiver examples/root_receiver/root_receiver.cxx LINK_LIBRARIES ohroot cmdline)

tdaq_add_executable(oh_display 
  display/*.cxx
  INCLUDE_DIRECTORIES display
  LINK_LIBRARIES ohroot ROOT::Core ROOT::Matrix ROOT::HistPainter ROOT::X3d ROOT::GX11 ROOT::Gui ROOT::Graf3d ROOT::Gpad)

tdaq_add_is_schema(data/histogram.xml)

tdaq_add_executable(unit_test_is_conversion
	unit_test/is_conversion.cpp
    LINK_LIBRARIES ohroot Boost::unit_test_framework
)

tdaq_add_test(NAME oh::is_conversion_check POST_INSTALL COMMAND unit_test_is_conversion)

#include <string>
#include <is/infoany.h>
#include <is/infoT.h>
#include <oh/OHCommandSender.h>
#include <oh/OHUtil.h>
#include <oh/core/HistogramProvider.h>

void
OHCommandSender::sendCommand(	const std::string & server_name,
				const std::string & provider_name,
				const std::string & histogram_name, 
				const std::string & command ) 
{
    ISInfoAny histogram_info;
    std::string name = oh::util::create_info_name( server_name, provider_name, histogram_name );
    try
    {
    	dict_.getValue( name, histogram_info );
	histogram_info.sendCommand( command );
    }
    catch( daq::is::Exception & ex )
    {
    	throw daq::oh::ProviderNotFound( ERS_HERE, name, ex );
    }
}
			
void
OHCommandSender::sendCommand(	const std::string & server_name,
				const std::string & provider_name,
				const std::string & command ) 
{
    oh::HistogramProvider provider_info;
    std::string name = oh::util::create_provider_name( server_name, provider_name );
    try
    {
    	dict_.getValue( name, provider_info );
	provider_info.sendCommand( command );
    }
    catch( daq::is::Exception & ex )
    {
    	throw daq::oh::ProviderNotFound( ERS_HERE, name, ex );
    }
}

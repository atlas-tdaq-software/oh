/*
    OHSubscriber.cxx
    
    Fredrik Sandin, Oct 2001
    Barczyk Monika, Feb 2003
    Serguei Kolos, Jan 2004
*/

#include <oh/OHUtil.h>
#include <oh/OHSubscriber.h>

void 
OHSubscriber::is_value_callback( ISCallbackInfo * isc )
{
    if ( isc->reason() != ISInfo::Deleted ) 
    {
    	try {
            receiver_.readObject( *isc );
        }
        catch(daq::oh::InvalidObject & ex) {
            ers::debug(ex, 0);
        }
    }
}

void 
OHSubscriber::is_event_callback( ISCallbackEvent * ise )
{
    receiver_.objectChanged( ise->name(), ise->time(), ise->reason() );
}

/*!
    \param p A valid IPCPartition
    \param server Name of a valid OH server
    \param receiver Should be a user defined histogram receiver object derived
    from one of the OH receiver classes.
    \param serialise_callbacks defines whether callbacks are serialized or not
*/
OHSubscriber::OHSubscriber(	const IPCPartition & p,
				const std::string & server,
				OHReceiver & receiver,
				bool serialise_callbacks )
  : ISInfoReceiver( p, serialise_callbacks ),
    receiver_( receiver ),
    server_( server )
{
    ERS_DEBUG( 1, "Creating OHSubscriber for server " << server << " in the " << p.name( ) << " partition.");
}

/*!
    \param p A valid IPCPartition
    \param receiver Should be a user defined histogram receiver object derived
    from one of the OH receiver classes.
    \param serialise_callbacks defines whether callbacks are serialized or not
*/
OHSubscriber::OHSubscriber(	const IPCPartition & p,
				OHReceiver & receiver,
				bool serialise_callbacks )
  : ISInfoReceiver( p, serialise_callbacks ),
    receiver_( receiver )
{
    ERS_DEBUG( 1, "Creating OHSubscriber for server in the " << p.name( ) << " partition.");
}


/////////////////////////////////////////////////////////////////////////////
//
// Subscription functions
//
/////////////////////////////////////////////////////////////////////////////

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param name Full name of histogram which includes the IS server name, provider name and the histogram name itself
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const std::string & name, bool provide_histogram_value ) 
{
    try {
	if ( provide_histogram_value )
	    ISInfoReceiver::subscribe( name, &OHSubscriber::is_value_callback, this );
	else
	    ISInfoReceiver::subscribe( name, &OHSubscriber::is_event_callback, this );
    }
    catch( daq::is::Exception & ex ) {
    	throw daq::oh::Exception( ERS_HERE, ex.what() );
    }
    
    ERS_DEBUG( 1, "Subscription for histograms \"" << name << "\" was done" );
}

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param provider Name of histogram provider
    \param histoname Name of histogram
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const std::string & provider, 
			 const std::string & histoname, 
			 bool provide_histogram_value ) 
{
    subscribe( oh::util::create_info_name( server_, provider, histoname ), provide_histogram_value );
}

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param server Name of a valid OH server
    \param provider Name of histogram provider
    \param histoname Name of histogram
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const std::string & server,
			 const std::string & provider, 
			 const std::string & histoname, 
			 bool provide_histogram_value ) 
{
    subscribe( oh::util::create_info_name( server, provider, histoname ), provide_histogram_value );
}

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param server Name of a valid OH server
    \param regex Regular expression for both provider and the histogram names
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const std::string & server,
			 const OWLRegexp & regex,
			 bool provide_histogram_value ) 
{
    try {
	if ( provide_histogram_value )
	    ISInfoReceiver::subscribe( server, ISCriteria(regex.str()), &OHSubscriber::is_value_callback, this );
	else
	    ISInfoReceiver::subscribe( server, ISCriteria(regex.str()), &OHSubscriber::is_event_callback, this );
    }
    catch( daq::is::Exception & ex ) {
    	throw daq::oh::Exception( ERS_HERE, ex.what() );
    }

    ERS_DEBUG( 1, "Subscription for histograms with regular expression \"" << regex.str() << "\" was done" );
}

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param provider Regular expression for the histogram providers names
    \param histoname Regular expression for the histograms names
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const OWLRegexp & provider,
			 const OWLRegexp & histoname, 
			 bool provide_histogram_value ) 
{
    OWLRegexp regex( oh::util::create_regex( provider.str(), histoname.str() ) );
    subscribe( server_, regex, provide_histogram_value );
}

/*!
    It is not allowed to subscribe more than once for each combination of the
    selection parameters.
    \param server Name of a valid OH server
    \param provider Regular expression for the histogram providers names
    \param histoname Regular expression for the histograms names
    \param provide_histogram_value Subscriber with either receive the value of the
    changed histogram or just a name of it.
*/
void
OHSubscriber::subscribe( const std::string & server,
			 const OWLRegexp & provider,
			 const OWLRegexp & histoname, 
			 bool provide_histogram_value ) 
{
    OWLRegexp regex( oh::util::create_regex( provider.str(), histoname.str() ) );
    subscribe( server, regex, provide_histogram_value );
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Unsubscribe functions
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
    Removes subscription.
    This function requires the same parameter value as been used for its subscibe counterpart.
    \param name Full name of histogram which includes the IS server name, provider name and the histogram name itself
*/
void
OHSubscriber::unsubscribe( const std::string & name ) 
{
    try {
	ISInfoReceiver::unsubscribe( name, true );
    }
    catch( daq::is::Exception & ex ) {
    	throw daq::oh::Exception( ERS_HERE, ex.what() );
    }
    ERS_DEBUG( 1, "Unsubscribe for histograms \"" << name << "\" was done " );
}

/*!
    Removes subscription.
    \param server Name of a valid OH server
    \param regex Regular expression for both provider and the histogram names
*/
void
OHSubscriber::unsubscribe( const std::string & server,
			   const OWLRegexp & regex ) 
{
    try {
	ISInfoReceiver::unsubscribe( server, ISCriteria(regex.str()), true );
    }
    catch( daq::is::Exception & ex ) {
    	throw daq::oh::Exception( ERS_HERE, ex.what() );
    }
    ERS_DEBUG( 1, "Unsubscribe for histograms with regular expression \"" << regex.str() << "\" was done" );
}

/*!
    Removes subscription.
    This function requires the same parameter values as been used for the subscibe counterpart.
    \param provider Name of histogram provider
    \param histoname Name of histogram
*/
void
OHSubscriber::unsubscribe( const std::string & provider, const std::string & histoname ) 
{
    unsubscribe( oh::util::create_info_name( server_, provider, histoname ) );
}

/*!
    Removes subscription.
    \param server Name of a valid OH server
    \param provider Name of histogram provider
    \param histoname Name of histogram
*/
void
OHSubscriber::unsubscribe( const std::string & server,
			   const std::string & provider,
                           const std::string & histoname ) 
{
    unsubscribe( oh::util::create_info_name( server, provider, histoname ) );
}

/*!
    Removes subscription.
    \param provider Regular expression for the histogram providers names
    \param histoname Regular expression for the histograms names
*/
void
OHSubscriber::unsubscribe( const OWLRegexp & provider, const OWLRegexp & histoname ) 
{
    OWLRegexp regex( oh::util::create_regex( provider.str(), histoname.str() ) );
    unsubscribe( server_, regex );
}

/*!
    Removes subscription.
    \param server Name of a valid OH server
    \param provider Regular expression for the histogram providers names
    \param histoname Regular expression for the histograms names
*/
void
OHSubscriber::unsubscribe( const std::string & server,
			   const OWLRegexp & provider,
                           const OWLRegexp & histoname ) 
{
    OWLRegexp regex( oh::util::create_regex( provider.str(), histoname.str() ) );
    unsubscribe( server, regex );
}

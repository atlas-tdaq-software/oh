/*
    OHProviderIterator.cxx
    
    OH histogram provider iterator implementation.
    Fredrik Sandin, Oct 2001
*/

#include <assert.h>

#include <is/infoT.h>
#include <is/infodictionary.h>

#include <oh/OHProviderIterator.h>
#include <oh/OHUtil.h>
#include <oh/core/HistogramProvider.h>

/*!
    Immediately after construction the "current provider" is undefined and
    the iterator must be advanced with one of the ++ operators.
    \param p A valid IPCPartition
    \param server Name of a valid OH server
*/
OHProviderIterator::OHProviderIterator( const IPCPartition & p,
                                        const std::string & server,
                                        const std::string & provider_mask )
  : ISInfoIterator( p, server,
      oh::HistogramProvider::type() && oh::util::create_provider_name( std::string(), std::string( provider_mask ) ) )
{
}

/*!
    \return The name of the current provider or "" if the current provider is undefined.
*/
OHProviderIterator::operator std::string( ) {
    return name();
}

/*!
 *     \return The name of the current provider or "" if the current provider is undefined.
 *     */
std::string
OHProviderIterator::name( ) {
    std::string name = this->ISInfoIterator::name();
    return oh::util::get_provider_name( name );
}

/*!
    \return true if the carrent provider exists, false otherwise
*/
bool
OHProviderIterator::isActive( ) {
    oh::HistogramProvider providerinfo;
    providerinfo.active = true;
    ISInfoDictionary dict( partition() );
    try
    {
        dict.getValue( this->ISInfoIterator::name(), providerinfo );
        if ( providerinfo.active && providerinfo.providerExist() )
        {
            return true;
        }
        else
        {
	    return false;
        }
    }
    catch( daq::is::Exception & ex )
    {
	ERS_DEBUG( 1, ex << "was received while trying to verify existence of the '" << name() << "' provider" );
        return false;
    }
}

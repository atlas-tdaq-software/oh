/*
 OHRootProvider.cxx

 Fredrik Sandin, Oct 2001
 Monika Barczyk, Sept 2002
 Sergei Kolos,   Nov 2004
 Herbert Kaiser, Aug 2006
 */

#include <oh/OHRootProvider.h>
#include <oh/OHRootMutex.h>
#include <oh/core/EfficiencyData.h>
#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/GraphData.h>
#include <oh/core/HistogramTraits.h>
#include <oh/exceptions.h>

#include <TEfficiency.h>
#include <TH2.h>
#include <TH3.h>
#include <THashList.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <TObjString.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TGraphBentErrors.h>
#include <TGraph2DErrors.h>

namespace internal {

    template<class T, class A>
    void copy_axis(T & is_object, oh::Axis::ID id, A * axis) {
        const TArrayD * rootp = axis->GetXbins();

        is_object.set_axis_title(id, std::string(axis->GetTitle()));
        if (rootp->GetSize() > 0) {
            // Variable width bins
            is_object.set_axis_range(id, rootp->GetArray(), rootp->GetSize());
        }
        else {
            // Fixed width bins
            is_object.set_axis_range(id, axis->GetBinLowEdge(1),
                    axis->GetBinWidth(1), axis->GetNbins());
        }

        THashList * hl = axis->GetLabels();
        if (hl) {
            std::vector<int> & indices = is_object.indices(id);
            indices.clear();
            indices.reserve(hl->GetEntries());
            std::vector<std::string> & labels = is_object.labels(id);
            labels.clear();
            labels.reserve(hl->GetEntries());
            TIter next(hl);
            TObjString * obj;
            while ((obj = (TObjString*) next()) != 0) {
                indices.push_back((Int_t)obj->GetUniqueID());
                labels.push_back(obj->GetName());
            }
        }
    }

    template<class T>
    void base_convert(oh::IHistogram & is_object, const T & histogram) {
        is_object.set_name(histogram.GetName());
        is_object.set_title(histogram.GetTitle());
        is_object.set_entries((unsigned int) histogram.GetEntries());

        switch (histogram.GetDimension()) {
            case 1:
                is_object.set_dimension(oh::Histogram::D1);
                break;
            case 2:
                is_object.set_dimension(oh::Histogram::D2);
                break;
            case 3:
                is_object.set_dimension(oh::Histogram::D3);
                break;
            default:
                ERS_ASSERT_MSG(0, "Bad histogram object has been provided");
                break;
        }

        if (histogram.GetXaxis())
            copy_axis(is_object, oh::Axis::X, histogram.GetXaxis());
        if (histogram.GetYaxis())
            copy_axis(is_object, oh::Axis::Y, histogram.GetYaxis());
        if (histogram.GetZaxis())
            copy_axis(is_object, oh::Axis::Z, histogram.GetZaxis());

        // Set errors without copying
        const TArrayD * errors = histogram.GetSumw2();
        if (errors) {
            is_object.set_errors(errors->fArray, (unsigned int) errors->fN);
        }

        is_object.statistics.resize(oh::HistogramTraits<T>::StatsSize);
        histogram.GetStats(&(is_object.statistics[0]));
    }

    template<class T>
    oh::Histogram *
    convert(const T & histogram,
            const std::vector<std::pair<std::string, std::string> > & ann) {
        auto is_object = new typename oh::HistogramTraits<T>::ISType();
        is_object->set_annotations(ann);

        base_convert(*is_object, histogram);

        // Set bin content without copying
        is_object->set_bins_array(histogram.GetArray(),
                (unsigned int) histogram.GetSize());

        return is_object;
    }

    template<typename T>
    oh::Histogram *
    convert_profile(const T & profile,
            const std::vector<std::pair<std::string, std::string> > & ann) {
        auto is_object = new typename oh::HistogramTraits<T>::ISType();
        is_object->set_annotations(ann);

        base_convert(*is_object, profile);

        // Set bin content without copying
        is_object->set_bins_array(profile.GetArray(),
                (unsigned int) profile.GetSize());

        is_object->bin_entries.resize(profile.GetSize());
        for (int i = 0; i < profile.GetSize(); i++)
            is_object->bin_entries[i] = profile.GetBinEntries(i);

        return is_object;
    }

    oh::Histogram *
    convert(const TProfile & profile,
            const std::vector<std::pair<std::string, std::string> > & ann) {
        return convert_profile(profile, ann);
    }

    oh::Histogram *
    convert(const TProfile2D & profile,
            const std::vector<std::pair<std::string, std::string> > & ann) {
        return convert_profile(profile, ann);
    }

    oh::Histogram *
    convert(const TProfile3D & profile,
            const std::vector<std::pair<std::string, std::string> > & ann) {
        return convert_profile(profile, ann);
    }

}

#define ROOT_HISTOGRAM_TYPES(decl)	\
decl(TH1C)	decl ## _SEPARATOR	\
decl(TH2C)	decl ## _SEPARATOR	\
decl(TH3C)	decl ## _SEPARATOR	\
decl(TH1S)	decl ## _SEPARATOR	\
decl(TH2S)	decl ## _SEPARATOR	\
decl(TH3S)	decl ## _SEPARATOR	\
decl(TH1I)	decl ## _SEPARATOR	\
decl(TH2I)	decl ## _SEPARATOR	\
decl(TH3I)	decl ## _SEPARATOR	\
decl(TH1F)	decl ## _SEPARATOR	\
decl(TH2F)	decl ## _SEPARATOR	\
decl(TH3F)	decl ## _SEPARATOR	\
decl(TProfile)	decl ## _SEPARATOR	/* must precede the TH1D since TProfile inherits from TH1D */ \
decl(TH1D)	decl ## _SEPARATOR	\
decl(TProfile2D)decl ## _SEPARATOR	/* must precede the TH2D since TProfile2D inherits from TH2D */ \
decl(TH2D)	decl ## _SEPARATOR	\
decl(TProfile3D)decl ## _SEPARATOR	/* must precede the TH3D since TProfile3D inherits from TH3D */ \
decl(TH3D)

#define CONVERT(x)		if ( histogram.InheritsFrom( x::Class() ) ) \
			            h.reset( internal::convert( static_cast<const x &>( histogram ), ann ) );
#define CONVERT_SEPARATOR	else

#define BASE_CONVERT(x)         if ( histogram.InheritsFrom( x::Class() ) ) \
                                    internal::base_convert( is_object, static_cast<const x &>( histogram ) );
#define BASE_CONVERT_SEPARATOR  else

namespace internal {
    void convert(const TEfficiency & eff, oh::EfficiencyData & is_object)
    {
        struct ProtectedAccessor: public TEfficiency {
            const std::vector<std::pair<double, double>> getBetaBinParams() const {
                return this->fBeta_bin_params;
            }
        };

        is_object.set_name(eff.GetName());
        is_object.set_title(eff.GetTitle());
        is_object.betaAlpha = eff.GetBetaAlpha();
        is_object.betaBeta = eff.GetBetaBeta();
        is_object.confidenceLevel = eff.GetConfidenceLevel();
        is_object.weight = eff.GetWeight();
        auto & v = reinterpret_cast<const ProtectedAccessor&>(eff).getBetaBinParams();
        is_object.betaAlphaPerBin.reserve(v.size());
        is_object.betaBetaPerBin.reserve(v.size());
        for (auto pair : v) {
            is_object.betaAlphaPerBin.push_back(pair.first);
            is_object.betaBetaPerBin.push_back(pair.second);
        }

        const TH1 * total_h = eff.GetTotalHistogram();
        const TH1 * passed_h = eff.GetPassedHistogram();

        auto translate = [](const TH1 & histogram, oh::HistogramData<double> & is_object) {
            ROOT_HISTOGRAM_TYPES(BASE_CONVERT)
        };
        translate(*total_h, is_object.total);
        translate(*passed_h, is_object.passed);

        const TArrayD * arrd = dynamic_cast<const TArrayD*>(total_h);
        if ( arrd ) {
            // Set bin content without copying
            is_object.total.set_bins_array(arrd->GetArray(), (unsigned int)arrd->GetSize());
        } else {
            const TArray * arr = dynamic_cast<const TArray*>(total_h);
            ERS_ASSERT(arr);
            double * bins = new double[arr->GetSize()];
            for (int i = 0; i < arr->GetSize(); ++i) {
                bins[i] = arr->GetAt(i);
            }
            is_object.total.set_bins_array(bins, (unsigned int)arr->GetSize(), true);
        }

        arrd = dynamic_cast<const TArrayD*>(passed_h);
        if ( arrd ) {
            // Set bin content without copying
            is_object.passed.set_bins_array(arrd->GetArray(), (unsigned int)arrd->GetSize());
        } else {
            const TArray * arr = dynamic_cast<const TArray*>(passed_h);
            ERS_ASSERT(arr);
            double * bins = new double[arr->GetSize()];
            for (int i = 0; i < arr->GetSize(); ++i) {
                bins[i] = arr->GetAt(i);
            }
            is_object.passed.set_bins_array(bins, (unsigned int)arr->GetSize(), true);
        }
    }

    void convert(const TGraph & graph, oh::GraphData & is_object)
    {
        is_object.set_entries(graph.GetN());
        is_object.set_name(graph.GetName());
        is_object.set_title(graph.GetTitle());
        internal::copy_axis(is_object, oh::Axis::X, graph.GetXaxis());
        internal::copy_axis(is_object, oh::Axis::Y, graph.GetYaxis());
        std::vector<const double*> points(2);
        points[0] = graph.GetX();
        points[1] = graph.GetY();
        is_object.set_points(points);

        std::vector<const double*> errorArrays;
        if (graph.InheritsFrom(TGraphErrors::Class())) {
            is_object.set_error_style(oh::Graph::Symmetric);
            errorArrays.resize(2);
            errorArrays[0] = static_cast<const TGraphErrors*>(&graph)->GetEX();
            errorArrays[1] = static_cast<const TGraphErrors*>(&graph)->GetEY();
        }
        else if (graph.InheritsFrom(TGraphAsymmErrors::Class())) {
            is_object.set_error_style(oh::Graph::Asymmetric);
            errorArrays.resize(4);
            errorArrays[0] =
                    static_cast<const TGraphAsymmErrors*>(&graph)->GetEXlow();
            errorArrays[1] =
                    static_cast<const TGraphAsymmErrors*>(&graph)->GetEXhigh();
            errorArrays[2] =
                    static_cast<const TGraphAsymmErrors*>(&graph)->GetEYlow();
            errorArrays[3] =
                    static_cast<const TGraphAsymmErrors*>(&graph)->GetEYhigh();
        }
        else if (graph.InheritsFrom(TGraphBentErrors::Class())) {
            is_object.set_error_style(oh::Graph::AsymBent);
            errorArrays.resize(8);
            errorArrays[0] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEXlow();
            errorArrays[1] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEXhigh();
            errorArrays[2] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEYlow();
            errorArrays[3] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEYhigh();
            errorArrays[4] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEXlowd();
            errorArrays[5] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEXhighd();
            errorArrays[6] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEYlowd();
            errorArrays[7] =
                    static_cast<const TGraphBentErrors*>(&graph)->GetEYhighd();
        }
        else {
            is_object.set_error_style(oh::Graph::None);
        }

        // data is copied to the oh::Graph
        is_object.set_errors(errorArrays);
    }

    void convert(const TGraph2D & graph, oh::Graph2DData & is_object)
    {
        is_object.set_entries(graph.GetN());
        is_object.set_name(graph.GetName());
        is_object.set_title(graph.GetTitle());
        internal::copy_axis(is_object, oh::Axis::X, graph.GetXaxis());
        internal::copy_axis(is_object, oh::Axis::Y, graph.GetYaxis());
        internal::copy_axis(is_object, oh::Axis::Z, graph.GetZaxis());
        std::vector<const double*> points(3);
        points[0] = graph.GetX();
        points[1] = graph.GetY();
        points[2] = graph.GetZ();
        is_object.set_points(points);

        std::vector<const double*> errorArrays;
        if (graph.InheritsFrom(TGraph2DErrors::Class())) {
            is_object.set_error_style(oh::Graph2D::Symmetric);
            errorArrays.resize(3);
            errorArrays[0] =
                    static_cast<const TGraph2DErrors*>(&graph)->GetEX();
            errorArrays[1] =
                    static_cast<const TGraph2DErrors*>(&graph)->GetEY();
            errorArrays[2] =
                    static_cast<const TGraph2DErrors*>(&graph)->GetEZ();
        }
        else {
            is_object.set_error_style(oh::Graph2D::None);
        }

        // data is copied to the oh::Graph
        is_object.set_errors(errorArrays);
    }
}

/*!
 \param p A valid IPCPartition
 \param server Name of a valid OH server
 \param name Name of the provider, user is responsible for specifying a
 unique name. Should only contain characters from [a-z], [A-Z], [0-9] and '_'.
 \param lst User defined command listener object.
 */
OHRootProvider::OHRootProvider(const IPCPartition & p,
        const std::string & server, const std::string & name,
        OHCommandListener * lst) :
        OHProvider(p, server, name, lst) {
    ;
}

std::unique_ptr<oh::Histogram>
OHRootProvider::convert(const TH1& histogram,
        const std::vector<std::pair<std::string, std::string>>& ann)
{
    std::unique_ptr<oh::Histogram> h;
    {
        std::unique_lock lock(OHRootMutex::getMutex());
        ROOT_HISTOGRAM_TYPES(CONVERT)
    }
    return h;
}

/*!
 \param histogram A valid ROOT histogram
 \param name A name describing the histogram. Should only contain characters
 from [a-z], [A-Z], [0-9] and '_'.
 \param update Tells wether to keep the previous version of this histogram (by default false)
 \return OHRootProvider::CommFailure if a communication error occurred,
 OHRootProvider::InvalidArgument if one or more arguments is invalid,
 else OHRootProvider::Success.
 */
void OHRootProvider::publish(const TH1 & histogram, const std::string & name,
        int tag, const std::vector<std::pair<std::string, std::string> > & ann)
{
    std::unique_ptr<oh::Histogram> h = convert(histogram, ann);
    ERS_ASSERT_MSG(h, "Bad histogram object has been provided");

    OHProvider::publish(*h, name, tag);
}

void
OHRootProvider::publish(const TEfficiency & eff, const std::string & name, int tag,
        const std::vector<std::pair<std::string, std::string> > & ann)
{
    oh::EfficiencyData is_object;
    {
        std::unique_lock lock(OHRootMutex::getMutex());
        internal::convert(eff, is_object);
    }

    is_object.set_annotations(ann);
    OHProvider::publish(is_object, name, tag);
}

/* *************************************************************
 * Part for Graph support
 * ************************************************************* */
void OHRootProvider::publish(const TGraph & graph, const std::string & name,
        int tag, const std::vector<std::pair<std::string, std::string> > & ann)
{
    oh::GraphData is_object;
    {
        std::unique_lock lock(OHRootMutex::getMutex());
        internal::convert(graph, is_object);
    }

    is_object.set_annotations(ann);

    OHProvider::publish(is_object, name, tag);
}

void OHRootProvider::publish(const TGraph2D & graph, const std::string & name,
        int tag, const std::vector<std::pair<std::string, std::string> > & ann)
{
    oh::Graph2DData is_object;
    {
        std::unique_lock lock(OHRootMutex::getMutex());
        internal::convert(graph, is_object);
    }

    is_object.set_annotations(ann);

    OHProvider::publish(is_object, name, tag);
}

// EOF

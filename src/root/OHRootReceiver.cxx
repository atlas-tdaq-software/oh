/*
    OHRootReceiver.cxx
    
    Fredrik Sandin, Oct 2001
    Sergei Kolos,   Nov 2004
    Herbert Kaiser, Aug 2006
*/

#include <type_traits>

#include <oh/OHRootReceiver.h>
#include <oh/OHUtil.h>
#include <oh/core/DataTypes.h>
#include <oh/core/EfficiencyData.h>
#include <oh/core/GraphData.h>
#include <oh/core/HistogramTraits.h>
#include <TEfficiency.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TGraphBentErrors.h>
#include <TGraph2DErrors.h>

namespace internal
{
  template <class T, int D> struct RootObject;

  template <> struct RootObject<oh::HistogramData<char>,1>	{ typedef TH1C		Type; };
  template <> struct RootObject<oh::HistogramData<short>,1>	{ typedef TH1S		Type; };
  template <> struct RootObject<oh::HistogramData<int>,1>	{ typedef TH1I		Type; };
  template <> struct RootObject<oh::HistogramData<float>,1>	{ typedef TH1F		Type; };
  template <> struct RootObject<oh::HistogramData<double>,1>	{ typedef TH1D		Type; };
  template <> struct RootObject<oh::ProfileData,1>		{ typedef TProfile 	Type; };
  template <> struct RootObject<oh::GraphData,1>		{ typedef TGraph 	Type; };

  template <> struct RootObject<oh::HistogramData<char>,2>	{ typedef TH2C		Type; };
  template <> struct RootObject<oh::HistogramData<short>,2>	{ typedef TH2S		Type; };
  template <> struct RootObject<oh::HistogramData<int>,2>	{ typedef TH2I		Type; };
  template <> struct RootObject<oh::HistogramData<float>,2>	{ typedef TH2F		Type; };
  template <> struct RootObject<oh::HistogramData<double>,2>	{ typedef TH2D		Type; };
  template <> struct RootObject<oh::ProfileData,2>		{ typedef TProfile2D 	Type; };
  template <> struct RootObject<oh::Graph2DData,2>		{ typedef TGraph2D 	Type; };

  template <> struct RootObject<oh::HistogramData<char>,3>	{ typedef TH3C		Type; };
  template <> struct RootObject<oh::HistogramData<short>,3>	{ typedef TH3S		Type; };
  template <> struct RootObject<oh::HistogramData<int>,3>	{ typedef TH3I		Type; };
  template <> struct RootObject<oh::HistogramData<float>,3>	{ typedef TH3F		Type; };
  template <> struct RootObject<oh::HistogramData<double>,3>	{ typedef TH3D		Type; };
  template <> struct RootObject<oh::ProfileData,3>              { typedef TProfile3D    Type; };
  template <> struct RootObject<oh::EfficiencyData,1>           { typedef TEfficiency   Type; };

  template <class T> struct OHObject;
  template <> struct OHObject<oh::HistogramData<char> >		{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::HistogramData<short> >	{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::HistogramData<int> >		{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::HistogramData<float> >	{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::HistogramData<double> >	{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::ProfileData>			{ typedef OHRootHistogram  Type; };
  template <> struct OHObject<oh::GraphData>			{ typedef OHRootGraph 	   Type; };
  template <> struct OHObject<oh::Graph2DData>			{ typedef OHRootGraph2D	   Type; };
  template <> struct OHObject<oh::EfficiencyData>               { typedef OHRootEfficiency Type; };

  ////////////////////////////////////////////////////////////////////////////////////////////////
  // Generic functions to handle conversion from IS to Root axes
  ////////////////////////////////////////////////////////////////////////////////////////////////
  template <class I>
  void copy_axis( const I & gdata, oh::Axis::ID ax, TAxis * axis )
  {
    int temp = gdata.get_bin_count( ax );
    unsigned int nbins = ( temp != -1 ? (unsigned int)temp : 1 );

    axis -> SetTitle(gdata.get_axis_title( ax ).c_str( ) );

    if ( gdata.get_axis_type( ax ) == oh::Axis::Fixed )
    {
	double low, binwidth;
	gdata.get_axis_range( ax, low, binwidth );
	axis -> Set( nbins, low, low + binwidth * nbins );
    }
    else if ( gdata.get_axis_type( ax ) == oh::Axis::Variable )
	axis -> Set( nbins,gdata.get_axis_range( ax ).size()
	        ? &(gdata.get_axis_range( ax )[0]) : 0 );
    else
	throw daq::oh::InvalidObject( ERS_HERE, "Invalid axis type" );

    const std::vector<int> & indices = gdata.get_indices( ax );
    const std::vector<std::string> & labels = gdata.get_labels( ax );

    for ( size_t i = 0; i < indices.size(); i++ )
    {
	axis -> SetBinLabel( indices[i], labels[i].c_str() );
    }
  }

  template <class T, class I>
  void copy_axes( T * res, const I & gdata, unsigned int number )
  {
    copy_axis( gdata, oh::Axis::X, res->GetXaxis() );
    if ( number == 1 ) return; 
    copy_axis( gdata, oh::Axis::Y, res->GetYaxis() ); 
    if ( number == 2 ) return; 
    copy_axis( gdata, oh::Axis::Z, res->GetZaxis() ); 
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  // Specific functions for Graph axes - TGraph does not have ZAxis
  ////////////////////////////////////////////////////////////////////////////////////////////////
  void copy_axes( TGraph * res, const oh::GraphData& gdata )
  {
      // Save and then restore the directory status.
      // If the status is not set to false then the histogram that is used
      // internally by the TGraph object for drawing axis will be attached
      // to the current directory, which will produce a warning about possible memory leak.
      // This looks like a bug in ROOT's TGraph::GetHistogram() function
    bool s = TH1::AddDirectoryStatus();
    TH1::AddDirectory(false);

    copy_axis( gdata, oh::Axis::X, res->GetXaxis() ); 
    copy_axis( gdata, oh::Axis::Y, res->GetYaxis() ); 

    TH1::AddDirectory(s);
  }
  
  ///////////////////////////////////////////////////////////////////////////////////
  // Histogram conversion
  ///////////////////////////////////////////////////////////////////////////////////

  template <int D, class T>
  typename RootObject<T,D>::Type *
  convert_histogram( const std::string & name, const T & histogram )
  {
    typename RootObject<T,D>::Type * result = new typename RootObject<T,D>::Type;
    result -> Adopt( histogram.get_bins_size(), histogram.get_bins_array( true ) );
    if (oh::HistogramTraits<typename RootObject<T,D>::Type>::StatsSize
            == histogram.statistics.size()) {
        result -> PutStats( (double *)&histogram.statistics[0] );
    }
    
    //
    // Reset internal arrays according to the number of bins
    //
    unsigned int  tbins = ( histogram.get_bin_count( oh::Axis::X ) + 2 ) *
    			  ( histogram.get_bin_count( oh::Axis::Y ) + 2 ) *
			  ( histogram.get_bin_count( oh::Axis::Z ) + 2 );
                          
    result -> SetBinsLength( tbins );
    
    // copy the axes
    copy_axes( result, histogram, histogram.get_axes_number() );

    result -> SetDirectory( 0 );
    result -> SetName( name.c_str() );
    result -> SetTitle( histogram.get_title().c_str() );
    
    result -> SetEntries( histogram.get_entries( ) );   
    
    //
    // TODO: Could this be done without copying?
    //
    if ( histogram.get_errors() )
    	result -> GetSumw2() -> Set( tbins, histogram.get_errors() );
    else
    	result -> GetSumw2() -> Set( 0 );

    return result; 
  }

  template <class T>
  TH1 *
  convert( const std::string & name, const oh::HistogramData<T> & data )
  {
    if ( data.get_dimension() == oh::Histogram::D1 ) {
        return convert_histogram<1>( name, data );
    }
    if ( data.get_dimension() == oh::Histogram::D2 ) {
        return convert_histogram<2>( name, data );
    }
    if ( data.get_dimension() == oh::Histogram::D3 ) {
        return convert_histogram<3>( name, data );
    }
    throw daq::oh::InvalidObject( ERS_HERE, "Profile has wrong dimension" );
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Profile conversion
  ///////////////////////////////////////////////////////////////////////////////////

  template<class T>
  struct ProtectedAccessor: public T {
    void setBinEntries(const std::vector<double> &v) {
        this->fBinEntries.Set(v.size(), &v[0]);
    }
  };

  TH1 *
  convert( const std::string & name, const oh::ProfileData & pdata )
  {
    if ( pdata.get_dimension() == oh::Histogram::D1 ) {
        auto p = convert_histogram<1>( name, pdata );
        reinterpret_cast<ProtectedAccessor<std::remove_reference<decltype(*p)>::type>*>(p)
                  -> setBinEntries( pdata.bin_entries );
        return p;
    }
    if ( pdata.get_dimension() == oh::Histogram::D2 ) {
        auto p = convert_histogram<2>( name, pdata );
        reinterpret_cast<ProtectedAccessor<std::remove_reference<decltype(*p)>::type>*>(p)
                  -> setBinEntries( pdata.bin_entries );
        return p;
    }
    if ( pdata.get_dimension() == oh::Histogram::D3 ) {
        auto p = convert_histogram<3>( name, pdata );
        reinterpret_cast<ProtectedAccessor<std::remove_reference<decltype(*p)>::type>*>(p)
                  -> setBinEntries( pdata.bin_entries );
        return p;
    }
    throw daq::oh::InvalidObject( ERS_HERE, "Profile has wrong dimension" );
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Efficiency conversion
  ///////////////////////////////////////////////////////////////////////////////////
  TEfficiency *
  convert( const std::string& name, const oh::EfficiencyData & edata )
  {
      TH1 * total = convert( edata.total.get_name(), edata.total );
      TH1 * passed = convert( edata.passed.get_name(), edata.passed );
      TEfficiency * eff = new TEfficiency(*passed, *total);
      eff->SetBetaAlpha(edata.betaAlpha);
      eff->SetBetaBeta(edata.betaBeta);
      eff->SetConfidenceLevel(edata.confidenceLevel);
      eff->SetWeight(edata.weight);
      if (edata.betaAlphaPerBin.size() == edata.betaBetaPerBin.size()) {
          for (uint32_t i = 0; i < edata.betaAlphaPerBin.size(); ++i ) {
              eff->SetBetaBinParameters(i, edata.betaAlphaPerBin[i], edata.betaBetaPerBin[i]);
          }
      }
      eff->SetDirectory(0);
      eff->SetNameTitle(name.c_str(), edata.get_title().c_str());
      return eff;
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Graph conversion
  ///////////////////////////////////////////////////////////////////////////////////
  TGraph *
  convert( const std::string& name, const oh::GraphData & gdata )
  {
    std::vector<const double*> data;
    gdata.get_data(data);

    TGraph * graph;
    switch( gdata.get_error_style() )
    {
    	case oh::Graph::None:
	    graph = new TGraph( gdata.get_entries(), (double*)data[0], (double*)data[1] );
            break;
    	case oh::Graph::Symmetric:
	    graph = new TGraphErrors(gdata.get_entries(),
	            (double*)data[0], (double*)data[1], (double*)data[2], (double*)data[3]);
            break;
    	case oh::Graph::Asymmetric:
	    graph = new TGraphAsymmErrors(gdata.get_entries(),
	            (double*)data[0], (double*)data[1], (double*)data[2],
	            (double*)data[3], (double*)data[4], (double*)data[5]);
            break;
    	case oh::Graph::AsymBent:
	    graph = new TGraphBentErrors(gdata.get_entries(),
	            (double*)data[0], (double*)data[1], (double*)data[2],
                    (double*)data[3], (double*)data[4], (double*)data[5],
                    (double*)data[6], (double*)data[7], (double*)data[8],
                    (double*)data[9]);
            break;
    	default:
	    throw daq::oh::InvalidObject( ERS_HERE, "graph has wrong errorstyle" );
    }

    graph -> UseCurrentStyle();
    graph -> SetName( name.c_str() );
    graph -> SetTitle( gdata.get_title().c_str() );

    copy_axes( graph, gdata );

    return graph;
  }

  TGraph2D *
  convert( const std::string& name, const oh::Graph2DData & gdata )
  {    
    std::vector<const double*> data;
    gdata.get_data(data);
    
    TGraph2D * graph;
    switch( gdata.get_error_style() )
    {
    	case oh::Graph2D::None:
	    graph = new TGraph2D( gdata.get_entries(),
	            (double*)data[0], (double*)data[1], (double*)data[2] );
            break;
    	case oh::Graph2D::Symmetric:
	    graph = new TGraph2DErrors( gdata.get_entries(),
	            (double*)data[0], (double*)data[1], (double*)data[2],
            	    (double*)data[3], (double*)data[4], (double*)data[5] );
            break;
	default:
	    throw daq::oh::InvalidObject( ERS_HERE, "graph has wrong errorstyle" );
    }

    graph -> SetDirectory( 0 );
    graph -> UseCurrentStyle();
    graph -> SetName( name.c_str() );
    graph -> SetTitle( gdata.get_title().c_str() );

    copy_axes( graph, gdata, 3 );

    return graph;
  }  

  ////////////////////////////////////////////////////////////////////////////////////////////////
  // Generic functions to handle conversion from IS to Root objects
  ////////////////////////////////////////////////////////////////////////////////////////////////
  template <class H, class T>
  void
  oh_to_root( const std::string & name, H & oh_object, T & is_data )
  {
    oh_object.getObject().reset( convert( name, is_data ) );
    oh_object.time = is_data.time();
    oh_object.tag = is_data.tag();
    is_data.get_annotations( oh_object.annotations );
  }

  template <class T>
  void
  receive( OHRootReceiver * receiver, const std::string & name, T & is_data )
  {    
    typename OHObject<T>::Type oh_object;
    oh_to_root( name, oh_object, is_data );
    receiver -> receive( oh_object );
  }

  template <class T>
  void
  receive( OHRootReceiver * receiver, const std::string & name, std::vector<T> & is_data )
  {
    std::vector<typename OHObject<T>::Type*> oh_objects( is_data.size( ) );
    
    for ( unsigned int i = 0; i < oh_objects.size( ); i++ )
    {
	oh_objects[i] = new typename OHObject<T>::Type;
	oh_to_root( name, *oh_objects[i], is_data[i] );
    }
    
    receiver -> receive( oh_objects );
    
    for ( unsigned int i = 0; i < oh_objects.size( ); i++ )
    	delete oh_objects[i];
  }
  
  template <class T>
  typename OHObject<T>::Type
  get_object( ISInfoAny & infoany, const std::string & is_name )
  {    
    T is_data;
    infoany >>= is_data;
    typename OHObject<T>::Type oh_object;
    oh_to_root( is_name, oh_object, is_data );
    return oh_object;
  }

  template <class T>
  OHRootObject
  get_base_object( ISInfoAny & infoany, const std::string & is_name )
  {    
    T is_data;
    infoany >>= is_data;
    OHRootObject oh_object;
    oh_to_root( is_name, oh_object, is_data );
    return oh_object;
  }
} // unnamed namespace


#include <oh/core/DataTypes.h>

#define OH_DECLARE_RECEIVE_FUNCTION(xx)	\
    void	\
    OHRootReceiver::receive_( const std::string & name, xx & is_object ) {	\
	internal::receive( this, name, is_object ); \
    }	\
	\
    void	\
    OHRootReceiver::receive_( const std::string & name, std::vector<xx> & is_set ) {	\
	internal::receive( this, name, is_set );	\
    }

#define OH_READ_PRESERVING_TYPE(xx)	\
    if ( info.type() == xx::type() )	\
	return internal::get_object<xx>( info, name );

#define OH_READ_AS_ROOT_OBJECT(xx)	\
    if ( info.type() == xx::type() )	\
	return internal::get_base_object<xx>( info, name );


FOR_OH_ALL_TYPES( OH_DECLARE_RECEIVE_FUNCTION )

///////////////////////////////////////////////////////////////////////////////////////////////
// public part
///////////////////////////////////////////////////////////////////////////////////////////////
namespace {
    void readAny(const IPCPartition & partition, const std::string & name, int tag, ISInfoAny & info) {
        ISInfoDictionary dict( partition );
        try {
            if ( tag == -1 )
                dict.getValue( name, info );
            else
                dict.getValue( name, tag, info );
        }
        catch( daq::is::Exception & ex ) {
            throw daq::oh::ObjectNotFound( ERS_HERE, oh::util::get_server_name( name ),
                                                     oh::util::get_provider_name( name ),
                                                     oh::util::get_object_name( name ),
                                                     tag, ex );
        }
    }
}

OHRootHistogram 
OHRootReceiver::getRootHistogram( const IPCPartition & partition,
				  const std::string & name,
                                  int tag )
{
    ISInfoAny info;
    readAny(partition, name, tag, info);
    
    FOR_OH_HISTOGRAM_TYPES( OH_READ_PRESERVING_TYPE )
        
    throw daq::oh::InvalidObject( ERS_HERE );
}

OHRootEfficiency
OHRootReceiver::getRootEfficiency(const IPCPartition & partition,
				  const std::string & name, int tag )
{
    ISInfoAny info;
    readAny(partition, name, tag, info);
    
    if ( info.type() == oh::Efficiency::type() ) {
	return internal::get_object<oh::EfficiencyData>( info, name );
    }
    throw daq::oh::InvalidObject( ERS_HERE );
}

OHRootGraph
OHRootReceiver::getRootGraph(   const IPCPartition & partition,
                                const std::string & name,
                                int tag )
{
    ISInfoAny info;
    readAny(partition, name, tag, info);

    if ( info.type() == oh::Graph::type() ) {
        return internal::get_object<oh::GraphData>( info, name );
    }
    throw daq::oh::InvalidObject( ERS_HERE );
}

OHRootGraph2D 
OHRootReceiver::getRootGraph2D(	const IPCPartition & partition,
				const std::string & name,
                                int tag )
{
    ISInfoAny info;
    readAny(partition, name, tag, info);
    
    if ( info.type() == oh::Graph2D::type() ) {
 	return internal::get_object<oh::Graph2DData>( info, name );
    }
    throw daq::oh::InvalidObject( ERS_HERE );
}

OHRootObject
OHRootReceiver:: getRootObject(	const IPCPartition & partition,
				const std::string & name,
                                int tag )
{
    ISInfoAny info;
    readAny(partition, name, tag, info);
    
    FOR_OH_ALL_TYPES( OH_READ_AS_ROOT_OBJECT )

    throw daq::oh::InvalidObject( ERS_HERE );
}


OHRootEfficiency
OHRootReceiver::getRootEfficiency(	const IPCPartition & partition,
					const std::string & server,
					const std::string & provider,
					const std::string & objectname,
                                        int tag )
{
    return getRootEfficiency( partition,
            oh::util::create_info_name( server, provider, objectname ), tag );
}

OHRootHistogram
OHRootReceiver::getRootHistogram(       const IPCPartition & partition,
                                        const std::string & server,
                                        const std::string & provider,
                                        const std::string & histoname,
                                        int tag )
{
    return getRootHistogram( partition,
            oh::util::create_info_name( server, provider, histoname ), tag );
}

OHRootGraph 
OHRootReceiver::getRootGraph(	const IPCPartition & partition,
				const std::string & server,
				const std::string & provider,
				const std::string & graphname,
                                int tag )
{
    return getRootGraph( partition,
            oh::util::create_info_name( server, provider, graphname ), tag );
}

OHRootGraph2D 
OHRootReceiver::getRootGraph2D(	const IPCPartition & partition,
				const std::string & server,
				const std::string & provider,
				const std::string & graphname,
                                int tag )
{
    return getRootGraph2D( partition,
            oh::util::create_info_name( server, provider, graphname ), tag );
}

OHRootObject
OHRootReceiver:: getRootObject(	const IPCPartition & partition,
				const std::string & server,
				const std::string & provider,
				const std::string & objectname,
                                int tag )
{
    return getRootObject( partition,
            oh::util::create_info_name( server, provider, objectname ), tag );
}


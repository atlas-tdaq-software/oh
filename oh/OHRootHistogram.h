/*
    OHRootObject.h - OH Public header file.
    
    Serguei Kolos, Dec 2004
*/

#ifndef OH_ROOTHISTOGRAM_H
#define OH_ROOTHISTOGRAM_H

#include <TEfficiency.h>
#include <TH1.h>
#include <TGraph.h>
#include <TGraph2D.h>

#include <vector>
#include <string>
#include <memory>

#include <owl/time.h>

template <class T>
struct OHRootObjectBase
{
    std::unique_ptr<T> graph;
    
    std::unique_ptr<T> & getObject()
    { return graph; }
};

template <>
struct OHRootObjectBase<TObject>
{
    std::unique_ptr<TObject> object;
    
    std::unique_ptr<TObject> & getObject()
    { return object; }
};

template <>
struct OHRootObjectBase<TEfficiency>
{
    std::unique_ptr<TEfficiency> efficiency;

    std::unique_ptr<TEfficiency> & getObject()
    { return efficiency; }
};

template <>
struct OHRootObjectBase<TH1>
{
    std::unique_ptr<TH1> histogram;
    
    std::unique_ptr<TH1> & getObject()
    { return histogram; }
};


/** \brief Template class to represent different types of Root objects
	This struct is used to encapsulate information, given to the OHRootReceiver \n
	There are four classes generated of this template by \b typedef: 
	OHRootHistogram, OHRootGraph, OHRootGraph2D, OHRootObject\n
 */
template <class T>
struct OHRootObjectT : public OHRootObjectBase<T>
{    
    typedef T RootType;
    
    //! time of the last update
    OWLTime time;
    
    //! Object's tag
    int tag;
    
    //! Histogram Annotations
    std::vector< std::pair< std::string,std::string > > annotations;
};
 
//! Instantiation of the OHRootObjectT for histograms with TEfficiency type
/** \class OHRootEfficiency
    Use the \c std::unique_ptr \c OHRootEfficiency::efficiency
 *  to access the ROOT efficiency itself */
typedef OHRootObjectT<TEfficiency>	OHRootEfficiency;

//! Instantiation of the OHRootObjectT for histograms with TH1 type
/** \class OHRootHistogram
    Use the \c std::unique_ptr \c OHRootHistogram::histogram
 *  to access the ROOT histogram itself */
typedef OHRootObjectT<TH1>      OHRootHistogram;

//! Instantiation of the OHRootObjectT for graphs with TGraph type 
/** \class  OHRootGraph
    Use the \c std::unique_ptr \c OHRootGraph::graph
 *  to access the ROOT graph itself */
typedef OHRootObjectT<TGraph>	OHRootGraph;

//! Instantiation of the OHRootObjectT for graphs with TGraph2D type
/** \class  OHRootGraph2D
    Use the \c std::unique_ptr OHRootGraph2D::graph
 *  to access the ROOT graph itself */
typedef OHRootObjectT<TGraph2D>	OHRootGraph2D;

//! Instantiation of the OHRootObjectT for generic Root objects with TObject type\n
/** \class OHRootObject 
    Use the \c std::unique_ptr OHRootObject::object
 *  to access the ROOT object itself */
typedef OHRootObjectT<TObject>	OHRootObject;

#endif // OH_ROOTHISTOGRAM_H

/*
    iterator.h - OH Public header file.
    
    Serguei Kolos,  Dec 2006
*/

#ifndef OH_INTERNAL_STREAM_H
#define OH_INTERNAL_STREAM_H

#include <is/infostream.h>

#include <oh/interface/OHReceiver.h>
#include <oh/exceptions.h>
#include <oh/OHUtil.h>

/*! \namespace oh
 *  This is a wrapping namespace for internal OH classes.
 */
namespace oh
{

    /*! \brief This class provides a possibility to selectively retrieve objects
	from the OH. It can be configured to work with specific type of such
	objects i.e. with only histograms or only graphs.
	There are several names generated of this template by \c typedef: 
        OHStream, OHHistogramStream, OHEfficiencyStream, OHGraphStream, OHGraph1DStream, OHGraph2DStream
    */
    template <class T, bool STRICT = false>
    class stream : public ISInfoStream
    {
    public:

	/*!
	    Constructs a new histogram stream object.
            
            \param partition A valid IPCPartition
	    \param server Name of a valid OH server
	    \param provider Optional name of histogram provider, should only contain
	    [a-z], [A-Z], [0-9], '_' and optional wildcars according to the Posix
	    style of regular expressions
	    \param object Optional name of histogram, should only contain
	    [a-z], [A-Z], [0-9], '_' and optional wildcars according to the Posix
	    style of regular expressions
	    \param synchronous if true then the constructor will return only when the 
            		stream is fully constructed, otherwie it resturns immediately
	    \param history_depth defines how many history values have to be added to the
            	stream for each matching infomation object. By defualt is equal to 1. 
                	Negative value means all.
	*/
	stream( const IPCPartition & partition,
		const std::string & server,
		const std::string & provider = ".*",
		const std::string & object = ".*",
                bool synchronous = false,
                int history_depth = 1 ) 
        try : ISInfoStream(  partition, server, 
			   ( STRICT ? T::type() : ~T::type() ) && oh::util::create_regex( provider, object ),
                             synchronous, history_depth )
        { ; } 
        catch ( daq::is::InvalidCriteria & ex ) {
            throw daq::oh::InvalidRegex( ex.context(), ex.get_expression() );
        }
        catch ( daq::is::RepositoryNotFound & ex ) {
            throw daq::oh::RepositoryNotFound( ex.context(), ex.get_name() );
        }

	/*!
	    Retrieve the current object or set of objects at the beginning of the stream.
	    \param receiver An instance of an OHReceiver derived class that. The object at the
	    beginning of the stream will be passed to this receiver via one of its virtual functions.
	*/
	void retrieve( OHReceiver & receiver ) 
	{
	    receiver.readObject( *this );
        }

	/*!
	    Retrieve name of the provider which has published the current histogram
	    \return The name of the provider who published the histogram at
	    the beginning of the stream.
	*/
	std::string provider( ) const{
	    return ( oh::util::get_provider_name( ISInfoStream::name( ) ) );
	}

	/*!
	    Retrieve name of the current object
	    \return The name of the the histogram at the beginning of the stream.
	*/
	std::string name( ) const {
	    return ( oh::util::get_object_name( ISInfoStream::name( ) ) );
	}
    };
}

#endif

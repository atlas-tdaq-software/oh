/*
 OHRootProvider.h - OH Public header file.

 Fredrik Sandin, Oct 2001
 Monika Barczyk, Sept 2002
 Serguei Kolos , Nov 2004
 Herbert Kaiser, Aug 2006
 */

#ifndef OH_ROOTPROVIDER_H
#define OH_ROOTPROVIDER_H

#include <memory>
#include <TEfficiency.h>
#include <TH1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <oh/OHUtil.h>
#include <oh/interface/OHProvider.h>

//! OH Provider for Root objects
/*!
 This class provides functionality to export histograms and graphs from ROOT to the OH
 */
class OHRootProvider: public OHProvider {
public:

    //! Create a new ROOT provider
    OHRootProvider(const IPCPartition & p, const std::string & server,
            const std::string & name, OHCommandListener * lst = 0);

    /*!
     *  \param histogram A valid ROOT histogram
     *  \param name A name describing the histogram. Should only contain characters
     *         from [a-z], [A-Z], [0-9] and '_'.
     *  \param tag this tag will be associated with the currently published histogram value. This tag
     *         can be used later to retrive the corresponding value from the OH repository.
     *  \throw daq::oh::RepositoryNotFound if a communication error occurred while communicating with the OH repository
     *  \throw daq::oh::ObjectTypeMismatch if the name which was provided is already taken by the object of a different type (i.e. graph)
     */
    void
    publish(const TH1 & histogram, const std::string & name, int tag = -1,
            const std::vector<std::pair<std::string, std::string> > & ann =
                    oh::util::EmptyAnnotation);

    /**  Publish ROOT graph.
     *  Supported types are: \c TGraph, \c TGraphErrors, \c TGraphAsymmErrors, \c TGraphBentErrors
     *  \param graph the graph to publish
     *  \param name  name of the graph as it is published in IS
     *  \param tag this tag will be associated with the currently published histogram value. This tag
     *         can be used later to retrive the corresponding value from the OH repository.
     *  \throw daq::oh::RepositoryNotFound if a communication error occurred while communicating with the OH repository
     *  \throw daq::oh::ObjectTypeMismatch if the name which was provided is already taken by the object of a different type (i.e. histogram)
     */
    void
    publish(const TGraph & graph, const std::string & name, int tag = -1,
            const std::vector<std::pair<std::string, std::string> > & ann =
                    oh::util::EmptyAnnotation);

    /** Publish 2D ROOT graph
     *  Supported types are: TGraph2D and TGraph2DErrors
     *  \param graph the graph to publish
     *  \param name  name of the graph as it is published in IS
     *  \param tag this tag will be associated with the currently published histogram value. This tag
     *         can be used later to retrive the corresponding value from the OH repository.
     *  \throw daq::oh::RepositoryNotFound if a communication error occurred while communicating with the OH repository
     *  \throw daq::oh::ObjectTypeMismatch if the name which was provided is already taken by the object of a different type (i.e. histogram)
     *  \sa OHRootProvider::publish( const TGraph & graph, ... )
     */
    void
    publish(const TGraph2D & graph, const std::string & name, int tag = -1,
            const std::vector<std::pair<std::string, std::string> > & ann =
                    oh::util::EmptyAnnotation);

    /** Publish TEfficiency ROOT object
     *  \param eff the effici*  ency object to publish
     *  \param name  name of the graph as it is published in IS
     *  \param tag this tag will be associated with the currently published histogram value. This tag
     *         can be used later to retrive the corresponding value from the OH repository.
     *  \throw daq::oh::RepositoryNotFound if a communication error occurred while communicating with the OH repository
     *  \throw daq::oh::ObjectTypeMismatch if the name which was provided is already taken by the object of a different type (i.e. histogram)
     *  \sa OHRootProvider::publish( const TH1 & , ... )
     */
    void
    publish(const TEfficiency & eff, const std::string & name, int tag = -1,
            const std::vector<std::pair<std::string, std::string> > & ann =
                    oh::util::EmptyAnnotation);

    /**
     *  Convert ROOT histogram object into corresponding IS object
     *
     *  \param histogram A valid ROOT histogram
     */
    static std::unique_ptr<oh::Histogram>
    convert(const TH1& histogram,
            const std::vector<std::pair<std::string, std::string>>& ann = oh::util::EmptyAnnotation);

};

#endif // OHROOTPROVIDER_H

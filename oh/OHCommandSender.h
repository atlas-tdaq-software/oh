/* OHCommandSender.h - OH private header file.  OH command receiver baseclass definition.  Serguei Kolos, Feb 2004 */
#ifndef OH_COMMAND_SENDER_H
#define OH_COMMAND_SENDER_H
#include <string>
#include <is/infodictionary.h>
#include <oh/exceptions.h>

//! Send commands to OH Providers
/**
    This class can be used to send commands to histogram providers.
    \sa OHCommandListener
 */
class OHCommandSender
{
  public:
        
    OHCommandSender( const IPCPartition & p )
      : dict_( p )
    { ; }
    
    /*!
	Sends command which is targeted to a particular histogram
	\param server_name name of the IS server where the target histogram is published
	\param provider_name name of the target provider
	\param histogram_name name of the target histogram
	\param command	command to be sent
    */
    void sendCommand(	const std::string & server_name,
    			const std::string & provider_name,
    			const std::string & histogram_name, 
    			const std::string & command ) ;
			
    /*!
	Sends command which is targeted to a particular provider
	\param server_name name of the IS server where the target histogram is published
	\param provider_name name of the target provider
	\param command	command to be sent
    */
    void sendCommand(	const std::string & server_name,
    			const std::string & provider_name,
    			const std::string & command ) ;
                        
  private:
    ISInfoDictionary dict_;			
};

#endif

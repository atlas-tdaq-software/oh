/*
    OHRawProvider.inl - OH Private file.
    
    Implementation of the OHRawProvider template.
    Fredrik Sandin, Oct 2001
    Sergei Kolos,   May 2004
    Herbert Kaiser, Aug 2006
*/

#include <assert.h>
#include <oh/core/HistogramData.h>
#include <oh/core/GraphData.h>

template <typename T>
OHAxis::OHAxis( const std::string & label, size_t bincount, T low, T width )
  : label_ ( label ),
    bincount_( bincount ),
    low_ ( low ),
    width_ ( width )
{ ; }

template <typename T>
OHAxis::OHAxis( const std::string & label, size_t bincount, T * bins )
  : label_ ( label ),
    bincount_( bincount ),
    bins_( bincount_ + 1 ),
    low_ ( 0 ),
    width_ ( 0 )
{
    for ( size_t i = 0; i <= bincount_; i++ )
    	bins_[i] = bins[i];
}


template<class TB>
OHRawProvider<TB>:: OHRawProvider(	const IPCPartition & p,
					const string & server,
					const string & name,
					OHCommandListener * lst )
  : OHProvider( p, server, name, lst ) 
{ ; }


template<class TB>
template<class TC, class TE>
void
OHRawProvider<TB>:: publish(	const string & name,
				const string & title,
				const OHAxis & xaxis,
				const TC * contents,
				const TE * errors,
				bool hasOverflowAndUnderflow,
				int tag,
				const vector< pair<string,string> > & annotations ) const
{
    assert( contents );
    oh::HistogramData<TC> histo;
    histo.set_dimension( oh::Histogram::D1 );
    histo.set_title( title );
    histo.set_axis_title( oh::Axis::X, xaxis.label_ );
    histo.set_annotations( annotations );

    int entries = 0;
    size_t size = hasOverflowAndUnderflow ? xaxis.bincount_ + 2 : xaxis.bincount_;
    for (size_t i = 0; i < size; ++i) {
        entries += contents[i];
    }
    histo.set_entries( entries );
    
    if ( xaxis.bins_.empty() )
    	histo.set_axis_range( oh::Axis::X, xaxis.low_, xaxis.width_, xaxis.bincount_ );
    else
    	histo.set_axis_range( oh::Axis::X, xaxis.bins_ );
    
    if ( hasOverflowAndUnderflow )
    {
	histo.set_bins_array( contents, xaxis.bincount_ + 2 );
        if ( errors )
	    for ( size_t x = 0; x < xaxis.bincount_ + 2; x++ )
	    {
		histo.set_error( x, 0, 0, errors[TB::index(x)] );
	    }
    }
    else
    {
	histo.set_bin_value( 0, 0, 0, 0 );  			// underflow
	histo.set_bin_value( xaxis.bincount_ + 1, 0, 0, 0 );	// overflow
	
	if ( errors )
	{
	    histo.set_error( 0, 0, 0, 0 );  			// underflow
	    histo.set_error( xaxis.bincount_ + 1, 0, 0, 0 );	// overflow
	    for ( size_t x = 0; x < xaxis.bincount_; x++ )
	    {
		histo.set_bin_value( x + 1, 0, 0, contents[TB::index(x)] );
		histo.set_error( x + 1, 0, 0, errors[TB::index(x)] );
	    }
	}
	else
	{
	    for ( size_t x = 0; x < xaxis.bincount_; x++ )
	    {
		histo.set_bin_value( x + 1, 0, 0, contents[TB::index(x)] );
	    }
	}
    }
    OHProvider::publish( histo, name, tag );
}

template<class TB>
template<class TC, class TE>
void
OHRawProvider<TB>:: publish(	const string & name,
				const string & title,
				const OHAxis & xaxis,
				const OHAxis & yaxis,
				const TC * contents,
				const TE * errors,
				bool hasOverflowAndUnderflow,
				int tag,
				const vector< pair<string,string> > & annotations ) const
{
    assert( contents );
    oh::HistogramData<TC> histo;
    histo.set_dimension( oh::Histogram::D2 );
    histo.set_title( title );
    histo.set_axis_title( oh::Axis::X, xaxis.label_ );
    histo.set_axis_title( oh::Axis::Y, yaxis.label_ );
    histo.set_annotations( annotations );
    
    if ( xaxis.bins_.empty() )
    	histo.set_axis_range( oh::Axis::X, xaxis.low_, xaxis.width_, xaxis.bincount_ );
    else
    	histo.set_axis_range( oh::Axis::X, xaxis.bins_ );
	
    if ( yaxis.bins_.empty() )
    	histo.set_axis_range( oh::Axis::Y, yaxis.low_, yaxis.width_, yaxis.bincount_ );
    else
    	histo.set_axis_range( oh::Axis::Y, yaxis.bins_ );
    
    size_t wx = xaxis.bincount_ + 2;
    size_t wy = yaxis.bincount_ + 2;
    
    int entries = 0;
    size_t x_size = hasOverflowAndUnderflow ? wx : xaxis.bincount_;
    size_t y_size = hasOverflowAndUnderflow ? wy : yaxis.bincount_;
    for (size_t x = 0; x < x_size; ++x) {
        for (size_t y = 0; y < y_size; ++y) {
            entries += contents[TB::index(x, x_size, y)];
        }
    }
    histo.set_entries( entries );
    
    if ( hasOverflowAndUnderflow )
    {
	histo.set_bins_array( contents, wx * wy );
        if ( errors )
	    for ( size_t x = 0; x < wx; x++ )
	    {
		for ( size_t y = 0; y < wy; y++ )
		{
		    histo.set_error( x, y, 0, errors[TB::index(x,wx,y)] );
		}
	    }
    }
    else
    {
	for ( size_t x = 0; x < wx; x++ )
	{
	    histo.set_bin_value( x, 0, 0, 0 );  			// underflow
	    histo.set_bin_value( x, yaxis.bincount_ + 1, 0, 0 );	// overflow
	}
	
	for ( size_t y = 0; y < wy; y++ )
	{
	    histo.set_bin_value( 0, y, 0, 0 );  			// underflow
	    histo.set_bin_value( xaxis.bincount_ + 1, y, 0, 0 );	// overflow
	}
	
	if ( errors )
	{
	    for ( size_t x = 0; x < wx; x++ )
	    {
		histo.set_error( x, 0, 0, 0 );  			// underflow
		histo.set_error( x, yaxis.bincount_ + 1, 0, 0 );	// overflow
	    }
	
	    for ( size_t y = 0; y < wy; y++ )
	    {
		histo.set_error( 0, y, 0, 0 );  			// underflow
		histo.set_error( xaxis.bincount_ + 1, y, 0, 0 );	// overflow
	    }
	
	    for ( size_t x = 0; x < xaxis.bincount_; x++ )
	    {
		for ( size_t y = 0; y < yaxis.bincount_; y++ )
		{
		    histo.set_bin_value( x + 1, y + 1, 0, contents[TB::index(x,xaxis.bincount_,y)] );
		    histo.set_error( x + 1, y + 1, 0, errors[TB::index(x,xaxis.bincount_,y)] );
		}
	    }
    	}
	else
	{
	    for ( size_t x = 0; x < xaxis.bincount_; x++ )
	    {
		for ( size_t y = 0; y < yaxis.bincount_; y++ )
		{
		    histo.set_bin_value( x + 1, y + 1, 0, contents[TB::index(x,xaxis.bincount_,y)] );
		}
	    }
	}
    }
    
    OHProvider::publish( histo, name, tag );
}


template<class TB>
void 
OHRawProvider<TB>::publish(	const string & name,
				const string & title,
				unsigned int entries,
				const double * x,
				const double * y,
				const OHAxis & xaxis,
				const OHAxis & yaxis,
				oh::Graph::ErrorStyle estyle,
				const vector<const double *>& errorlist,
				int tag,
				const vector< pair<string,string> > & annotations) const {
    assert( x && y );
    oh::GraphData gi;
    gi.set_error_style(estyle);
    gi.set_title(title);
    gi.set_entries(entries);
    gi.set_annotations(annotations);
    gi.set_axis_title(oh::Axis::X, xaxis.label_);
    gi.set_axis_title(oh::Axis::Y, yaxis.label_);
    if ( xaxis.bins_.empty() )
    	gi.set_axis_range( oh::Axis::X, xaxis.low_, xaxis.width_, xaxis.bincount_ );
    else
    	gi.set_axis_range( oh::Axis::X, xaxis.bins_ );
    if ( yaxis.bins_.empty() )
    	gi.set_axis_range( oh::Axis::Y, yaxis.low_, yaxis.width_, yaxis.bincount_ );
    else
    	gi.set_axis_range( oh::Axis::Y, yaxis.bins_ );
    std::vector<const double*> points(2);
    points[0]=x;
    points[1]=y;
    gi.set_points(points);
    gi.set_errors(errorlist);

    OHProvider::publish( gi, name, tag );
}


template<class TB>
void
OHRawProvider<TB>::publish(	const string & name,
				const string & title,
				unsigned int entries,
				const double * x,
				const double * y,
				const double * z,
				const OHAxis & xaxis,
				const OHAxis & yaxis,
				const OHAxis & zaxis,
				oh::Graph2D::ErrorStyle estyle,
				const vector<const double *>& errorlist,
				int tag,
				const vector< pair<string,string> > & annotations ) const {
    assert( x && y && z);
    oh::Graph2DData gi;
    gi.set_error_style(estyle);
    gi.set_title(title);
    gi.set_entries(entries);
    gi.set_annotations(annotations);
    gi.set_axis_title(oh::Axis::X, xaxis.label_);
    gi.set_axis_title(oh::Axis::Y, yaxis.label_);
    gi.set_axis_title(oh::Axis::Z, yaxis.label_);
    if ( xaxis.bins_.empty() )
    	gi.set_axis_range( oh::Axis::X, xaxis.low_, xaxis.width_, xaxis.bincount_ );
    else
    	gi.set_axis_range( oh::Axis::X, xaxis.bins_ );
    if ( yaxis.bins_.empty() )
    	gi.set_axis_range( oh::Axis::Y, yaxis.low_, yaxis.width_, yaxis.bincount_ );
    else
    	gi.set_axis_range( oh::Axis::Y, yaxis.bins_ );
    if ( zaxis.bins_.empty() )
    	gi.set_axis_range( oh::Axis::Z, zaxis.low_, zaxis.width_, zaxis.bincount_ );
    else
    	gi.set_axis_range( oh::Axis::Z, zaxis.bins_ );
    std::vector<const double*> points(3);
    points[0]=x;
    points[1]=y;
    points[2]=z;
    gi.set_points(points);
    gi.set_errors(errorlist);

    OHProvider::publish( gi, name, tag );
}

/*
    OHReceiver.h - OH private header file.
    
    Fredrik Sandin, Oct 2001
    Sergei Kolos,   May 2004
    Herbert Kaiser, Aug 2006
*/

#ifndef OH_RECEIVER_H
#define OH_RECEIVER_H

#include <vector>

#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <is/infostream.h>

#include <oh/core/GraphData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/HistogramData.h>
#include <oh/core/EfficiencyData.h>
#include <oh/OHUtil.h>
#include <oh/exceptions.h>

namespace oh
{
    template <class , bool > class iterator;
    template <class , bool > class stream;
}


//! Histogram Receiver baseclass
/*!
    This class provides functionality to receive histograms and graphs from an
    OHIterator or OHSubscriber in the OH internal
    formats and thus acts as an abstraction layer to the underlying
    technology used to transport and buffer histograms. Derive a new
    class for each external package (or histogram format) which should
    import histograms/graphs from the OH.
    
    Example:
    
    \code
    class MyReceiver : public OHReceiver {
    protected:
        
        // Histogram user tasks should override this function to
        // receive histograms
        virtual void receive( MyHistogram & histo ) = 0;
        
    private:
        
        void receive_( const std::string & name, oh::HistogramData<char> & histogram ) {
            // Need to create a new histogram and fill it with info
            // from the received histogram
            MyHistogram h( ... );
            
            // Conversion stuff goes here...
            
            // Let the user receive the histogram
            receive( h );
        }
        
    };
    \endcode

    For more information on how to convert from the OH internal histogram
    representations see oh::OHHistogramData. For OH internal graphs see
    oh::GraphData.
    
    Observe the possibility here to use multiple inheritance in order to
    access protected members in the non OH histograms.
    
    \sa OHRootReceiver

*/

class OHReceiver
{
  template <class , bool > friend class oh::iterator;
  template <class , bool > friend class oh::stream;
  friend class OHSubscriber;
    
  public:
    typedef ISInfo::Reason      Reason;

    static const Reason Created = ISInfo::Created;
    static const Reason Updated = ISInfo::Updated;
    static const Reason Deleted = ISInfo::Deleted;

    //! Destructor
    virtual ~OHReceiver( ) { ; }
    
    //! Can be used to read all tags for a given object
    static void getTags(const IPCPartition & p,
			const std::string & name,
                        std::vector<std::pair<int,OWLTime> > & tags ) ;

    //! Can be used to read all tags for a given object
    static void getTags(const IPCPartition & p,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
                        std::vector<std::pair<int,OWLTime> > & tags ) ;

    //! Can be used to read a single histogram or profile from the OH
    void getHistogram(	const IPCPartition & p,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			bool  with_history = false,
			int   how_many = -1 ) ;

    //! Can be used to read a single histogram or profile from the OH
    void getHistogram(	const IPCPartition & p,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			int  tag ) ;

    //! Can be used to read a single histogram or profile from the OH
    void getHistogram(	const IPCPartition & p,
			const std::string & name,
			bool  with_history = false,
			int   how_many = -1 ) ;

    //! Can be used to read a single histogram or profile from the OH
    void getHistogram(	const IPCPartition & p,
			const std::string & name,
			int  tag ) ;

    //! Can be used to read a single histogram, profile or graph from the OH
    void getObject(	const IPCPartition & p,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			bool  with_history = false,
			int   how_many = -1 ) ;

    //! Can be used to read a single histogram, profile or graph from the OH
    void getObject(	const IPCPartition & p,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			int tag ) ;

    //! Can be used to read a single histogram, profile or graph from the OH
    void getObject(	const IPCPartition & p,
			const std::string & name,
			bool  with_history = false,
			int   how_many = -1 ) ;

    //! Can be used to read a single histogram, profile or graph from the OH
    void getObject(	const IPCPartition & p,
			const std::string & name,
			int tag ) ;

    //! Called when a histogram is received
    /*!
        \param histogram A histogram.
        \param name A histogram name.
    */
    virtual void receive_( const std::string & name, oh::HistogramData<char> & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::HistogramData<short> & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::HistogramData<int> & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::HistogramData<float> & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::HistogramData<double> & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::ProfileData & histogram ) = 0;
    virtual void receive_( const std::string & name, oh::GraphData & graph ) = 0;
    virtual void receive_( const std::string & name, oh::Graph2DData & graph ) = 0;
    virtual void receive_( const std::string & name, oh::EfficiencyData & efficiency ) = 0;
    
    //! Called when a set of histograms is received
    /*!
        \param name A histogram name.
        \param set A std::vector of recent values of this histogram.
    */
    virtual void receive_( const std::string & name, std::vector<oh::HistogramData<char> > & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::HistogramData<short> > & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::HistogramData<int> > & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::HistogramData<float> > & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::HistogramData<double> > & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::ProfileData> & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::GraphData> & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::Graph2DData> & set ) = 0;
    virtual void receive_( const std::string & name, std::vector<oh::EfficiencyData> & set ) = 0;
    
    //! This method is called when one of the objects in the OH has been changed.
    //! The value of the object is not transported.
    /*!
        \param name An object name.
        \param time Time when the hsitogram has been changed. 
        \param reason Explains what has happened with the object. The possibilities are: Created, Updated, Removed.
    */
    virtual void objectChanged( const std::string & name, const OWLTime & time, Reason reason );

    //! This method is called by the OH core once for every receiver in order to set the receiving policy.
    /*!
        \return true if the recievd object has to be removed from the repository, false otherwise.
    */
    virtual bool removePolicy( ) { return false; }

  private:

    //! Used by OHIterator
    void readObjects( const ISInfoIterator & iterator, bool with_history, short how_many );
    
    //! Used by OHIterator
    void readObjectWithTag( const ISInfoIterator & iterator, int tag );
    
    //! Used by OHStream
    void readObject( ISInfoStream & stream );
        
    //! Used by OHSubscriber
    void readObject( const ISCallbackInfo & info );
    
    //! Used internaly by the OHReceiver
    void readObjects( const IPCPartition & partition, const std::string & info_name, int how_many );

    //! Used internaly by the OHReceiver
    void readObjectWithTag( const IPCPartition & partition, const std::string & info_name, int tag );
};

#endif // OH_RECEIVER_H

/*
 * HistogramTraits.h
 *
 *  Created on: Apr 12, 2019
 *      Author: kolos
 */

#ifndef OH_CORE_HISTOGRAMTRAITS_H_
#define OH_CORE_HISTOGRAMTRAITS_H_

#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>

#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>

namespace oh {
    template<typename T>
    struct HistogramTraits;

    template<> struct HistogramTraits<TH1C> {
        typedef char BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 4 };
    };
    template<> struct HistogramTraits<TH1S> {
        typedef short BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 4 };
    };
    template<> struct HistogramTraits<TH1I> {
        typedef int BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 4 };
    };
    template<> struct HistogramTraits<TH1F> {
        typedef float BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 4 };
    };
    template<> struct HistogramTraits<TH1D> {
        typedef double BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 4 };
    };
    template<> struct HistogramTraits<TProfile> {
        typedef double BinType;
        typedef oh::ProfileData ISType;
        enum _void_ { StatsSize = 6 };
    };

    template<> struct HistogramTraits<TH2C> {
        typedef char BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 7 };
    };
    template<> struct HistogramTraits<TH2S> {
        typedef short BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 7 };
    };
    template<> struct HistogramTraits<TH2I> {
        typedef int BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 7 };
    };
    template<> struct HistogramTraits<TH2F> {
        typedef float BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 7 };
    };
    template<> struct HistogramTraits<TH2D> {
        typedef double BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 7 };
    };
    template<> struct HistogramTraits<TProfile2D> {
        typedef double BinType;
        typedef oh::ProfileData ISType;
        enum _void_ { StatsSize = 9 };
    };

    template<> struct HistogramTraits<TH3C> {
        typedef char BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 11 };
    };
    template<> struct HistogramTraits<TH3S> {
        typedef short BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 11 };
    };
    template<> struct HistogramTraits<TH3I> {
        typedef int BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 11 };
    };
    template<> struct HistogramTraits<TH3F> {
        typedef float BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 11 };
    };
    template<> struct HistogramTraits<TH3D> {
        typedef double BinType;
        typedef oh::HistogramData<BinType> ISType;
        enum _void_ { StatsSize = 11 };
    };
    template<> struct HistogramTraits<TProfile3D> {
        typedef double BinType;
        typedef oh::ProfileData ISType;
        enum _void_ { StatsSize = 13 };
    };
}

#endif /* OH_CORE_HISTOGRAMTRAITS_H_ */

#ifndef IDENTITY_H
#define IDENTITY_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * This class is used to distinguish OH clases from the other IS types
 * 
 * @author  produced by the IS generator
 */

class Identity : public ISInfo {
public:

    /**
     * Object name
     */
    std::string                                name;

    /**
     * Object title
     */
    std::string                                title;


    static const ISType & type() {
	static const ISType type_ = Identity( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "name: " << name << "\t// Object name" << std::endl;
	out << "title: " << title << "\t// Object title";
	return out;
    }

    Identity( )
      : ISInfo( "Identity" )
    {
	initialize();
    }

    ~Identity(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Identity( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << title;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> title;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Identity & info ) {
    info.print( out );
    return out;
}

}

#endif // IDENTITY_H

#ifndef PROFILEDATA_H
#define PROFILEDATA_H

#include "oh/core/HistogramData.h"
#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * This class is used to store profiles in the IS
 * 
 * @author  produced by the IS generator
 */

class ProfileData : public HistogramData<double> {
public:

    /**
     * Number of entries per bin
     */
    std::vector<double>                        bin_entries;


    static const ISType & type() {
	static const ISType type_ = ProfileData( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	HistogramData<double>::print( out );
	out << std::endl;
	out << "bin_entries[" << bin_entries.size() << "]:\t// Number of entries per bin" << std::endl;
	for ( size_t i = 0; i < bin_entries.size(); ++i )
	    out << i << " : " << bin_entries[i] << std::endl;
	return out;
    }

    ProfileData( )
      : HistogramData<double>( "ProfileData" )
    {
	initialize();
    }

    ~ProfileData(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ProfileData( const std::string & type )
      : HistogramData<double>( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	HistogramData<double>::publishGuts( out );
	out << bin_entries;
    }

    void refreshGuts( ISistream & in ){
	HistogramData<double>::refreshGuts( in );
	in >> bin_entries;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ProfileData & info ) {
    info.print( out );
    return out;
}

}

#endif // PROFILEDATA_H

#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "oh/core/Object.h"
#include "oh/core/Axis.h"
#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * This class is used to store histograms in the IS
 * 
 * @author  produced by the IS generator
 */

class Histogram : public Object {
public:
    enum Dimension { D1 = 1, D2, D3 };


    /**
     * Histogram dimension
     */
    Dimension                                  dimension;

    /**
     * Optional error values for the bins
     */
    std::vector<double>                        errors;

    /**
     * Statistics values
     */
    std::vector<double>                        statistics;

    /**
     * Number of entries
     */
    uint64_t                                   entries;

    /**
     * Axes for this object
     */
    std::vector<Axis>                          axes;


    static const ISType & type() {
	static const ISType type_ = Histogram( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	Object::print( out );
	out << std::endl;
	out << "dimension: " << dimension << "\t// Histogram dimension" << std::endl;
	out << "errors[" << errors.size() << "]:\t// Optional error values for the bins" << std::endl;
	for ( size_t i = 0; i < errors.size(); ++i )
	    out << i << " : " << errors[i] << std::endl;
	out << "statistics[" << statistics.size() << "]:\t// Statistics values" << std::endl;
	for ( size_t i = 0; i < statistics.size(); ++i )
	    out << i << " : " << statistics[i] << std::endl;
	out << "entries: " << entries << "\t// Number of entries" << std::endl;
	out << "axes[" << axes.size() << "]:\t// Axes for this object" << std::endl;
	for ( size_t i = 0; i < axes.size(); ++i )
	    out << i << " : " << axes[i] << std::endl;
	return out;
    }

    Histogram( )
      : Object( "Histogram" )
    {
	initialize();
    }

    ~Histogram(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Histogram( const std::string & type )
      : Object( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	Object::publishGuts( out );
	out << dimension << errors << statistics << entries << axes;
    }

    void refreshGuts( ISistream & in ){
	Object::refreshGuts( in );
	in >> dimension >> errors >> statistics >> entries >> axes;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>
protected:
    int index( int x, int y, int z ) const
    {
        int wx = get_bin_count( oh::Axis::X ) + 2;
        int wy = get_bin_count( oh::Axis::Y ) + 2;
        return x + wx * ( y + wy * z );
    }

    void alloc_errors( )
    {
        if ( errors.size() == 0 ) {
            //
            // + 2 is used to take into account overflow and underflow bins
            //
            int size =      ( get_bin_count( oh::Axis::X ) + 2 ) *
                            ( get_bin_count( oh::Axis::Y ) + 2 ) *
                            ( get_bin_count( oh::Axis::Z ) + 2 );
            errors.resize( size );
        }
    }

public :
    virtual int get_bin_count( oh::Axis::ID axis ) const
    {
        return -1;
    }

    void set_dimension( oh::Histogram::Dimension d )
    {
        dimension = d;
    }

    oh::Histogram::Dimension get_dimension( ) const
    {
        return dimension;
    }

    double get_error( int x, int y, int z ) const {
        assert( errors.size() != 0 );
        return errors[ index( x, y, z ) ];
    }

    const double * get_errors( ) const
    {
        return ( errors.size() > 0 ? &errors[0] : 0 );
    }

    void set_error( int x, int y, int z, double error )
    {
        alloc_errors( );
        errors[ index( x, y, z ) ] = error;
    }

    void set_errors( double * errors, unsigned int size )
    {
        this->errors.resize( size );
        memcpy( &(this->errors[0]), errors, size * sizeof( double ) );
    }

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Histogram & info ) {
    info.print( out );
    return out;
}

}

#endif // HISTOGRAM_H

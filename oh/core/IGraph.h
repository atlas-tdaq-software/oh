/*
 * IGraph.h - interface to the oh::Graph IS type
 * Serguei Kolos
 */

#ifndef OH_IGRAPH_H
#define OH_IGRAPH_H

#include <oh/core/ObjectBase.h>

namespace oh
{
    /** Interface to the IS Graph type.
     *  The Graph object to work on must be given in the constructor.\n
     *  When setting the data via arrays, be sure that the arrays have the
     *  appropriate size (which is the number of graph points), this is not checked.
     *  Do \c set_dimension and \c set_entries before setting the data and do
     *  \c set_errorstyle before setting the errors.
     */
    template <class T>
    class IGraph : public ObjectBase<T>
    {
        public:
            /** Retrieve a vector with all error arrays NO COPIES are made
	     *  For TGraphErrors we have 2 errors: ex,ey
	     *  For TGraphAsymmErrors we have 4 errors: exl,exh,eyl,eyh
	     *  For TGraphBentErrors we have 8 errors: exl,exh,eyl,eyh,exld,exhd,eyld,eyhd
	     *  For TGraph2DErrors we have 3 errors: ex,ey,ez
	     */
	    void get_errors( std::vector<const double*>& output ) const {
	        output.resize( T::errors.size() );
	        for ( unsigned int i = 0; i < T::errors.size(); i++ ) {
	            output[i] = &T::errors[i*T::entries];
	        }
	    }

            /** set all the errors, copies of the double arrays are made */
	    void set_errors(const std::vector<const double*>& input ) {
	        T::errors.resize( input.size()*T::entries );
	        for (unsigned int i = 0; i < input.size(); i++ ) {
	            memcpy( &T::errors[i*T::entries], input[i], sizeof( double ) * T::entries );
	        }
	    }

            /** Set error style */
	    void set_error_style(typename T::ErrorStyle es) {
	        T::errorStyle=es;
	    }

            /** get error style */
	    typename T::ErrorStyle get_error_style() const {
	        return T::errorStyle;
	    }

            /** get all points at once no copies are made*/
	    void get_points(std::vector<const double*>& valueArrays) const {
	        assert( T::points.size() >= (T::dimension+1)*T::entries );
	        valueArrays.resize(T::dimension+1);
	        for (int i=0;i<=T::dimension;i++) {
	            valueArrays[i]=&T::points[i*T::entries];
	        }
	    }

            /** get all the data in arrays first all the points then all the errors */
	    void get_data(std::vector<const double*>& data) const {
	        data.resize( T::dimension + T::errors.size() + 1 );
	        for (unsigned int i=0;i<=T::dimension;i++) {
	            data[i]=&T::points[i*T::entries];
	        }
	        for (unsigned int i=0;i<T::errors.size();i++) {
	            data[i+T::dimension+1]=&T::errors[i*T::entries];
	        }
	    }

            /** set all points at once, copies of the arrays are made
             * The vector must provide 2 arrays for normal graphs
             * and 3 arrays for Graph2D graphs
             */
	    void set_points(const std::vector<const double*>& valueArrays) {
	        assert( valueArrays.size() >= static_cast<unsigned>(T::dimension+1) );
	        T::points.resize( (T::dimension+1)*T::entries );
	        for (unsigned int i=0;i<T::dimension+1;i++) {
	            memcpy(&T::points[i*T::entries], valueArrays[i], sizeof(double) * T::entries);
	        }
	    }

    };
}

#endif //OH_IGRAPH_H


#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/GraphData.h>

#define FOR_OH_HISTOGRAM_TYPES(decl)	\
decl( oh::HistogramData<char> )		\
decl( oh::HistogramData<short> ) 	\
decl( oh::HistogramData<int> ) 		\
decl( oh::HistogramData<float> ) 	\
decl( oh::HistogramData<double> )	\
decl( oh::ProfileData )

#define FOR_OH_ALL_TYPES(decl)		\
FOR_OH_HISTOGRAM_TYPES(decl)		\
decl( oh::GraphData )			\
decl( oh::Graph2DData )                 \
decl( oh::EfficiencyData )


/*
    OHServerIterator.h - OH Public header file.
    
    Fredrik Sandin, Oct 2001
    Monika Barczyk, Feb 2003
*/

#ifndef OH_SERVER_ITERATOR_H
#define OH_SERVER_ITERATOR_H

#include <is/serveriterator.h>

//! OH servers iterator
/*! \class OHServerIterator
    This is a \b typedef for the ISServerIterator class which provides a way
    of retrieving the names of all IS servers in an particular IPC partition.
*/

typedef ISServerIterator OHServerIterator;

#endif // OH_SERVER_ITERATOR_H

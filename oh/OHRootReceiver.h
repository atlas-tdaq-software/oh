/*
 OHRootReceiver.h - OH Public header file.

 Fredrik Sandin, Oct 2001
 Serguei Kolos,  Apr 2006
 Herbert Kaiser, Aug 2006
 */

#ifndef OH_ROOTRECEIVER_H
#define OH_ROOTRECEIVER_H

#include <vector>
#include <string>

#include <is/infodictionary.h>

#include <oh/interface/OHReceiver.h>
#include <oh/core/EfficiencyData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/HistogramData.h>
#include <oh/OHRootHistogram.h>
#include <oh/exceptions.h>

//! OH Receiver for Root objects
/*!
 This class provides functionality to import histograms and graphs from the OH to ROOT
 */
class OHRootReceiver: public OHReceiver {
public:

    virtual ~OHRootReceiver() = default;
    
    /*!
     Call this function to get a particular efficiency object from the OH repository.
     \param partition TDAQ IPC partition
     \param server OH server name
     \param provider name of the histogram provider
     \param object name of the efficiency object
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootEfficiency object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootEfficiency
     */
    static OHRootEfficiency getRootEfficiency(const IPCPartition &p, const std::string &server,
            const std::string &provider, const std::string & object, int tag = -1);

    /*!
     Call this function to get a particular histogram from the OH repository.
     \param partition TDAQ IPC partition
     \param server OH server name
     \param provider name of the histogram provider
     \param object name of the histogram
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootHistogram object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootHistogram
     */
    static OHRootHistogram getRootHistogram(const IPCPartition &p, const std::string &server,
            const std::string &provider, const std::string &object, int tag = -1);

    /*!
     Call this function to get a particular graph with 2 axes (TGraph-type) from the OH repository.
     \param partition TDAQ IPC partition
     \param server OH server name
     \param provider name of the provider
     \param object name of the graph
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootGraph object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootHistogram
     */
    static OHRootGraph getRootGraph(const IPCPartition &p, const std::string &server,
            const std::string &provider, const std::string &object, int tag = -1);

    /*!
     Call this function to get a particular graph with 3 axes (TGraph2D-type) from the OH repository.
     \param partition TDAQ IPC partition
     \param server OH server name
     \param provider name of the provider
     \param object name of the graph
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootGraph2D object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootHistogram
     */
    static OHRootGraph2D getRootGraph2D(const IPCPartition &p, const std::string &server,
            const std::string &provider, const std::string &object, int tag = -1);

    /*!
     Call this function to get a ROOT object derived from TObject from the OH repository.
     At the moment this would be a histogram of type TH1 or a graph of type TGraph or TGraph2D.
     \param partition TDAQ IPC partition
     \param server OH server name
     \param provider name of the provider
     \param object name of the object
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootObject object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootObject
     */
    static OHRootObject getRootObject(const IPCPartition &p, const std::string &server,
            const std::string &provider, const std::string &object, int tag = -1);

    /*!
     Call this function to get a particular efficiency object from the OH repository.
     \param partition TDAQ IPC partition
     \param name full efficiency name which includes dot separated server name, provider name and object name
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootEfficiency object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootEfficiency
     */
    static OHRootEfficiency getRootEfficiency(const IPCPartition &p, const std::string &name,
            int tag = -1);

    /*!
     Call this function to get a particular histogram from the OH repository.
     \param partition TDAQ IPC partition
     \param name full histogram name which includes dot separated server name, provider name and histogram name
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootHistogram object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootHistogram
     */
    static OHRootHistogram getRootHistogram(const IPCPartition &p, const std::string &name,
            int tag = -1);

    /*!
     Call this function to get a particular graph with 2 axes (TGraph-type) from the OH repository.
     \param partition TDAQ IPC partition
     \param name full graph name which includes dot separated server name, provider name and graph name
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootGraph object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootGraph
     */
    static OHRootGraph getRootGraph(const IPCPartition &p, const std::string &hname, int tag = -1);

    /*!
     Call this function to get a particular graph with 3 axes (TGraph2D-type) from the OH repository.
     \param partition TDAQ IPC partition
     \param name full graph name which includes dot separated server name, provider name and graph name
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootGraph2D object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception.
     \sa OHRootGraph2D
     */
    static OHRootGraph2D getRootGraph2D(const IPCPartition &p, const std::string &name,
            int tag = -1);

    /*!
     Call this function to get a ROOT object derived from TObject from the OH repository.
     At the moment this would be a histogram of type TH1 or a graph of type TGraph or TGraph2D.
     \param partition TDAQ IPC partition
     \param name full object name which includes dot separated server name, provider name and graph name
     \param tag objects tag. If -1 value is given (the default) then takes the most
     recent value from the repository.
     \return Returns the OHRootObject object if the corresponding object value
     exists in the OH repository, otherwise it will throw the daq::oh::ObjectNotFound
     exception. The actual TObject is in the OHRootObject::object attribute of the returned
     OHRootObject.
     \sa OHRootObject
     */
    static OHRootObject getRootObject(const IPCPartition &p, const std::string &name, int tag = -1);

    //! Called when a ROOT histogram is received
    /*!
     Override this function in order to receive ROOT histograms. User can
     take the ownership of the received histogram by storing the return value
     of the OHRootHistogram::histogram.release() operation. This call returns the
     pointer to the H1 object. User will be responsible for calling the delete
     operator when this histogram will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the histogram without taking the ownership then he has
     to use the OHRootHistogram::histogram.get() operation, which returns a pointer
     to the H1 object. The histogram in this case will be destroyed automatically
     at exit from this function.
     \param histogram The received OHRootHistogram object
     \sa OHRootHistogram
     */
    virtual void receive(OHRootHistogram &histogram) {
    }

    /*! Called when a vector of ROOT histograms is received
     Override this function in order to receive a vector of ROOT histograms. User can
     take the ownership of the received histograms by storing the return values
     of the OHRootHistogram::histogram.release() operations. This call returns the
     pointer to the H1 object. User will be responsible for calling the delete
     operator when this histogram will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the histograms without taking the ownership then he has
     to use the OHRootHistogram::histogram.get() operation, which returns a pointer
     to the H1 object. The histograms in this case will be destroyed automatically
     at exit from this function.
     \param histograms The received set of OHRootHistogram objects
     \sa OHRootHistogram
     */
    virtual void receive(std::vector<OHRootHistogram*> &histograms) {
    }

    //! Called when a ROOT efficiency object is received
    /*!
     Override this function in order to receive ROOT efficiencies. User can
     take the ownership of the received object by storing the return value
     of the OHRootEfficiency::efficiency.release() operation. This call returns the
     pointer to the TEfficiency object. User will be responsible for calling the delete
     operator when this object will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the efficiency without taking the ownership then he has
     to use the OHRootEfficiency::efficiency.get() function, which returns a pointer
     to the TEfficiency object. The object in this case will be destroyed automatically
     at exit from this function.
     \param efficiency The received OHRootEfficiency object
     \sa OHRootEfficiency
     */
    virtual void receive(OHRootEfficiency & efficiency) {
    }

    //! Called when a vector of ROOT efficiency object is received
    /*!
     Override this function in order to receive a vector of ROOT efficiencies. User can
     take the ownership of the received object by storing the return value
     of the OHRootEfficiency::efficiency.release() operation. This call returns the
     pointer to the TEfficiency object. User will be responsible for calling the delete
     operator when this object will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the efficiency without taking the ownership then he has
     to use the OHRootEfficiency::efficiency.get() function, which returns a pointer
     to the TEfficiency object. The object in this case will be destroyed automatically
     at exit from this function.
     \param efficiencies The received OHRootEfficiency object
     \sa OHRootEfficiency
     */
    virtual void receive(std::vector<OHRootEfficiency*> &efficiencies) {
    }

    /*! Called when a ROOT graph is received
     Override this function in order to receive ROOT graphs of type TGraph (with 2 axes). User can
     take the ownership of the received graphs by storing the return value
     of the OHRootGraph::histogram.release() operation. The name OHRootGraph::histogram.release()
     is not a mistake, this is because the internal classes OHRootGraph and OHRootHistogram are
     the same. The call returns the pointer to the H1 object. User will be
     responsible for calling the delete
     operator when the graph will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the graph without taking the ownership then he has
     to use the OHRootGraph::histogram.get() operation, which returns a pointer
     to the H1 object. The graph in this case will be destroyed automatically
     at exit from this function.
     \param graph The received OHRootGraph object
     \sa OHRootGraph
     */
    virtual void receive(OHRootGraph &graph) {
    }

    /*! Called when a vector of ROOT graphs is received
     Override this function in order to receive a set ROOT graphs of type TGraph (with 2 axes). User can
     take the ownership of the received graphs by storing the return value
     of the OHRootGraph::graph.release() operation. The call returns the pointer to the
     TGraph object. User will be responsible for calling the delete
     operator when the graph will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the graph without taking the ownership then he has
     to use the OHRootGraph::graph.get() operation, which returns a pointer
     to the TGraph object. The graph in this case will be destroyed automatically
     at exit from this function.
     \param graphs The received set of OHRootGraph
     \sa OHRootGraph
     */
    virtual void receive(std::vector<OHRootGraph*> &graphs) {
    }

    /*! Called when a ROOT graph is received
     Override this function in order to receive ROOT graphs of type TGraph2D (with 3 axes). User can
     take the ownership of the received graphs by storing the return value
     of the OHRootGraph2D::graph.release() operation. The call returns the pointer to the TGraph2D
     object. User will be responsible for calling the delete
     operator when the graph will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the graph without taking the ownership then he has
     to use the OHRootGraph2D::graph.get() operation, which returns a pointer
     to the TGraph2D object. The graph in this case will be destroyed automatically
     at exit from this function.
     \param graph The received OHRootGraph2D object
     \sa OHRootGraph
     */
    virtual void receive(OHRootGraph2D &graph) {
    }

    /*! Called when a vector of ROOT graphs is received
     Override this function in order to receive a set ROOT graphs of type TGraph2D (with 3 axes). User can
     take the ownership of the received graphs by storing the return value
     of the OHRootGraph2D::graph.release() operation. The call returns the pointer to the TGraph2D
     object. User will be responsible for calling the delete
     operator when the graph will not be used anymore. Failing to do this will
     result in memory leak.
     If one needs to use the graph without taking the ownership then he has
     to use the OHRootGraph2D::graph.get() operation, which returns a pointer
     to the TGraph2D object. The graph in this case will be destroyed automatically
     at exit from this function.
     \param graphs The received set of OHRootGraph2D
     \sa OHRootGraph2D
     */
    virtual void receive(std::vector<OHRootGraph2D*> &graphs) {
    }

private:

    //! Called when a histogram is received
    /*!
     \param histogram A histogram.
     \param name A histogram name.
     */
    void receive_(const std::string &name, oh::HistogramData<char> &histogram);
    void receive_(const std::string &name, oh::HistogramData<short> &histogram);
    void receive_(const std::string &name, oh::HistogramData<int> &histogram);
    void receive_(const std::string &name, oh::HistogramData<float> &histogram);
    void receive_(const std::string &name, oh::HistogramData<double> &histogram);
    void receive_(const std::string &name, oh::ProfileData &profile);
    void receive_(const std::string &name, oh::GraphData &graph);
    void receive_(const std::string &name, oh::Graph2DData &graph);
    void receive_(const std::string &name, oh::EfficiencyData &efficiency);

    //! Called when a set of histograms is received
    /*!
     \param name A histogram name.
     \param set A std::vector of recent values of this histogram.
     */
    void receive_(const std::string &name, std::vector<oh::HistogramData<char> > &set);
    void receive_(const std::string &name, std::vector<oh::HistogramData<short> > &set);
    void receive_(const std::string &name, std::vector<oh::HistogramData<int> > &set);
    void receive_(const std::string &name, std::vector<oh::HistogramData<float> > &set);
    void receive_(const std::string &name, std::vector<oh::HistogramData<double> > &set);
    void receive_(const std::string &name, std::vector<oh::ProfileData> &set);
    void receive_(const std::string &name, std::vector<oh::GraphData> &graph);
    void receive_(const std::string &name, std::vector<oh::Graph2DData> &graph);
    void receive_(const std::string &name, std::vector<oh::EfficiencyData> &set);
};

#endif // OH_ROOT_RECEIVER_H

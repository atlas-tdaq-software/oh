/*
    OHSubscriber.h - OH Public header file.
    
    Fredrik Sandin, Oct 2001
    Serguei Kolos,  Apr 2006
*/

#ifndef OH_HISTOGRAMSUBSCRIBER_H
#define OH_HISTOGRAMSUBSCRIBER_H

#include <is/inforeceiver.h>
#include <owl/regexp.h>
#include <oh/interface/OHReceiver.h>

//! OH objects subscriber
/*!
    This class provides functionality to subscribe for histograms with
    specific attributes.
    \sa OHReceiverIterator
*/
class OHSubscriber : public ISInfoReceiver
{
public:

    //! Create a new histogram subscriber
    OHSubscriber( const IPCPartition & p,
		  const std::string & server,
		  OHReceiver & receiver,
		  bool serialise_callbacks = true );
				
    //! Create a new histogram subscriber
    OHSubscriber( const IPCPartition & p,
		  OHReceiver & receiver,
		  bool serialise_callbacks = true );
				
    void subscribe( const std::string & name, 
		    bool provide_histogram_value = true ) ;
                    
    void subscribe( const std::string & provider, const std::string & histoname, 
		    bool provide_histogram_value = true ) ;
                    
    void subscribe( const std::string & server, const std::string & provider, const std::string & histoname, 
		    bool provide_histogram_value = true ) ;
                    

    void subscribe( const std::string & server, const OWLRegexp & provider, 
		    bool provide_histogram_value = true ) ;
                    
    void subscribe( const std::string & server, const OWLRegexp & provider, const OWLRegexp & histoname, 
		    bool provide_histogram_value = true ) ;
                    
    void subscribe( const OWLRegexp & provider, const OWLRegexp & histoname, 
		    bool provide_histogram_value = true ) ;
                    

    void unsubscribe( const std::string & name )
			;
    
    void unsubscribe( const std::string & provider, const std::string & histoname )
			;
    
    void unsubscribe( const std::string & server, const std::string & provider, const std::string & histoname )
			;
    
    void unsubscribe( const std::string & name, const OWLRegexp & provider )
			;
                        
    void unsubscribe( const OWLRegexp & provider, const OWLRegexp & histoname )
			;
    
    void unsubscribe( const std::string & server, const OWLRegexp & provider, const OWLRegexp & histoname )
			;

private:

    //! IS callbacks
    void is_value_callback( ISCallbackInfo  * isc );
    void is_event_callback( ISCallbackEvent * ise );
    
    OHReceiver &	receiver_;
    std::string		server_;
};

#endif // OHHISTOGRAMSUBSCRIBER_H

/*
    OHIterator.h - OH Public header file.
    
    Serguei Kolos,  Apr 2006
*/

#ifndef OH_STREAM_H
#define OH_STREAM_H

#include <oh/core/Histogram.h>
#include <oh/core/Graph.h>
#include <oh/core/Graph2D.h>

#include <oh/internal/stream.h>


//! OH objects stream
/*!
    \class OHIterator
    This is a \b typedef for oh::stream class which can be used to select, 
    list and retrieve any objects from the OH.
*/
typedef oh::stream<oh::Object>	OHStream;

//! OH histograms stream
/*!
    \class OHHistogramStream
    This is a \b typedef for oh::stream class which can be used to select, 
    list and retrieve only histograms from the OH.
*/
typedef oh::stream<oh::Histogram>	OHHistogramStream;

//! OH efficiency objects stream
/*!
    \class OHEfficiencyStream
    This is a \b typedef for oh::stream class which can be used to select,
    list and retrieve only efficiency objects from the OH.
*/
typedef oh::stream<oh::Efficiency>       OHEfficiencyStream;

//! OH graphs stream
/*!
    \class OHGraphStream
    This is a \b typedef for oh::stream class which can be used to select, 
    list and retrieve only graphs from the OH.
*/
typedef oh::stream<oh::Graph>		OHGraphStream;

//! OH 1D graphs stream
/*!
    \class OHGraph1DStream
    This is a \b typedef for oh::stream class which can be used to select, 
    list and retrieve only 1-dimensional graphs from the OH.
*/
typedef oh::stream<oh::Graph,true>	OHGraph1DStream;

//! OH 2D graphs stream
/*!
    \class OHGraph2DStream
    This is a \b typedef for oh::stream class which can be used to select, 
    list and retrieve only 2-dimensional graphs from the OH.
*/
typedef oh::stream<oh::Graph2D,true>	OHGraph2DStream;

#endif

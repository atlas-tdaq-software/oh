/*
    oh_ls.cxx
    
    A simple utility which list all providers and objects in the OH.
    
    Serguei Kolos, Mar 2007

*/

#include <iostream>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>

template <class Iterator>
int 
show_objects(	const IPCPartition & partition, 
		const std::string & server_name, 
		const std::string & provider_name,
		const std::string & object_name,
		const std::string & type_name )
{
    try
    {
	OHProviderIterator pit( partition, server_name, provider_name );

        std::cout << pit.entries() << " OH providers found on server '" << server_name << "' in partition '" << partition.name() << "':" << std::endl;
	while ( pit++ ) {
	    std::cout << "    " << pit.name( ) << "\t" << ( pit.isActive() ? "[alive]" : "[absent]" );

	    if ( object_name.empty() )
            {
            	std::cout << std::endl;
                continue;
            }

	    Iterator it( partition, server_name, pit.name(), object_name );
	    std::cout << " contains " << it.entries() << " " << type_name << "(s)" << std::endl;
	    while ( it() )
	    {
		std::cout << "\t" << it.name() << "\t" << it.time() << std::endl;
	    }
	}
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    return 0;
}

template <class Iterator>
int 
show_all_objects(	const IPCPartition & partition, 
                        const std::string & provider_name,
                        const std::string & object_name,
                        const std::string & type_name )
{
    int result = 0;
    try
    {
	OHServerIterator it( partition );

	while ( it++ )
        {
	    result += show_objects<Iterator>( partition, it.name(), provider_name, object_name, type_name );
	}
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    return 0;
}

int main( int argc, char ** argv )
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }

    CmdArgStr partition_name( 'p', "partition", "partition_name", "Partition name" );
    CmdArgStr server_name( 's', "server", "server_name", "OH (IS) server name" );
    CmdArgStr provider_name( 'n', "provider", "provider_name", "OH provider name (regular expressions allowed)" );
    CmdArgStr object_name( 'o', "object", "object_name", "OH object name (regular expressions allowed)" );
    CmdArgChar object_type( 't', "type", "object_type",	"OH object type\n"
    				"allowed types are: 'H' - histograms, 'G' - graphs, 'E' - efficiency, 'A' - all (default)" );
                                                         
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &object_name, &object_type, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( "Display OH providers and OH objects (histograms and graphs)" );
    
    object_name = ".*";
    provider_name = ".*";
    object_type = 'A';
    cmd.parse(arg_iter);
    
    IPCPartition partition;
    
    if ( partition_name.flags() && CmdArg::GIVEN )
    {
    	partition = IPCPartition( (const char*)partition_name );
    }
    
    if ( server_name.flags() && CmdArg::GIVEN )
    {
	switch( object_type )
	{
	    case 'A':
		return show_objects<OHIterator>( partition,
		        (const char*)server_name,
			(const char*)provider_name,
			(const char*)object_name,
			"object" );
            case 'E':
                return show_objects<OHEfficiencyIterator>( partition,
                        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                         "efficiency" );
            case 'G':
                return show_objects<OHGraphIterator>( partition,
                        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "graph" );
	    case 'H':
		return show_objects<OHHistogramIterator>( partition,
		        (const char*)server_name,
			(const char*)provider_name,
			(const char*)object_name,
			"histogram" );
	    default:
		break;
	}
    }
    else
    {
	switch( object_type )
	{
	    case 'A':
		return show_all_objects<OHIterator>( partition,
		        (const char*)provider_name,
			(const char*)object_name,
			"object" );
            case 'E':
                return show_all_objects<OHEfficiencyIterator>( partition,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "efficiency" );
            case 'G':
                return show_all_objects<OHGraphIterator>( partition,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "graph" );
	    case 'H':
		return show_all_objects<OHHistogramIterator>( partition,
		        (const char*)provider_name,
			(const char*)object_name,
			"histogram" );
	    default:
		break;
	}
    }
    
    ers::error( daq::oh::InvalidParameter( ERS_HERE, object_type ) );
    return 1;
}

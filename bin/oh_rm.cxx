/*
    oh_rm.cxx
    
    A simple utility which removes all histograms from the OH.
    
    Serguei Kolos, Mar 2007

*/

#include <functional>
#include <iostream>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/threadpool.h>

#include <oh/core/Efficiency.h>
#include <oh/core/Histogram.h>
#include <oh/core/Graph.h>
#include <oh/core/Graph2D.h>

#include <oh/exceptions.h>

#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHUtil.h>

namespace
{
    // Must be atomic, but race condition is exremely unlikely and does not harm
    int ErrorCounter = 0;
}

template <class T>
void 
remove_objects(	const IPCPartition & partition, 
		const std::string & server_name, 
		const std::string & provider_name,
		const std::string & object_name,
		const std::string & type_name )
{
    try
    {
	std::cout << "removing " << type_name
	    << " objects from the '" << server_name << "' server ... ";

	ISInfoDictionary id(partition);
	ISCriteria c( ~T::type() && oh::util::create_regex( provider_name, object_name ) );
	id.removeAll( server_name, c );
	std::cout << "done" << std::endl;
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
	std::cout << "failed" << std::endl;
        ++ErrorCounter;
    }
}

template <class T>
int 
remove_all_objects(	const IPCPartition & partition, 
                        const std::string & server_name,
                        const std::string & provider_name,
                        const std::string & object_name,
                        const std::string & type_name )
{    
    try
    {
	OHServerIterator it( partition, server_name );

	IPCThreadPool p( it.entries() );
        while ( it++ )
        {
	    p.addJob( std::bind(
            	remove_objects<T>, 
                partition, it.name(), provider_name, object_name, type_name ) );
	}
        
        p.waitForCompletion();
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
	return 1;
    }
    
    return ErrorCounter;
}

int main( int argc, char ** argv )
{
    try {
	std::list< std::pair< std::string, std::string > > options = IPCCore::extractOptions( argc, argv );
	options.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("20000") ) );
        IPCCore::init( options );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }

    CmdArgStr partition_name( 'p', "partition", "partition_name", "Partition name" );
    CmdArgStr server_name( 's', "server", "server_name", "OH (IS) server name (regular expressions allowed)", CmdArg::isREQ );
    CmdArgStr provider_name( 'n', "provider", "provider_name", "OH provider name (regular expressions allowed)", CmdArg::isREQ );
    CmdArgStr object_name( 'o', "object", "object_name", "OH object name (regular expressions allowed)", CmdArg::isREQ );
    CmdArgChar object_type( 't', "type", "object_type",	"OH object type\n"
    			"allowed types are: 'H' - histograms, 'G' - graphs, 'E' - efficiency, 'A' - all (default)" );
                                                         
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &object_name, &object_type, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( "Removes histograms (or graphs) from the OH service" );
    
    object_type = 'A';
    cmd.parse(arg_iter);
    
    IPCPartition partition;
    
    if ( partition_name.flags() && CmdArg::GIVEN )
    {
    	partition = IPCPartition( (const char*)partition_name );
    }
    
    switch( object_type )
    {
	case 'A':
	    return remove_all_objects<oh::Object>( partition,
                                (const char*)server_name,
                                (const char*)provider_name,
				(const char*)object_name,
				"object" );
        case 'E':
            return remove_all_objects<oh::Efficiency>(  partition,
                                (const char*)server_name,
                                (const char*)provider_name,
                                (const char*)object_name,
                                "efficiency" );
        case 'G':
            return remove_all_objects<oh::Graph>( partition,
                                (const char*)server_name,
                                (const char*)provider_name,
                                (const char*)object_name,
                                "graph" );
	case 'H':
	    return remove_all_objects<oh::Histogram>( partition,
                                (const char*)server_name,
                                (const char*)provider_name,
				(const char*)object_name,
				"histogram" );
	default:
            break;
    }
    
    ers::error( daq::oh::InvalidParameter( ERS_HERE, object_type ) );
    return 1;
}

/*
    test_provider.cxx
    
    A RAW provider test application.
    
    Serguei Kolosn, June 2005

*/
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <iostream>

#include <cmdl/cmdargs.h>
#include <owl/timer.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <oh/OHRawProvider.h>

using namespace std;

CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
CmdArgStr provider_name( 'n', "provider", "provider_name",
                              "Histogram provider name", CmdArg::isREQ );
CmdArgStr histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram type name", CmdArg::isREQ );
CmdArgBool	quiet ('q', "quiet", "print only summary lines to standard error.");
CmdArgBool	update('u', "update", "publish histograms with update mod");

typedef char OH_STRING[256];

template <class T>
void test( OHRawProvider<> * provider, int histogram_number, int iterations, int size, int delay )
{
    // Sample data, this should be retrieved from somewhere in a real app
    T * contents = new T[size];
    char  * errors = new char[size];
    float * axis = new float[size-2];

    for ( int j = 0; j < size-2; j++ )
    {
    	contents[j] = j*j;
    	errors[j] = j;
        axis[j] = j;
    }
    // Publish a sample 1D histogram with variable width bins, errors,
    // underflow and overflow
        
    double * data = new double[ histogram_number * iterations ];
    double   tmin = 1000000.;
    double   tmax = 0.;
    int	     count = 0;
    
    OH_STRING * names = new OH_STRING[histogram_number]; 
    for( int n = 0; n < histogram_number; n++ )
    {
    	sprintf(names[n],"%s.%d",(const char*)histogram_name,n);
    }
    
    for( int i = 0; i < iterations; i++ )
    {
	for( int n = 0; n < histogram_number; n++ )
	{
	    OWLTimer t;
	    t.start();
	    provider->publish( names[n], "RAW Histogram 1D",
				    OHAxis( "X Axis", size-3, axis ),
				    contents,
				    errors,
				    true,
                                    update );
	    t.stop();
	    data[count++] = t.totalTime();

	    tmin = ( tmin < t.totalTime() ) ? tmin : t.totalTime();
	    tmax = ( tmax > t.totalTime() ) ? tmax : t.totalTime();

	    if ( !quiet )
	    {
		std::cout << count << "\t" << t.totalTime() * 1000 << std::endl;
	    }
	}
	if ( delay > 0 )
	    sleep( delay );
    }
        
    delete[] data;
}

class MyCommandListener : public OHCommandListener
{
  public:
    void command ( const std::string & name, const std::string & cmd )
    {
	std::cout << this << ": Command " << cmd << " received for the " << name << " histogram" << std::endl;
    }

    void command ( const std::string & cmd )
    {
	std::cout << this << ": Command " << cmd << " received for all histograms" << std::endl;
    }
};

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    CmdArgInt   pnum ('N', "Provider", "number", "number of providers");
    CmdArgInt   hnum ('H', "Histogram", "number", "number of histograms");
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &update, &quiet, &pnum, &hnum, NULL );
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH RAW provider example" );
        
    cmd.parse(arg_iter);
    
    /////////////////////////////////////////////////////////////////////////
    // Create an OHRawProvider and publish some sample histograms
    /////////////////////////////////////////////////////////////////////////
    
    try
    {
	IPCPartition partition( partition_name );
	MyCommandListener lst;
	OHRawProvider<> * raw = new OHRawProvider<>( partition, (const char*)server_name, (const char*)provider_name, &lst );

	test<int>( raw, 5, 1, 100, 1 );

	OHRawProvider<> * raw1 = new OHRawProvider<>( partition, (const char*)server_name, (const char*)provider_name, &lst );
	test<int>( raw1, 5, 1, 100, 1 );

	cout << "step 1 ..." << endl;
	sleep( 1 );
	delete raw;
	cout << "The OH RAW provider object \"" << provider_name << "\" has been destroyed." << endl;

	cout << "step 2 ..." << endl;
	sleep( 1 );
	delete raw1;
	cout << "The OH RAW provider object \"" << provider_name << "\" has been destroyed." << endl;

	MyCommandListener lst2;
	{
	    OHRawProvider<> raw2( partition, (const char*)server_name, (const char*)provider_name, &lst2 );
	    test<int>( &raw2, 5, 1, 100, 1 );

	    OHRawProvider<> * raw3 = new OHRawProvider<>( partition, (const char*)server_name, (const char*)provider_name, &lst2 );

	    test<int>( raw3, 5, 1, 100, 1 );

	    cout << "step 3 ..." << endl;
	    sleep( 1 );
	    cout << "The OH RAW provider object \"" << provider_name << "\" has been destroyed." << endl;
	}
    
	std::cout << "OH test provider \"" << provider_name << "\" has been started in the \""
		    << partition_name << "\" partition" << std::endl;

	
        for ( int i = 0; i < pnum; ++i )
        {
	    std::ostringstream out;
            out << (const char*)provider_name << "_" << i;
	    OHRawProvider<> * provider = new OHRawProvider<>( partition, (const char*)server_name, out.str() );
            test<int>( provider, hnum, 1, 100, 0 );
        }
        
        daq::ipc::signal::wait_for();
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
        return 1;
    }
    return 0;
}


/*
    test_provider.cxx
    
    A RAW provider test application.
    
    Serguei Kolos, June 2005

*/

#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <chrono>
#include <iostream>
#include <thread>

#include <cmdl/cmdargs.h>
#include <ers/ers.h>
#include <owl/timer.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <oh/OHRawProvider.h>

using namespace std;

OHRawProvider<> * provider;

void stat( double * data, int count, double & mean, double & dev );

CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
CmdArgStr provider_name( 'n', "provider", "provider_name",
                              "Histogram provider name", CmdArg::isREQ );
CmdArgStr histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram type name", CmdArg::isREQ );
CmdArgBool quiet ('q', "quiet", "print only summary lines to standard error.");
CmdArgBool update('u', "update", "publish histograms with update mod");
CmdArgBool no_summary ('U', "no-summary", "don't print summary lines to standard error.");
CmdArgInt scale ('S', "scale", "scale_factor", "bin range multiplication factor.");

typedef char OH_STRING[256];

void test( int histogram_number, int iterations, int size, int delay, int fill, int scale, unsigned int stime)
{
    // Sample data, this should be retrieved from somewhere in a real app
    double * data = new double[ histogram_number * iterations ];
    double   tmin = 1000000.;
    double   tmax = 0.;
    int	    errcnt = 0;
    int     count = 0;
    
    OH_STRING * names = new OH_STRING[histogram_number];
    for( int n = 0; n < histogram_number; n++ )
    {
    	sprintf(names[n],"%s.%d",(const char*)histogram_name,n);
    }
    std::cout << "total number of bins is " << histogram_number * size << std::endl;

    std::vector<int> bins(size, 1);
    
    namespace ch = std::chrono;
    auto passed = ch::system_clock::now().time_since_epoch()/ch::seconds(1);

    if (stime > passed) {
        std::this_thread::sleep_for(ch::seconds(stime - passed));
    }

    auto timeout = ch::system_clock::now();
    ERS_LOG("Start publishing");

    for( int i = 0; i < iterations; i++ )
    {
        ERS_LOG("start iteration #" << i);
	for( int n = 0; n < histogram_number; n++ )
	{
	    OWLTimer t;
	    t.start();
            if ( !quiet )
            	std::cout << provider_name << ": publishing '" << names[n] << "' histogram" << std::endl;
            provider->publish<int, int>(names[n], names[n], OHAxis("X Axis", size, 0, 100),
                &bins[0], nullptr, false);
	    t.stop();
	    data[count++] = t.totalTime();

	    tmin = ( tmin < t.totalTime() ) ? tmin : t.totalTime();
	    tmax = ( tmax > t.totalTime() ) ? tmax : t.totalTime();

	    if ( !quiet )
		std::cout << count << "\t" << t.totalTime() * 1000 << std::endl;
	}
        ERS_LOG("finished iteration #" << i);

	if ( delay > 0 ) {
	    timeout += ch::seconds(delay);
	    std::this_thread::sleep_until(timeout);
	}
    }
        
    if ( !no_summary )
    {
    	double mean, dev;
	
	stat( data, histogram_number * iterations, mean, dev );
	std::cerr << "IS test finished (pid = " << getpid() << ") : " ;
	std::cerr << count << " publish done, " << errcnt << " publish failed : " << std::endl;
	
	std::cerr << "{ min/avg/max/dev = " << tmin * 1000. << "/"
				       << mean * 1000. << "/" 
				       << tmax * 1000. << "/" 
				       << dev  * 1000. << std::endl;
    }

    delete[] data;
    delete[] names;
}

void stat( double * data, int count, double & mean, double & dev )
{
    int i;
    mean = 0.;

    for ( i = 0; i < count; i++ )
    {
	mean += data[i];
    }
    mean /= count;

    dev = 0.;

    for ( i = 0; i < count; i++ )
    {
	dev += ( data[i] - mean ) * ( data[i] - mean );
    }
    dev /= count;

    dev = sqrt( dev );
}

class MyCommandListener : public OHCommandListener
{
  public:
    void command ( const std::string & name, const std::string & cmd )
    {
	std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
    }

    void command ( const std::string & cmd )
    {
	std::cout << " Command " << cmd << " received for all histograms" << std::endl;
	if ( cmd == "exit" )
	{
	    daq::ipc::signal::raise();
	}
        else
        {
            int hnum, rnum, size, delay, fill;
            unsigned int stime;
            sscanf( cmd.c_str(), "%d %d %d %d %d %u", &hnum, &rnum, &size, &delay, &fill, &stime );
            std::cerr << "Start publishing " << hnum << " histograms of size " << size << " "
	    		<< rnum << " times with " << delay << " seconds delay at " << stime << std::endl;
            test( hnum, rnum, size, delay, fill, scale, stime );
        }
    }
};

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &update, &quiet, &no_summary, &scale, NULL );
    
    scale = 1;
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH RAW provider example" );
        
    cmd.parse(arg_iter);
    
    //
    // Create an OHRawProvider and publish some sample histograms
    /////////////////////////////////////////////////////////////////////////
    
    IPCPartition partition( partition_name );
    MyCommandListener lst;
    try
    {
    	provider = new OHRawProvider<>( partition, (const char*)server_name, (const char*)provider_name, &lst );
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
        return 1;
    }
                		 
    std::cout << "OH test provider \"" << provider_name << "\" has been started in the \"" 
    		<< partition_name << "\" partition" << std::endl;
                
    daq::ipc::signal::wait_for();
    return 0;
}


/*
    test_iterator.cxx
    
    Test application for OH iterators.
    
    Serguei Kolos, Dec 2006

*/

#include <unistd.h>
#include <math.h>
#include <iostream>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <owl/timer.h>

#include <oh/OHRootProvider.h>
#include <oh/OHIterator.h>

#include <TGraph.h>
#include <TGraph2D.h>
#include <TRandom.h>

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    CmdArgStr partition_name( 'p', "partition", "partition_name",
				  "Partition name", CmdArg::isREQ );
                                  
    CmdArgStr server_name( 's', "server", "server_name",
			       "OH (IS) Server name", CmdArg::isREQ );
                               
    CmdArgInt objects_number( 'n', "number", "objects_number",
				  "Number of OH objects to publish (default is 10)" );

    CmdArgStr provider_name( 'P', "provider", "provider_name",
                              "Histogram provider name", CmdArg::isREQ );

    CmdLine cmd( *argv, &partition_name, &server_name, &objects_number, &provider_name, NULL );
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH test iterators" );
    
    objects_number = 10;
        
    cmd.parse(arg_iter);
    
    //
    // Create an OHRootProvider and publish some sample objects
    /////////////////////////////////////////////////////////////////////////
    
    IPCPartition partition( partition_name );
    
    int count = 10;   
    double * x = new double[int(count)];
    double * y = new double[int(count)];
    double * z = new double[int(count)];
    for (int i=0;i<int(count);i++) {
	x[i]=100*double(random())/RAND_MAX;
	y[i]=100*double(random())/RAND_MAX;
	z[i]=100*double(random())/RAND_MAX;
    }
    
    try
    {
    	OHRootProvider * provider = new OHRootProvider( partition, (const char*)server_name, (const char*)provider_name );
        for ( int i = 0; i < objects_number; i++ )
        {
	    char name[32];
            sprintf( name, "TH1I histogram %d", i );
            TH1I * histo = new TH1I( name, name, gRandom->Integer( 100 ) + 10, -2, 2 );
            provider->publish( *histo, name );
        }
        
        for ( int i = 0; i < objects_number; i++ )
        {
	    char name[32];
            sprintf( name, "TH1F histogram %d", i );
            TH1F * histo = new TH1F( name, name, gRandom->Integer( 100 ) + 10, -2, 2 );
            provider->publish( *histo, name );
        }
        
        for ( int i = 0; i < objects_number; i++ )
        {
	    char name[32];
            sprintf( name, "TH1D histogram %d", i );
            TH1D * histo = new TH1D( name, name, gRandom->Integer( 100 ) + 10, -2, 2 );
            provider->publish( *histo, name );
        }
        
        for ( int i = 0; i < objects_number; i++ )
        {
	    char name[32];
            sprintf( name, "graph %d", i );
            TGraph * graph = new TGraph( count, x, y );
            provider->publish( *graph, name );
        }
        
        for ( int i = 0; i < objects_number; i++ )
        {
	    char name[32];
            sprintf( name, "graph2D %d", i );
            TGraph2D * graph = new TGraph2D( name, name, count, x, y, z );
            provider->publish( *graph, name );
        }
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
    }
    
    bool result = true;
    std::cout << "Testing generic iterator ... ";
    OHIterator it( partition, (const char*)server_name );
    std::cout << ( ( result = result && ( (int)it.entries() == ( objects_number * 5) ) ) ? "ok" : "failed" ) << std::endl;
    std::cout << "Object names are:" << std::endl;
    while ( it() )
    {
	std::cout << "\t" << it.name() << std::endl;
    }
            		 
    std::cout << "Testing histogram iterator ... ";
    OHHistogramIterator ith( partition, (const char*)server_name );
    std::cout << ( ( result = result && ( (int)ith.entries() == ( objects_number * 3) ) ) ? "ok" : "failed" ) << std::endl;
    std::cout << "Histogram names are:" << std::endl;
    while ( ith() )
    {
	std::cout << "\t" << ith.name() << std::endl;
    }
            		 
    std::cout << "Testing generic graph iterator ... ";
    OHGraphIterator itg( partition, (const char*)server_name );
    std::cout << ( ( result = result && ( (int)itg.entries() == ( objects_number * 2 ) ) ) ? "ok" : "failed" ) << std::endl;
    std::cout << "Graph names are:" << std::endl;
    while ( itg() )
    {
	std::cout << "\t" << itg.name() << std::endl;
    }
            		 
    std::cout << "Testing 1D graph iterator ... ";
    OHGraph1DIterator itg1( partition, (const char*)server_name );
    std::cout << ( ( result = result && ( (int)itg1.entries() == objects_number ) ) ? "ok" : "failed" ) << std::endl;
    std::cout << "Graph names are:" << std::endl;
    while ( itg1() )
    {
	std::cout << "\t" << itg1.name() << std::endl;
    }
            		 
    std::cout << "Testing 2D graph iterator ... ";
    OHGraph2DIterator itg2( partition, (const char*)server_name );
    std::cout << ( ( result = result && ( (int)itg2.entries() == objects_number ) ) ? "ok" : "failed" ) << std::endl;
    std::cout << "Graph2D names are:" << std::endl;
    while ( itg2() )
    {
	std::cout << "\t" << itg2.name() << std::endl;
    }
            		 
    return ( result ? 0 : 1 );
}


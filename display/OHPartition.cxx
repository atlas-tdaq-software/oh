/* 
   OHPartition.cxx

   Serguei Kolos April 2007
*/
#include <functional>
#include <mutex>
#include <sstream>

#include <TGMsgBox.h>

#include "OHPartition.h"
#include "OHProvider.h"
#include "OHConfigDialog.h"

#include <ipc/threadpool.h>

#include <oh/OHFileSaver.h>
#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>
#include <oh/OHCommandSender.h>

//ClassImp(OHPartition)

const TGPicture * 
OHPartition::GetSmallPicture() const
{
    static const TGPicture * picture = gClient->GetPicture("partition__16x16");
    return picture;
}

const TGPicture * 
OHPartition::GetLargePicture() const
{
    static const TGPicture * picture = gClient->GetPicture("partition__32x32");
    return picture;
}
  
void
OHPartition::Browse(TBrowser * browser)
{
    RemoveChildren();
    Refresh( browser, OHConfigDialog::ProviderRegex );    
    OHFolder::Explore( browser );
}

void 
OHPartition::updateProvidersForServer( const std::string & server, const std::string & mask, std::mutex & mutex )
{
    IPCPartition partition(GetName());
    OHProviderIterator pii( partition, server, mask );
    while ( pii++ )
    {
	std::unique_lock lock(mutex);
	AddChildItem( pii.name(), new OHProvider( pii.name(), server, pii.time(), this ), this );
    }
}

void
OHPartition::Refresh( TBrowser * , const std::string & mask )
{    
    IPCPartition partition(GetName());
    try
    {
        OHServerIterator sit( partition, OHConfigDialog::ServerRegex );

        std::mutex mutex;
        IPCThreadPool p( sit.entries() );
        while ( sit++ )
        {
            std::string s(sit.name());
            p.addJob( std::bind( &OHPartition::updateProvidersForServer, this, s, mask, std::ref(mutex) ) );
        }
        p.waitForCompletion();
    }
    catch ( ers::Issue & ex )
    {
        ERS_DEBUG( 0, ex );
    }
}

void 
OHPartition::SaveToFile( TGMainFrame * frame, const std::string & ) const
{
    std::string filename = GetName();
    filename = AskFileName( frame, filename );
    
    if ( filename.empty() )
    {
    	return ;
    }
    
    OHFileSaver receiver( filename );
  
    if ( receiver.error() )
    {
	std::ostringstream out;
	out << "Can not create '" << filename << "' file";
	std::string message = out.str();
	new TGMsgBox( gClient->GetRoot(), frame, "Error", message.c_str(), kMBIconExclamation );
	return ;
    }

    IPCPartition pp(GetName());
    try
    {
	OHServerIterator sii( pp, OHConfigDialog::ServerRegex );
	while ( sii++ )
	{
	    OHProviderIterator pii( pp, sii.name(), OHConfigDialog::ProviderRegex );
	    while ( pii++ )
	    {
		receiver.pushd( pii.name() );
		OHIterator it( pp, sii.name(), pii.name(), OHConfigDialog::ObjectRegex );

		while ( it++ )
		{
		    it.retrieve( receiver, true );
		}
		receiver.popd( );
	    }
	}
    }
    catch ( ers::Issue & ex )
    {
	ERS_DEBUG( 0, ex );
    }
}	

void
OHPartition::SendCommand( TGMainFrame * frame, const std::string & cmd ) const
{
    std::string command = cmd;
    if ( command.empty() )
    {
        std::ostringstream out;
        out << "Send command to all providers in '" << GetName() << "' partition";
        command = AskCommand( frame, out.str() );
    }
    
    if ( command.empty() )
    {
    	return;
    }

    IPCPartition partition( GetName() );
    OHCommandSender cs( partition );

    int error_count = 0;
    int command_count = 0;
    try
    {
	OHServerIterator sii( partition, OHConfigDialog::ServerRegex );
	while ( sii++ )
	{
	    OHProviderIterator pii( partition, sii.name(), OHConfigDialog::ProviderRegex );
	    while ( pii++ )
	    {
		++command_count;
                try {
		    cs.sendCommand( sii.name(), pii.name(), command );
		}
		catch( ers::Issue & ex ) {
		    ++error_count;
		}
	    }
	}
    }
    catch ( ers::Issue & ex )
    {
	std::string message = ex.what();
	new TGMsgBox( gClient->GetRoot(), frame, "Send Command Failed", message.c_str(), kMBIconExclamation );
        return;
    }
   
    if ( error_count )
    {
	std::ostringstream out;
	out << "Sending command '" << command << "' failed for " << error_count << " providers out of a total of " << command_count;
	std::string message = out.str();
    	new TGMsgBox( gClient->GetRoot(), frame, "Send Command Failed", message.c_str(), kMBIconExclamation );
    }
}

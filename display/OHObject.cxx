/* 
   OHObject.cxx

   Serguei Kolos, Jan 2004
*/

#include <TCanvas.h>
#include <TGMsgBox.h>
#include <TClass.h>

#include "OHCanvas.h"
#include "OHObject.h"
#include "OHProvider.h"
#include "OHPartition.h"
#include "OHRootBrowser.h"
#include "OHConfigDialog.h"

#include <oh/OHFileSaver.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHCommandSender.h>

//ClassImp(OHObject)

namespace
{
    std::string
    getDrawOption( const std::vector< std::pair<std::string,std::string> > & ann,
            OHObject::Type type )
    {
        for (unsigned i = 0; i < ann.size(); i++) {
            if (ann[i].first == "DrawOption") {
                return ann[i].second;
            }
        }
        return (type == OHObject::Graph ? "ALP" :
                type == OHObject::Graph2D ? "SURF1" : "");
    }
}

const TGPicture *
OHObject::GetSmallPicture( ) const
{
    static const TGPicture * g_picture = gClient->GetPicture("graph__16x16");
    static const TGPicture * g2_picture = gClient->GetPicture("graph_2d__16x16");
    static const TGPicture * h_picture = gClient->GetPicture("histogram__16x16");
    static const TGPicture * e_picture = gClient->GetPicture("profile__16x16");
    return (  GetType() == OHObject::Graph ? g_picture
            : GetType() == OHObject::Graph2D ? g2_picture
            : GetType() == OHObject::Efficiency ? e_picture : h_picture );
}

const TGPicture *
OHObject::GetLargePicture( ) const
{
    static const TGPicture * g_picture = gClient->GetPicture("graph__32x32");
    static const TGPicture * g2_picture = gClient->GetPicture("graph_2d__32x32");
    static const TGPicture * h_picture = gClient->GetPicture("histogram__32x32");
    static const TGPicture * e_picture = gClient->GetPicture("profile__32x32");
    return (  GetType() == OHObject::Graph ? g_picture
            : GetType() == OHObject::Graph2D ? g2_picture
            : GetType() == OHObject::Efficiency ? e_picture : h_picture );
}

OHObject::OHObject(	const std::string & name,
			        const OWLTime & time,
                    OHEntry::Type type,
                    int tag,
                    OHFolder * parent )
  : OHFolder( name, parent, time.c_time(), type ),
    tag_( tag )
{
    std::ostringstream out;
    out << GetTitle() << ";" << tag_;
    std::string title = out.str();
    SetTitle( title.c_str() );
}

void
OHObject::DrawObject( TPad & pad ) const
{
    pad.GetListOfPrimitives()->Add( object_.get(), options_.c_str() );
    pad.Modified( kTRUE );
    pad.Update();
}

void
OHObject::UpdateObject( std::unique_ptr<TObject> & object, int tag,
                        const std::vector<std::pair<std::string,std::string> > & annotations,
                        TPad & pad )
{
    auto copy_xyz_ranges = [](auto * n, auto * o) {
        n->GetXaxis()->SetRange( o->GetXaxis()->GetFirst(), o->GetXaxis()->GetLast() );
        n->GetYaxis()->SetRange( o->GetYaxis()->GetFirst(), o->GetYaxis()->GetLast() );
        n->GetZaxis()->SetRange( o->GetZaxis()->GetFirst(), o->GetZaxis()->GetLast() );
    };

    if (object->IsA()->InheritsFrom( TH1::Class() )) {
        if ( object->IsA() == object_->IsA() and object->Compare(object_.get()) == 0 ) {
            copy_xyz_ranges((TH1*)object.get(), (TH1*)object_.get());
        }
    }

    options_ = getDrawOption( annotations, GetType() );
    old_object_ = object_;
    pad.GetListOfPrimitives()->Remove( object_.get() );
    object_.reset(object.release());
    UpdateTitle( object_->GetName(), annotations );
    DrawObject( pad );
    tag_ = tag;
}

void
OHObject::UpdateTitle(	const char * name,
                        const std::vector<std::pair<std::string,std::string> > & annotations )
{
    std::string title = name;
    title += "[tag=";
    title += std::to_string(tag_);
    for ( size_t i = 0; i < annotations.size(); ++i )
    {
    	title += ",";
        title += annotations[i].first + "=" + annotations[i].second;
    }
    title += "]";
    ((TNamed*)object_.get())->SetTitle( title.c_str() );
}

void
OHObject::Browse( TBrowser * bb )
{
    OHRootBrowser * browser = (OHRootBrowser*)bb->GetBrowserImp();

    try
    {
        OHRootObject oo = OHRootReceiver::getRootObject(
                            IPCPartition( GetPartition() ),
                            GetServer(),
                            GetProvider(),
                            GetName(),
                            tag_ );
        object_.reset( oo.object.release() );
        options_ = getDrawOption( oo.annotations, GetType() );
        UpdateTitle( object_->GetName(), oo.annotations );
    }
    catch( ers::Issue & ex )
    {
        std::ostringstream out;
        out << "Can not get '" << GetName() << "' histogram.\nUse Refresh button to update the content of this folder";
        std::string message = out.str();
        new TGMsgBox( gClient->GetRoot(), gClient->GetRoot(), "Error", message.c_str(), kMBIconExclamation );
        return;
    }

    OHCanvas::DrawObject( *this, browser->GetCanvasMode() );
}

void
OHObject::SaveToFile( TGMainFrame * frame, const std::string & ) const
{
    std::string filename = GetPartition();
    filename += "-";
    filename += GetProvider();
    filename += "-";
    filename += GetName();
    filename = AskFileName( frame, filename );

    if ( filename.empty() )
    {
    	return;
    }

    OHFileSaver receiver( filename );

    if ( receiver.error() )
    {
        std::ostringstream out;
        out << "Can not create '" << filename << "' file";
        std::string message = out.str();
        new TGMsgBox( gClient->GetRoot(), frame, "Error", message.c_str(), kMBIconExclamation );
        return ;
    }

    IPCPartition pp( GetPartition() );
    try
    {
        receiver.getObject( pp, GetServer(), GetProvider(), GetName() );
    }
    catch ( ers::Issue & ex )
    {
        ERS_DEBUG( 0, ex );
    }
}

void
OHObject::SendCommand( TGMainFrame * frame, const std::string & cmd ) const
{
    std::string command = cmd;
    if ( command.empty() )
    {
        std::ostringstream out;
        out << "Send command to '" << GetName() << "' histogram";
        command = AskCommand( frame, out.str() );
    }

    if ( command.empty() )
    {
    	return;
    }

    IPCPartition partition( GetPartition() );
    OHCommandSender cs( partition );

    try {
        cs.sendCommand( GetServer(), GetProvider(), GetName(), command );
    }
    catch( ers::Issue & ex ) {
        new TGMsgBox( gClient->GetRoot(), frame, "Send Command Failed",
                "Histogram provider doesn't exist",
                kMBIconExclamation );
    }
}

#ifndef _OH_CONFIG_DIALOG_H
#define _OH_CONFIG_DIALOG_H

#include "TGButton.h" 
#include "TGLabel.h" 
#include "TGTextEntry.h"

#include <string>

class OHRootBrowser;

class OHConfigDialog : public TGTransientFrame
{
private:
    static bool initialised_;
    static bool ReadFromFile();
    static void WriteToFile();
    static std::string CreateFileName();

public:
    static std::string ConfigurationFileName;
    static std::string ServerRegex;
    static std::string ProviderRegex;
    static std::string ObjectRegex;
    static std::string Delimiters;
   
private:
    TList *		fGarbage;
    TGTextEntry * 	fServers;
    TGTextEntry * 	fProviders;
    TGTextEntry * 	fHistograms;
    TGTextEntry * 	fDelimiters;
    TGTextButton *	fOk;
    TGTextButton *	fCancel;
      
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
   
public:
    OHConfigDialog( TGMainFrame * main );
         
    virtual ~OHConfigDialog();

    void Popup();
};

#endif

/*
  OHEntry.h
  provides base class for all items which can be displayed by the OH display

  Serguei Kolos, April 2007
*/

#ifndef _OH_ENTRY_H_
#define _OH_ENTRY_H_

#include <string>

#include <TGPicture.h>
#include <TGListView.h>
#include <TGClient.h>

#include <owl/time.h>

class OHEntry : public TNamed
{ 
  public:
    enum Type { Folder, Graph, Graph2D, Histogram, Efficiency };
    
  private:
    OWLTime 	time_;
    std::string time_string_;
    Type	type_;
  
  public:
    OHEntry( const std::string & name = "", time_t seconds = 0, Type type = Folder )
      : TNamed( name.c_str(), name.c_str() ),
        time_( seconds ),
        time_string_( seconds ? time_.str() : std::string( "" ) ),
        type_( type )
    { ; }
    
    virtual ~OHEntry() = default;
    
    const OWLTime & GetTime() const { return time_; }
    const char * GetTimeString() const { return time_string_.c_str(); }
    Type GetType() const { return type_; }
    
    std::string AskFileName( TGMainFrame * frame, const std::string & filename ) const;
    std::string AskCommand( TGMainFrame * frame, const std::string & title ) const;
    
    virtual int GetTag() const = 0;
    virtual const char * GetServer( ) const = 0;
    virtual const char * GetProvider( ) const = 0;
    virtual const char * GetPartition( ) const = 0;
    virtual Bool_t CommandsAllowed() const { return kFALSE; }
    virtual void  SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const = 0;   
    virtual void  SendCommand( TGMainFrame * frame, const std::string & command ) const = 0;   
    virtual const TGPicture * GetSmallPicture( ) const = 0;   
    virtual const TGPicture * GetLargePicture( ) const = 0;   
    
    //    ClassDef( OHEntry, 1 )
};

class OHLVEntry: public TGLVEntry
{
    OHEntry * entry_;
    
  public:
    enum SortMode {
       kSortByName,
       kSortByTime,
       kSortByTag,
       kSortByType
    };
    
    OHLVEntry( TGLVContainer * container = 0, OHEntry * entry = 0, EListViewMode mode = kLVList )
      : TGLVEntry(	container, 
      			entry->GetLargePicture(), 
                        entry->GetSmallPicture(), 
                        new TGString( entry->GetTitle() ), 
                        0, 
                        mode ),
        entry_( entry )
    {
	// Add single character dummy string at the end to make sure
        // that the previous items are taken into account
        // If the first 2 items are empoty ROOT will ignore then which
        // may lead to a crash
        SetSubnames( entry_->GetTimeString(), entry_->GetServer(), " " );
    }
    
    ~OHLVEntry()
    {
    	;
    }
    
    OHEntry * entry() const { return entry_; }
    void Browse(TBrowser * b) { entry_->Browse( b ); }
    Bool_t IsFolder() const { return entry_->IsFolder(); }
    Int_t  CompareEntries( const OHLVEntry * entry, SortMode mode ) const;
    
    //    ClassDef( OHLVEntry, 1 )
};

#endif

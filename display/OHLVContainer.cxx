#include "OHLVContainer.h"
#include "OHRootBrowser.h"

#include <TGButton.h>
#include <TVirtualX.h>
namespace

{
    const UInt_t DefaultWidths[] = { 600, 200, 200 };
}

struct OHTextButton : public TGTextButton
{
    OHTextButton( const TGWindow * parent, const char * title, int idx, OHRootBrowser * browser )
      : TGTextButton( parent, new TGHotString( title ), idx, GetDefaultGC()(), GetDefaultFontStruct(), 
    			kRaisedFrame | kDoubleBorder | kFixedWidth ),
        fBrowser( browser ),
        fActive( kFALSE ),
        fAscending( kTRUE ),
        fWidth(idx < 3 ? DefaultWidths[idx] : 100)
    { 
    	Associate( this );
    }
    
    UInt_t GetDefaultWidth() const
    {
        return fWidth;
    }

    void SetText(const TString & s)
    {
        fClient->NeedRedraw(this);
    }

    void SetText(TGHotString * )
    {
        fClient->NeedRedraw(this);
    }
 
    Bool_t
    ProcessMessage( Long_t , Long_t p1, Long_t )
    {
        fBrowser->SetSortMode( (OHLVEntry::SortMode)p1 );
        return kTRUE;
    }
    
    void SetSortState( Bool_t active, Bool_t ascending = kTRUE )
    {
    	fActive = active;
        fAscending = ascending;
    }
    
    void DoRedraw()
    {
        TGTextButton::DoRedraw();
        
        if ( !fActive )
        {
            return;
        }
        
        UInt_t x = 10;
        UInt_t y = 5;
        
	    if (fState == kButtonDown || fState == kButtonEngaged) { ++x; ++y; }
        
	    if ( fAscending )
	    {
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 0, y + 5, x + 7, y + 5);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 1, y + 4, x + 6, y + 4);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 2, y + 3, x + 5, y + 3);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 3, y + 2, x + 4, y + 2);
        }
        else
        {
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 0, y + 2, x + 7, y + 2);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 1, y + 3, x + 6, y + 3);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 2, y + 4, x + 5, y + 4);
            gVirtualX->DrawLine(fId, GetShadowGC()(), x + 3, y + 5, x + 4, y + 5);
        }
    }
    
  private:
    OHRootBrowser *	fBrowser;
    Bool_t		fActive;
    Bool_t		fAscending;
    UInt_t		fWidth;
};

struct OHListView : public TGListView
{  
    OHListView( const TGWindow * parent, OHRootBrowser * browser )
      : TGListView( parent, 0, 0 ),
        fBrowser( browser )
    { ; }
    
    void SetHeader(const char* s, Int_t hmode, Int_t cmode, Int_t idx)
    {
        TGListView::SetHeader( s, hmode, cmode, idx );

        delete fColHeader[idx];
        fColHeader[idx] = new OHTextButton( fHeader, s, idx, fBrowser );
        fSplitHeader[idx]->SetFrame( fColHeader[idx], kTRUE );
    }
  
  private:
    
    OHRootBrowser * fBrowser;
};

OHLVContainer::OHLVContainer( TGCompositeFrame * parent, UInt_t w, UInt_t h, OHRootBrowser * browser )
  : TGLVContainer( parent, w, h, kHorizontalFrame, fgWhitePixel ),
    fSortMode( OHLVEntry::kSortByType ),
    fElementsClass( 0 )
{
    SetListView( new OHListView( parent, browser ) );
    fListView->SetContainer( this );
    ReparentWindow( fListView->GetViewPort() );
    parent->AddFrame( fListView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );
    
    SetColHeaders("Name", "Time", "Server");
}

void
OHLVContainer::AddItem( TGLVEntry * item )
{
    OHEntry * entry = ((OHLVEntry*)item)->entry();
    if ( !fElementsClass || typeid(*entry) != *fElementsClass )
    {
        fElementsClass = &typeid(*entry);
    }
    TGLVContainer::AddItem( item );
}

void
OHLVContainer::AddFrame(TGFrame *f, TGLayoutHints *l)
{
    Element * nw = new Element;
    nw->fFrame = f;
    nw->fLayout = l ? l : fgDefaultHints;
    nw->fState = 1;
    nw->fContainer = this;
    fList->Add(nw);
}

void
OHLVContainer::Update()
{
    SendMessage(fMsgWindow, MK_MSG(kC_CONTAINER, kCT_SELCHANGED), fTotal, fSelected);
    fListView->Layout();
    fClient->NeedRedraw( this );
    MapSubwindows();
}

void
OHLVContainer::Refresh()
{
    Sort( fSortMode, kTRUE );
    Update();
}

void
OHLVContainer::Sort( OHLVEntry::SortMode mode, Bool_t ascending )
{
    fSortMode = mode;
    
    TGTextButton ** headers = fListView->GetHeaderButtons();
    for ( UInt_t i = 0; i < fListView->GetNumColumns() - 1; ++i )
    {
        ((OHTextButton*)headers[i])->SetSortState( (OHLVEntry::SortMode)i == mode, ascending );
    }
    
    fList->Sort( ascending );
    Update();
}


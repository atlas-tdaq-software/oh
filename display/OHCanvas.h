#ifndef OH_CANVAS_H
#define OH_CANVAS_H

#include <memory>
#include <vector>

#include <TCanvas.h>

#include "OHObject.h"
#include "OHRootBrowser.h"

class OHPad;

class OHCanvas : public TCanvas
{  
  friend class OHPad;
  
  public:
      
    static void DrawObject( const OHObject & object, OHRootBrowser::ECanvasMode canvas_mode );
        
  private:
    OHCanvas( const OHObject & object );
    OHCanvas( );
    ~OHCanvas( );
    
    void Rebuild( const OHObject & object );
    void AddPad( const OHObject & object );
    
    static OHCanvas * GetLast( );

  private:
    static OHCanvas * s_last;

  private:
    std::vector<std::shared_ptr<OHPad> >	pads_;
};

#endif

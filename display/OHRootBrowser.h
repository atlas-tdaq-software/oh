//////////////////////////////////////////
// This code is based on TRootBrowser.h
// (c) by the ROOT team.
//
// (c) Monika Barczyk and Piotr Golonka,
//    April, 2002
// (c) Serguei Kolos
//    April, 2007
//////////////////////////////////////////

#ifndef _OH_ROOT_BROWSER_
#define _OH_ROOT_BROWSER_

#include "OHEntry.h"
#include "OHLVContainer.h"
#include "OHContextMenu.h"
#include "OHPictures.h"

#include <TBrowserImp.h>
#include <TGFSComboBox.h>
#include <TGFrame.h>
#include <TRootApplication.h>
#include <TGCanvas.h>
#include <TGMenu.h>
#include <TGStatusBar.h>
#include <TGLabel.h>
#include <TGListTree.h>
#include <TGToolBar.h>
#include <TG3DLine.h>
#include <TStyle.h>

class OHRootBrowser : public TGMainFrame, public TBrowserImp 
{
    friend class OHEntry;
    
  public:      
    enum ECanvasMode { kSingleCanvas = 10001, kNewCanvas, kMultiPad };
    
  private:
    OHPictures *		fPictures;
    TList *			fGarbage;
    TGHorizontal3DLine *	fToolBarSep;
    TGToolBar *			fToolBar;
    TGStatusBar *		fStatusBar;
    OHLVContainer *		fIconBox;
    TGListTree *		fListTree;
    
    OHContextMenu *		fDefaultMenu;
    OHContextMenu *		fCommandMenu;    
    OHContextMenu *		fCmdHistoryMenu;
    
    TGPopupMenu *		fFileMenu;
    TGPopupMenu *		fPreferencesMenu;
    TGPopupMenu *		fViewMenu;
    TGPopupMenu *		fViewAsMenu;
    TGPopupMenu *		fSortMenu;
    TGPopupMenu *		fStyleMenu;
    TGPopupMenu *		fCanvasMenu;
    
    TGListTreeItem *		fSelectedTreeItem; // currently selected TGListTree item
    OHEntry *			fSelectedEntry;
    
    EListViewMode		fViewMode; // current IconBox view mode
    ECanvasMode			fCanvasMode; // current histogram display mode
    OHLVEntry::SortMode		fSortMode; // current IconBox sort mode
    Bool_t			fAscending; // current IconBox sort direction
    Bool_t			fRefresh; // refresh is forced
    
    TStyle * fStyleDefault;
    TStyle * fStyleColor;
    
    void Add( TObject * obj, const char * = 0, int = 0 );
    void BrowseObj( TObject *obj );
    void CreateBrowser( const char * name );
    void ListTreeHighlight( TGListTreeItem *item );
    void ClearCurrentSelection( TGListTreeItem * item );
    void ListViewAction( OHEntry *obj );
    void DisplayTotal( Int_t total, Int_t selected );
    void SetViewMode( EListViewMode new_mode );
    void SetStyleMode( Int_t new_mode );
    void SetCanvasMode(ECanvasMode new_mode);
    void SetPreferences( Int_t item );
    void ShowHideToolBar( );
    void ShowHideStatusBar( );
    void SetToolStates( );
        
    // overridden from TGMainFrame
    void CloseWindow();
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
    
 public:
 
    static const int kShowHistory;
    
    OHRootBrowser(TBrowser *b, const char *title, UInt_t width, UInt_t height);
    OHRootBrowser(TBrowser *b, const char *title, Int_t x, Int_t y, UInt_t width, UInt_t height);
    ~OHRootBrowser();
    
    void RefreshListView();
    Bool_t IsRefresh() { return fRefresh; }
    void Show() { MapRaised(); }
    void SetSortMode( OHLVEntry::SortMode new_mode );
    Bool_t GetPreferences( int item );
    ECanvasMode GetCanvasMode() { return fCanvasMode; }
    
    //    ClassDef(OHRootBrowser,0)
};

#endif

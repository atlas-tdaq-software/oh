#include "OHContextMenu.h"
#include <iostream>

#include <TVirtualX.h>

OHContextMenu::OHContextMenu( const TGWindow * parent, OHContextMenu * commands )
  : TGPopupMenu( parent ),
    commands_( commands ),
    synch_( 0 ),
    command_( "" )
{
}
    
void OHContextMenu::AddElement( const char * item )
{
    const TList * list = GetListOfEntries();
    
    for ( int i = 0; i < list->GetSize(); i++ )
    {
    	const char * curi = ((TGMenuEntry*)list->At(i))->GetName();
	if ( curi && !strcmp( item, curi ) )
	    return;
    }
    AddEntry( item, list->GetSize() );
}

const char * OHContextMenu::Show( const TGWindow * w, int orgX, int orgY )
{
    Window_t wdum;
    int x, y;
    gVirtualX->TranslateCoordinates( w->GetId(), GetParent()->GetId(), orgX, orgY, x, y, wdum );
    return Show( x, y );
}

const char * OHContextMenu::Show( int x, int y )
{
    PlaceMenu( x, y, true, true );
    synch_ = new TGTransientFrame( fClient->GetRoot(), fClient->GetRoot(), 0, 0 );
    fClient->WaitFor( synch_ );
    return command_;
}

const char * OHContextMenu::GetElement( int index )
{
    TGMenuEntry * entry;
    const char * result = 0;
    if ( index >= 0 && ( entry = GetEntry( index ) ) != 0 )
	result = entry->GetName();
    
    return ( result ? result : "" );
}

const char * OHContextMenu::GetLastElement( )
{
     const TList * list = GetListOfEntries();
    
    return GetElement( list->GetSize() - 1 );
}

void OHContextMenu::Activated(Int_t id)
{
    if ( id < 1000 )
    {
    	command_ = commands_ ? commands_->GetElement( id ) : GetElement( id );
    }
    
    if ( synch_ )
    {
    	delete synch_;
	synch_ = 0;
    }
}

void OHContextMenu::PoppedDown()
{
    command_ = "";
    if ( synch_ )
    {
    	delete synch_;
	synch_ = 0;
    }
}

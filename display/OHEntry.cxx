/*
  OHEntry.cxx

  Serguei Kolos April 2007
*/

#include "OHEntry.h"
#include "OHRootBrowser.h"
#include "OHCommandDialog.h"

#include <TGFileDialog.h>

//ClassImp(OHEntry)
//ClassImp(OHLVEntry)

Int_t
OHLVEntry::CompareEntries( const OHLVEntry * entry, SortMode mode ) const
{
    if ( entry_->IsFolder() != entry->entry_->IsFolder() )
    {
    	return entry->entry_->IsFolder();
    }
    
    if ( mode == kSortByName )
    {
        return strcmp( entry_->GetName(), entry->entry_->GetName() );
    }
    
    if ( mode == kSortByTag )
    {
    	return ( entry_->GetTag() > entry->entry_->GetTag() );
    }
    
    if ( mode == kSortByTime )
    {
    	return ( entry_->GetTime() > entry->entry_->GetTime() );
    }
    
    if ( mode == kSortByType )
    {
    	return ( entry_->GetType() > entry->entry_->GetType() );
    }
    
    return 0;
}

std::string
OHEntry::AskFileName( TGMainFrame * frame, const std::string & name ) const
{
    std::string filename = name + ".root";
    TGFileInfo fileInfo;
    const char * types[] = { "Root files", "*.root", 0, 0 };
    fileInfo.fFileTypes = types;
    fileInfo.fFilename = strdup( filename.c_str() );
    new TGFileDialog( gClient->GetRoot(), frame, kFDSave, &fileInfo );

    return fileInfo.fFilename ? fileInfo.fFilename : "";
}

std::string
OHEntry::AskCommand( TGMainFrame * frame, const std::string & title ) const
{
    std::string command;
    OHCommandDialog * cd = new OHCommandDialog( frame, ((OHRootBrowser*)frame)->fCmdHistoryMenu, title, command );
    cd->Popup();
    return command;
}

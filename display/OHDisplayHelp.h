#ifndef _OH_DISPLAY_HELP_H_
#define _OH_DISPLAY_HELP_H_

#ifndef ROOT_Rtypes
#include "Rtypes.h"
#endif

R__EXTERN const char gOHHelpUsersGuide[];
R__EXTERN const char gOHHelpAbout[];

#endif

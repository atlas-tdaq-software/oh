#include "OHDisplayGuiFactory.h"
#include "OHRootBrowser.h"

////////////////////////////////////////////////////////////////////////////////////
//
// OHDisplayGuiFactory : helper class for THistBrowser.
//
// (c) Monika Barczyk and Piotr Golonka, April 2002
//
// OHDisplayGuiFactory replaces ROOT's original TRootGuiFactory object pointed by
// gGuiFactory. New object, of OHDisplayGuiFactory class, will use instances
// of THistBrowser instead of TRootBrowser, every time a new TBrowser
// object is created.
// THistBrowser is modified version of TRootBrowser, suited for
// ATLAS OnLine Software's Online Histogramming package.
//
// No additional code or instantiation is needed: static object 
// OHDisplayGuiFactory::gGuiFactory is instantaited when the library is being loaded,
// and gGuiFactory global pointer is updated.
////////////////////////////////////////////////////////////////////////////////////

#include <iostream>

// ClassImp(OHDisplayGuiFactory)

OHDisplayGuiFactory::OHDisplayGuiFactory( )
  : TRootGuiFactory( "OHDisplayFactory", "OH Display Factory" )
{
    old_factory = gGuiFactory;
    gGuiFactory = this;
}

OHDisplayGuiFactory::~OHDisplayGuiFactory( )
{
    gGuiFactory = old_factory;
}

TBrowserImp *OHDisplayGuiFactory::CreateBrowserImp(TBrowser *b, const char *title, UInt_t width, UInt_t height, const Option_t* )
{
    return new OHRootBrowser( b, title, width, height );
}

TBrowserImp *OHDisplayGuiFactory::CreateBrowserImp(TBrowser *b, const char *title,Int_t x, Int_t y, UInt_t width, UInt_t height, const Option_t* )
{
    return new OHRootBrowser( b, title, x, y, width, height );
}


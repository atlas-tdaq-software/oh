#ifndef _OH_COMMAND_DIALOG_H
#define _OH_COMMAND_DIALOG_H

#include "TGButton.h" 
#include "TGLabel.h" 
#include "TGComboBox.h"
#include "TGTextEntry.h"
#include "TGMenu.h"
#include "OHContextMenu.h"

#include <string>

class OHRootBrowser;

class OHTextEntry : public TGTextEntry
{
public:
   OHTextEntry( TGFrame * parent, const char * val, int id, OHContextMenu * m = 0 );
    
private:
   Bool_t HandleButton( Event_t * event );

   OHContextMenu *	menu_;    
};

class OHCommandDialog : public TGTransientFrame {

private:
   std::string &	command_;
   OHContextMenu *	menu_;
   
   TList *		fWidgets;
   OHTextEntry * 	fCommand;
   TGTextButton *	fSend;
      
public:
   OHCommandDialog( TGMainFrame * main, OHContextMenu * menu, const std::string & title, std::string & command );
   
   Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
      
   virtual ~OHCommandDialog();

   virtual void Popup();
};

#endif

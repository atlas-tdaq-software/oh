/*
  OHSystem.cxx

  Monika Barczyk & Piotr Golonka, May 2002
*/
#include <TGMsgBox.h>

#include "OHSystem.h"
#include "OHPartition.h"
#include "OHConfigDialog.h"

#include <oh/OHFileSaver.h>
#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>

//ClassImp(OHSystem)

const TGPicture * 
OHSystem::GetSmallPicture() const
{
    static const TGPicture * picture = gClient->GetPicture("system__16x16");
    return picture;
}
  
const TGPicture * 
OHSystem::GetLargePicture() const
{
    static const TGPicture * picture = gClient->GetPicture("system__32x32");
    return picture;
}
  
void OHSystem::Browse( TBrowser * browser )
{			    
    RemoveChildren();
    
    // Create an PartitionIterator
    std::list<IPCPartition> list;
    
    IPCPartition::getPartitions( list );
    // add initial partition to the list
    list.push_back( IPCPartition() );
    
    std::list<IPCPartition>::iterator ii = list.begin();
    for ( ;ii != list.end(); ii++ ) 
    {
	try
	{
	    ipc::partition_var pi = ii->getImplementation();
	    ipc::servant::ApplicationContext_var info = pi -> app_context();
	    AddChildItem( ii->name(), new OHPartition( ii->name(), info -> time ), this );
	}
	catch( ers::Issue & ex )
	{
	    ERS_DEBUG( 0, ex );
        }
        catch( CORBA::Exception & ex )
        {
            ERS_DEBUG( 0, ex._name() );
        }
    }
    
    OHFolder::Explore( browser );
}

void 
OHSystem::SaveToFile( TGMainFrame * frame, const std::string & ) const
{
    std::string filename = GetName();
    filename = AskFileName( frame, filename );
    
    if ( filename.empty() )
    {
    	return ;
    }
    
    OHFileSaver receiver( filename );
  
    if ( receiver.error() )
    {
	std::ostringstream out;
	out << "Can not create '" << filename << "' file";
	std::string message = out.str();
	new TGMsgBox( gClient->GetRoot(), frame, "Error", message.c_str(), kMBIconExclamation );
	return ;
    }

    // Create an PartitionIterator
    std::list<IPCPartition> list;
    
    IPCPartition::getPartitions( list );
    // add initial partition to the list
    list.push_back( IPCPartition() );
    
    std::list<IPCPartition>::iterator ii = list.begin();
    for ( ;ii != list.end(); ii++ )
    {
	IPCPartition pp( ii->name() );
        receiver.pushd( ii->name() );
	try
	{
	    OHServerIterator sii( pp, OHConfigDialog::ServerRegex );
	    while ( sii++ )
	    {
		OHProviderIterator pii( pp, sii.name(), OHConfigDialog::ProviderRegex );
		while ( pii++ )
		{
		    OHIterator it( pp, sii.name(), pii.name(), OHConfigDialog::ObjectRegex );
		    receiver.pushd( pii.name() );

		    while ( it++ )
		    {
			it.retrieve( receiver, true );
		    }
		    receiver.popd( );
		}
	    }
	}
	catch ( ers::Issue & ex )
	{
	    ERS_DEBUG( 0, ex );
	}
        receiver.popd( );
    }
}	

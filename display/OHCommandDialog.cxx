#include <oh/OHCommandSender.h>

#include "OHRootBrowser.h"
#include "OHCommandDialog.h"

//Root include file
#include "TGLabel.h"
#include "TGNumberEntry.h"
#include "TGButton.h"
#include "TGMsgBox.h" 
#include "TVirtualX.h"

OHTextEntry::OHTextEntry( TGFrame * parent, const char * val, int id, OHContextMenu * m )
  : TGTextEntry( parent, val, id ),
    menu_( m )
{
}

Bool_t OHTextEntry::HandleButton( Event_t * event )
{
    if ( menu_ && event->fCode == kButton3 )
    {
	std::string command = menu_->Show( this, event->fX, event->fY );
	if ( !command.empty() )
	    SetText( command.c_str() );
	return kTRUE;
    }
    else
    {
	return TGTextEntry::HandleButton( event );
    }
}


OHCommandDialog::OHCommandDialog( TGMainFrame * frame,
				  OHContextMenu * menu,
				  const std::string & title,
                                  std::string & command )
  : TGTransientFrame( gClient->GetRoot(), frame, 500, 100 ),
    command_( command ),
    menu_( menu )
{
   fWidgets = new TList;

   TGHorizontalFrame * Main = new TGHorizontalFrame(this, 500, 100, kFixedWidth);
   TGLayoutHints * El = new TGLayoutHints( kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 2, 2);
   
   AddFrame( Main, El );
   
   fWidgets->Add( Main ); 
   fWidgets->Add( El );

   SetWindowName( title.c_str() );
   SetIconName( title.c_str() );
   
   fCommand = new OHTextEntry( Main, "", 23, menu_ );

   Main->AddFrame( fCommand, El );
   fCommand->Associate( this );
}


OHCommandDialog::~OHCommandDialog()
{
   // Delete the dialog.

   delete fCommand;
   delete fSend;
   
   fWidgets->Delete();
   delete fWidgets;
}

//______________________________________________________________________________
void OHCommandDialog::Popup()
{
   // Popup dialog.

   //--- create the OK, Apply and Cancel buttons

   UInt_t  nb = 0, width = 0, height = 0;

   TGHorizontalFrame *hf = new TGHorizontalFrame(this, 500, 20, kFixedWidth);
   TGLayoutHints     *l1 = new TGLayoutHints(kLHintsCenterY | kLHintsExpandX, 5, 5, 0, 0);

   // put hf as last in the list to be deleted
   fWidgets->Add(l1);

   fSend = new TGTextButton(hf, "&Send", 11);
   fSend->Associate(this);
   hf->AddFrame(fSend, l1);
   height = fSend->GetDefaultHeight();
   width  = TMath::Max(width, fSend->GetDefaultWidth()); ++nb;
   
   std::string cmd = fCommand->GetText();
   if ( cmd.empty() )
	fSend->SetState( kButtonDisabled );

   TGTextButton *b;
   b = new TGTextButton(hf, "&Cancel", 12);
   fWidgets->Add(b);
   b->Associate(this);
   hf->AddFrame(b, l1);
   height = b->GetDefaultHeight();
   width  = TMath::Max(width, b->GetDefaultWidth()); ++nb;

   // place buttons at the bottom
   l1 = new TGLayoutHints(kLHintsBottom | kLHintsCenterX, 0, 0, 5, 5);
   fWidgets->Add(l1);
   fWidgets->Add(hf);

   AddFrame(hf, l1);

   // keep the buttons centered and with the same width
   hf->Resize((width + 20) * nb, height);

   // map all widgets and calculate size of dialog
   MapSubwindows();

   width  = GetDefaultWidth();
   height = GetDefaultHeight();

   Resize(width, height);

   // position relative to the parent's window
   Window_t wdum;
   int ax, ay;
   gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
                       (Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
                       (Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
                       ax, ay, wdum);
   Move(ax, ay);
   SetWMPosition(ax, ay);

   // make the message box non-resizable
   SetWMSize(width, height);
   SetWMSizeHints(width, height, width*5, height, 1, 1);

   SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                              kMWMDecorMinimize | kMWMDecorMenu,
               kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                              kMWMFuncMinimize,
               kMWMInputModeless);

   MapWindow();
   fClient->WaitFor(this);
}

Bool_t OHCommandDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t )
{
  if ( GET_MSG(msg) == kC_COMMAND )
  {	  
	switch (parm1) 
	{
	    case 11: // Send
	    {
		command_ = fCommand->GetText();
		menu_->AddElement( command_.c_str() );
	    }
	    case 12: // Cancel
		delete this;
	    default:
		break;
	}
  }
  else if ( GET_MSG(msg) == kC_TEXTENTRY && GET_SUBMSG(msg) == kTE_TEXTCHANGED )
  {
  	std::string cmd = fCommand->GetText();
	if ( cmd.empty() )
	    fSend->SetState( kButtonDisabled );
	else
	    fSend->SetState( kButtonUp );
  }
  return kTRUE;	    
}

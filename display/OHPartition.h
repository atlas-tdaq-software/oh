/*
   OHPartition.h

   Monika Barczyk & Piotr Golonka, May 2002
   Serguei Kolos April 2007
*/

#ifndef _OH_PARTITION_H_
#define _OH_PARTITION_H_

#include <mutex>

#include "OHFolder.h"

#include <TBrowser.h>

class OHPartition: public OHFolder
{
    const TGPicture * GetSmallPicture() const;
    const TGPicture * GetLargePicture() const;
    
    void updateProvidersForServer( const std::string & server, const std::string & mask, std::mutex & mutex );

 public:
    OHPartition( const std::string & name, time_t time )
      : OHFolder( name, time )
    { ; }

    ~OHPartition() { ; }

    int GetTag() const { return 0; }
    const char * GetPartition( ) const { return GetName(); }
    Bool_t CommandsAllowed() const { return kTRUE; }
    void SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const;
    void SendCommand( TGMainFrame * frame, const std::string & command ) const;   

    Bool_t IsFolder() const { return kTRUE; }

    void Browse( TBrowser * browser );
    void Refresh( TBrowser * browser, const std::string & mask );

    //    ClassDef(OHPartition,1)
};

#endif  

/*
    graph_provider -> cxx

    A simple utility which can publish ROOT graphs from a file or
    a randomly generated graph to the OH.
    This is derived from root_provider -> cxx.

    Herbert Kaiser, Aug 2006

*/

#include <iostream>

// ROOT include files
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TGraphBentErrors.h"
#include "TGraph2D.h"
#include "TGraph2DErrors.h"
#include "TKey.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TApplication.h"
#include "TClass.h"

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <owl/timer.h>
#include <stdlib.h>

// OH include files
#include "oh/OHRootProvider.h"

using namespace std;

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    //
    // Get command line arguments
    CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
    CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr provider_name( 'n', "provider", "provider_name",
                             "Provider name", CmdArg::isREQ );
    CmdArgStr histogram_name( 'h', "histogram", "histogram_name",
                              "Graph type name", CmdArg::isREQ );
    CmdArgStr option( 'o', "option", "option",
                      "1d, 2d, 1de, 1dea, 1deb, 2de or the name of a file with ROOT graphs",
                       CmdArg::isREQ );
    CmdArgInt count( 'c', "count","count", "number of points in the random generated graph (default is 30)");
    CmdArgInt   tag('t', "tag", "object-tag", "publish histogram with this tag");
    CmdArgBool	graphics('g', "graphics", "display graphs in graphics mode.");
    CmdArgBool	verbose('v', "verbose", "print graph content.");
    CmdArgBool	labels('l', "labels", "add labels to graphs.");
    CmdArgBool	stoptime('t', "time", "do a time test with the random generated graph");
    CmdArgStr	drawoption('d', "drawoption", "drawoption", "give a string for the ROOT DrawOption for the graph");
    
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name,
                 &histogram_name, &option,&count, &tag, &graphics, &verbose, &labels, &stoptime,&drawoption, NULL );
		 
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( "OH ROOT graph publisher utility" );

    count=30;
    tag = -1;
    cmd.parse( arg_iter );

    TApplication theApp( "App", 0, 0 );
    
    //
    // Create a ROOT provider
    IPCPartition p( partition_name );
    OHRootProvider * provider;
    try
    {
    	provider = new OHRootProvider( p, (const char*)server_name, (const char*)provider_name );
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
	return 1;
    }

    TGraph * graph = 0;
    TGraph2D * graph2D = 0;

    // create DrawOption annotation if wanted
    std::vector< std::pair<std::string,std::string> > ann;
    if (drawoption.isNULL())
       	ann=oh::util::EmptyAnnotation;
    else {
	ann.resize(1);
	ann[0].first="DrawOption";
	ann[0].second=drawoption;
    }

    //
    // Create and publish the graph(s)
    string opt( (const char*)option );
    if ( opt == "1d" ) {
	double *x,*y;
	x= new double[int(count)];
	y= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	}
        graph = new TGraph(int(count),x,y);
	graph->SetTitle((const char*)histogram_name);
        graph-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph-> GetYaxis( ) -> SetTitle( "Y Axis" );
	delete[] x;
	delete[] y;
    } else if ( opt == "1de" ) {
	double *x,*y,*ex,*ey;
	x= new double[int(count)];
	y= new double[int(count)];
	ex= new double[int(count)];
	ey= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	    ex[i]=10*double(random())/RAND_MAX;
	    ey[i]=10*double(random())/RAND_MAX;
	}
        graph = new TGraphErrors(int(count),x,y,ex,ey);
	graph->SetTitle((const char*)histogram_name);
        graph-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph-> GetYaxis( ) -> SetTitle( "Y Axis" );
	delete[] x;
	delete[] y;
	delete[] ex;
	delete[] ey;
    } else if ( opt == "1dea" ) {
	double *x,*y,*exl,*exh,*eyl,*eyh;
	x= new double[int(count)];
	y= new double[int(count)];
	exl= new double[int(count)];
	exh= new double[int(count)];
	eyl= new double[int(count)];
	eyh= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	    exl[i]=10*double(random())/RAND_MAX;
	    exh[i]=10*double(random())/RAND_MAX;
	    eyl[i]=10*double(random())/RAND_MAX;
	    eyh[i]=10*double(random())/RAND_MAX;
	}
        graph = new TGraphAsymmErrors(int(count),x,y,exl,exh,eyl,eyh);
	graph->SetTitle((const char*)histogram_name);
        graph-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph-> GetYaxis( ) -> SetTitle( "Y Axis" );
	delete[] x;
	delete[] y;
	delete[] exl;
	delete[] exh;
	delete[] eyl;
	delete[] eyh;
    } else if ( opt == "1deb" ) {
	double *x,*y,*exl,*exh,*eyl,*eyh,*exld,*exhd,*eyld,*eyhd;
	x= new double[int(count)];
	y= new double[int(count)];
	exl= new double[int(count)];
	exh= new double[int(count)];
	eyl= new double[int(count)];
	eyh= new double[int(count)];
	exld= new double[int(count)];
	exhd= new double[int(count)];
	eyld= new double[int(count)];
	eyhd= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	    exl[i]=10*double(random())/RAND_MAX;
	    exh[i]=10*double(random())/RAND_MAX;
	    eyl[i]=10*double(random())/RAND_MAX;
	    eyh[i]=10*double(random())/RAND_MAX;
	    exld[i]=10*double(random())/RAND_MAX;
	    exhd[i]=10*double(random())/RAND_MAX;
	    eyld[i]=10*double(random())/RAND_MAX;
	    eyhd[i]=10*double(random())/RAND_MAX;
	}
        graph = new TGraphBentErrors(int(count),x,y,exl,exh,eyl,eyh,exld,exhd,eyld,eyhd);
	graph->SetTitle((const char*)histogram_name);
        graph-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph-> GetYaxis( ) -> SetTitle( "Y Axis" );
	delete[] x;
	delete[] y;
	delete[] exl;
	delete[] exh;
	delete[] eyl;
	delete[] eyh;
	delete[] exld;
	delete[] exhd;
	delete[] eyld;
	delete[] eyhd;
    } else if ( opt == "2d" ) {
	double *x,*y,*z;
	x= new double[int(count)];
	y= new double[int(count)];
	z= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	    z[i]=100*double(random())/RAND_MAX;
	}
        graph2D = new TGraph2D(int(count),x,y,z);
	graph2D->SetTitle((const char*)histogram_name);
        graph2D-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph2D-> GetYaxis( ) -> SetTitle( "Y Axis" );
        graph2D-> GetZaxis( ) -> SetTitle( "Z Axis" );
	delete[] x;
	delete[] y;
	delete[] z;
    } else if ( opt == "2de" ) {
	double *x,*y,*z,*ex,*ey,*ez;
	x= new double[int(count)];
	y= new double[int(count)];
	z= new double[int(count)];
	ex= new double[int(count)];
	ey= new double[int(count)];
	ez= new double[int(count)];
	for (int i=0;i<int(count);i++) {
	    x[i]=100*double(random())/RAND_MAX;
	    y[i]=100*double(random())/RAND_MAX;
	    z[i]=100*double(random())/RAND_MAX;
	    ex[i]=100*double(random())/RAND_MAX;
	    ey[i]=100*double(random())/RAND_MAX;
	    ez[i]=100*double(random())/RAND_MAX;
	}
        graph2D = new TGraph2DErrors(int(count),x,y,z,ex,ey,ez);
	graph2D->SetTitle((const char*)histogram_name);
        graph2D-> GetXaxis( ) -> SetTitle( "X Axis" );
        graph2D-> GetYaxis( ) -> SetTitle( "Y Axis" );
        graph2D-> GetZaxis( ) -> SetTitle( "Z Axis" );
	delete[] x;
	delete[] y;
	delete[] z;
	delete[] ex;
	delete[] ey;
	delete[] ez;
    } else {
        // option should be a filename, try to open the file
        TFile file( opt.c_str( ) );
        if ( ! file.IsOpen( ) ) {
            cout << "Unable to open the file " << opt << "." << endl;
            return 0;
        }
        // The file is open, publish all graphs in the file
        long fcount = 0;
        TObject *obj;
        TKey *key;
        TIter it( file.GetListOfKeys() );
        cout << "Searching for histograms in the file..." << endl;
        while ( ( key = (TKey*)it( ) ) ) {
            obj = file.Get( key->GetName( ) );
            cout << "Found an object with name " << key->GetName()
                 << " and type " << obj->IsA( )->GetName( ) << "... ";
            fcount++;
            if ( obj->IsA( )== TGraph::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraph*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else if ( obj->IsA( )==TGraph2D::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraph2D*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else if ( obj->IsA( )==TGraphErrors::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraphErrors*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else if ( obj->IsA( )==TGraphAsymmErrors::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraphAsymmErrors*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else if ( obj->IsA( )==TGraphBentErrors::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraphBentErrors*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else if ( obj->IsA( )==TGraph2DErrors::Class() ) {
                cout << "Publishing..." << endl;
                provider -> publish( *(static_cast<TGraph2DErrors*>(obj)),(std::string(histogram_name)+"_"+key->GetName()).c_str(),tag,ann );
            } else {
                cout << "skipping" << endl;
                fcount--;
            }
        }
        cout << "Found " << fcount << " graphs in the file..." << endl;
        return 0;
    }
    

    if ( labels && graph )
    {
	for( int i = 1; i < graph->GetXaxis()->GetNbins(); i+=(graph->GetXaxis()->GetNbins())/10 )
	{
	    char label[20];
	    sprintf( label, "b %d", i );
	    graph -> GetXaxis( ) -> SetBinLabel( i, label );
	}
    }

    if ( labels && graph2D )
    {
	for( int i = 1; i < graph2D->GetXaxis()->GetNbins(); i+=(graph2D->GetXaxis()->GetNbins())/10 )
	{
	    char label[20];
	    sprintf( label, "b %d", i );
	    graph2D -> GetXaxis( ) -> SetBinLabel( i, label );
	    graph2D -> GetYaxis( ) -> SetBinLabel( i, label );
	}
    }

    if ( verbose )
    {
	if ( graphics )
	{
	    if (graph){
		TCanvas* canvas = new TCanvas( graph->GetName(), graph->GetTitle() );
		if (drawoption.isNULL())
		    graph -> Draw("ALP");
		else
		    graph-> Draw((const char*)drawoption);
		canvas -> Update( );
	    }else{
		TCanvas* canvas = new TCanvas( graph2D->GetName(), graph2D->GetTitle() );
		if (drawoption.isNULL())
		    graph2D -> Draw("TRIW");
		else
		    graph2D-> Draw((const char*)drawoption);
		canvas -> Update( );
	    }
	}
	else
	{
	    if (graph)
		graph -> Print( "all" );
	    else
		graph2D -> Print( "all" );
	}
    }
    if (graph)
	provider -> publish( *graph, (const char*)histogram_name, tag, ann  );
    else
	provider -> publish( *graph2D, (const char*)histogram_name, tag, ann  );
    std::cout << "publish successful " << std::endl;
    
    if ( stoptime && graph )
    {        
	OWLTimer timer;
	timer.start();
	for( int i = 0; i < 1000; i++ )
	{
	    provider -> publish( *graph, (const char*)histogram_name, tag,ann  );
	}
	timer.stop();
	std::cout << "time = " << timer.totalTime()/1000. << std::endl;
    }
    
    if ( stoptime && graph2D )
    {        
	OWLTimer timer;
	timer.start();
	for( int i = 0; i < 1000; i++ )
	{
	    provider -> publish( *graph2D, (const char*)histogram_name, tag,ann  );
	}
	timer.stop();
	std::cout << "time = " << timer.totalTime()/1000. << std::endl;
    }

    if ( graphics && verbose )
    {
	cout << "Entering ROOT message loop..." << endl
             << "Click 'Quit ROOT' on the file menu to quit"
             << endl;
        theApp.Run( );
    }
    delete graph;
    delete provider;

    return 0;
}

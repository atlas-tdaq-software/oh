/*
    root_display.cxx
    
    A simple utility which display histograms published in the OH using
    the ROOT framework.
    
    Fredrik Sandin, Nov 2001
    Herbert Kaiser, Aug 2006  (added DrawOption)

*/

#include <iostream>
using namespace std;

// ROOT include files
#include "TStyle.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TSlider.h"
#include "TGraph.h"
#include "TControlBar.h"
#include "TButton.h"
#include "TRandom.h"
#include "TApplication.h"

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>

/*
    This is the histogram/graph receiver implementation. It simply
    displays the received histograms on the screen. All histograms
    retrieved with a OHHistogramIterator or OHHistogramSubscriber
    finally must end up in some receiver implementation.
*/
class MyReceiver : public OHRootReceiver {
public:
    const char* getDrawOption(std::vector< std::pair<std::string,std::string> >& ann) {
	for (unsigned i=0;i<ann.size();i++) {
	    if (ann[i].first=="DrawOption")
		return ann[i].second.c_str();
	}
	return NULL;
    }


    MyReceiver( bool is_draw ) : draw_( is_draw ){ ; }
    
    void receive( OHRootHistogram & h )
    {
	if ( draw_ )
	{
	    TCanvas* canvas = new TCanvas( h.histogram -> GetName(), h.histogram -> GetTitle() );
	    const char* dop=getDrawOption(h.annotations);
	    if (dop)
		h.histogram -> Draw(dop);
	    else
		h.histogram -> Draw();
	    canvas -> Update( );
	    h.histogram.release(); // we ask the histogram unique_ptr to drop ownership
	    			   // for the histogram since it will be kept by ROOT 
	}
	else
	{
	    h.histogram -> Print( "all" );
	    // histogram will be deleted immediatelt after returning from this function
	}
    }
    
    void receive( vector<OHRootHistogram*> & )
    {
    }

    void receive( OHRootGraph & h )
    {
	if ( draw_ )
	{
	    TCanvas* canvas = new TCanvas( h.graph -> GetName(), h.graph -> GetTitle() );

	    const char* dop=getDrawOption(h.annotations);
	    if (dop)
		h.graph -> Draw(dop);
	    else
		h.graph -> Draw("ALP");
	    canvas -> Update( );
	    h.graph.release(); // we ask the graph unique_ptr to drop ownership
	    			   // for the graph since it will be kept by ROOT 
	}
	else
	{
	    h.graph -> Print( "all" );
	    // graph will be deleted immediatelt after returning from this function
	}
    }
    
    void receive( OHRootGraph2D & h )
    {
	if ( draw_ )
	{
	    TCanvas* canvas = new TCanvas( h.graph -> GetName(), h.graph -> GetTitle() );

	    const char* dop=getDrawOption(h.annotations);
	    if (dop)
		h.graph -> Draw(dop);
	    else
		h.graph -> Draw("TRIW");
	    canvas -> Update( );
	    h.graph.release(); // we ask the graph unique_ptr to drop ownership
	    			   // for the graph since it will be kept by ROOT 
	}
	else
	{
	    h.graph -> Print( "all" );
	    // graph will be deleted immediatelt after returning from this function
	}
    }
private:
        bool draw_;
};

int main( int argc, char ** argv )
{

    IPCCore::init( argc, argv );
    
    TApplication theApp("App", 0, 0);
    
    //
    // Get parameters from the command line
    //
    CmdArgStr	partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
    CmdArgStr	server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr	provider_name( 'n', "provider", "provider_name",
                             "Provider name" );
    CmdArgStr	histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram/Graph type name" );
    CmdArgBool	graphics('g', "graphics", "display histograms/graphs in graphics mode.");
    CmdArgInt	iters('i', "iters", "iterations", "number of test iterations (1 by default)");
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &graphics, &iters, NULL );
    
    iters = 1;
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH Histogram display utility" );
    
    cmd.parse(arg_iter);
    
    //
    // Check command line parameters
    if( provider_name.isNULL( ) ) provider_name = ".*";
    if( histogram_name.isNULL( ) ) histogram_name = ".*";
    
    
    if ( histogram_name.flags() && CmdArg::GIVEN )
    {
	//
	// One way of reading histograms (MyReceiver.receive function will be called)
	//
	
	std::cout << "Call the OHHistogramReceiver::getHistogram ... " << std::endl;
	MyReceiver receiver( graphics );
	IPCPartition p( partition_name );
	receiver.getHistogram( p, (const char*)server_name,
				  (const char*)provider_name,
				  (const char*)histogram_name );
				  
	//
	// Another way of reading histograms
	//
	try
	{
	    std::cout << "Call the OHRootReceiver::getRootObject ... " << std::endl;
	    OHRootObject od = OHRootReceiver::getRootObject( p, (const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    // display the object directly
	    if ( graphics )
	    {
		TCanvas* canvas = new TCanvas( static_cast<TNamed*>(od.object.get())->GetName(), static_cast<TNamed*>(od.object.get())->GetTitle() );
		const char* dop=receiver.getDrawOption(od.annotations);
		if (dop)
		    od.object -> Draw(dop);
		else
		    od.object -> Draw();
		canvas -> Update( );
		od.object.release(); // we ask the object unique_ptr to drop ownership
		// for the object since it will be kept by ROOT 
	    }
	    else
	    {
		od.object -> Print( "all" );
		// object will be deleted immediately after returning from this function
	    }


	    // the next are just examples
	    // In real case only one of these getFunctions doesn't throw an error, because the object
	    // can only be of one type...

	    std::cout << "Call the OHRootReceiver::getRootHistogram ... " << std::endl;
	    OHRootHistogram hd = OHRootReceiver::getRootHistogram( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd );
	    
	    std::cout << "Call the OHRootReceiver::getRootHistogram again ... " << std::endl;
	    // read the same histogram one more time
	    hd = OHRootReceiver::getRootHistogram( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd );

	    // do the same for TGraph - graphs
	    std::cout << "Call the OHRootReceiver::getRootGraph ... " << std::endl;
	    OHRootGraph hd1 = OHRootReceiver::getRootGraph( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd1 );
	    
	    std::cout << "Call the OHRootReceiver::getRootGraph again ... " << std::endl;
	    // read the same histogram one more time
	    hd1 = OHRootReceiver::getRootGraph( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd1 );

	    // do the same for TGraph2D - graphs
	    std::cout << "Call the OHRootReceiver::getRootGraph2D ... " << std::endl;
	    OHRootGraph2D hd2 = OHRootReceiver::getRootGraph2D( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd2 );
	    
	    std::cout << "Call the OHRootReceiver::getRootGraph2D again ... " << std::endl;
	    // read the same histogram one more time
	    hd2 = OHRootReceiver::getRootGraph2D( p, 	(const char*)server_name,
				  			(const char*)provider_name,
				  			(const char*)histogram_name );
	    //
	    // Dislay histogram (MyReceiver.receive function is called 
	    // explicitely to output histogram)
	    // 
	    receiver.receive( hd2 );
	}
	catch( daq::oh::Exception & ex )
	{
	    ers::fatal( ex );
	    return 1;
	}
    }
    else
    {
	//
	// Create an OHHistogramIterator object and retrieve all histograms
	// with the specified characteristics from the specified server
	//
	try
        {
	    long count = 0;
	    for( int i = 0; i < iters; i++ )
	    {
		MyReceiver receiver( graphics );
		IPCPartition p( partition_name );
		OHIterator it( p, (const char*)server_name, (const char*)provider_name, (const char*)histogram_name );

		while ( it++ )
		{
		    cout << "Retrieving histogram " << it.name( ) << " created by " <<
		    it.provider( ) << " at " << it.time( ) << "... " << endl;
		    it.retrieve( receiver );
		    count++;
		}

		if ( count > 0 )
		    cout << "No more histograms available..." << endl;
		else
		    cout << "No histograms found..." << endl;
	    }
	}
        catch( daq::oh::Exception & ex )
        {
	    ers::fatal( ex );
	    return 1;
        }
    }
    
    if ( graphics )
    {
	cout << "Entering ROOT message loop..." << endl
             << "Click 'Quit ROOT' on the file menu to quit"
             << endl;
        theApp.Run( );
    }
    return 0;
}

// EOF

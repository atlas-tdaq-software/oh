/*
    gatherer.cxx
    
    A simple utility which sums histograms.
    
    Serguei Kolos, Mar 2008
*/

#include <iostream>

#include <boost/thread.hpp>

#include <cmdl/cmdargs.h>

#include <ipc/core.h>
#include <ipc/signal.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infodynany.h>

#include <oh/OHRootProvider.h>
#include <oh/core/HistogramData.h>
#include <oh/core/ProfileData.h>

#include <ext/hash_map>

ERS_DECLARE_ISSUE( gatherer, Exception, , )

ERS_DECLARE_ISSUE_BASE(	    gatherer,
			    BadObjectType,
			    gatherer::Exception,
			    "Type of '" << name << "' object does not match the sum type.",
			    ,
			    ((std::string)name )
		    )

namespace __gnu_cxx
{
    template <> struct hash< std::string >
    {
	inline size_t operator() ( const std::string & x ) const
	{
	    return __stl_hash_string(x.c_str());
	}
    };
}

namespace gatherer
{
    template <class T>
    struct map : public __gnu_cxx::hash_map< std::string, T > {
	map( )
	{ ; }

	map( size_t n )
	  : __gnu_cxx::hash_map< std::string, T >( n )
	{ ; }
    };

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class is used to stop the gatherer properly.
// Constructor takes two parameters: number is the number of 'post' calls
// which must be done to unlock the semaphore. It should be set to the number
// of providers for a given summary object.
// Timeout is a number of seconds after which the semaphore will be forcebly unlocked.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class Semaphore
    {
      public:
	Semaphore( int number, int timeout )
	  : count_( 0 ),
	    number_( number ),
	    timeout_( timeout )
	{ ; }

	int wait()
	{
	    boost::mutex::scoped_lock lock( mutex_ );
	    if( count_ < number_ )
	    {
		boost::xtime xt;
		xtime_get( &xt, boost::TIME_UTC );
		xt.sec += timeout_;

		condition_.timed_wait( lock, xt );
	    }
	    return ( number_ - count_ );
	}

	void post()
	{
	    boost::mutex::scoped_lock lock( mutex_ );
	    count_++;
	    if ( count_ == number_ )
	    {
		condition_.notify_one();
	    }
	}

      private:
	int count_;
	int number_;
	int timeout_;
	boost::mutex mutex_;
	boost::condition condition_;
    };


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is base class which defines the interface and implements some basic operations
// which are common for any sum object.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class SumObjectBase : public ISInfoDictionary
    {
      public:

	SumObjectBase( const IPCPartition & partition, const std::string object_name )
	  : ISInfoDictionary( partition ),
	    name_( object_name ),
	    stopped_( false ),
	    semaphore_( 0 ),
	    summed_providers_( 0 ),
	    sum_empty_( true )
	{ ; }

	virtual ~SumObjectBase () { ; }

	void add( const std::string & provider, ISCallbackInfo & cb ) 
	{
	    if ( !type_matched( cb.type() ) )
	    {
		throw gatherer::BadObjectType( ERS_HERE, name_ );
	    }

	    boost::mutex::scoped_lock lock( mutex_ );
	    gatherer::map<bool>::iterator it = providers_.find( provider );
	    if ( it != providers_.end() )
	    {
		if ( it->second )
		{
		    publish( );
		}
		it->second = true;
		++summed_providers_;
	    }
	    else
	    {
		providers_.insert( std::make_pair( provider, true ) );
	    }

	    if ( sum_empty_ )
	    {
		set_sum( cb );
		sum_empty_ = false;
	    }
	    else
	    {
		add_to_sum( cb );
	    }

	    if ( summed_providers_ == providers_.size() )
	    {
		publish( );
	    }
	}

	void stop( Semaphore & semaphore )
	{
	    ERS_DEBUG( 1, "Stop command was received" );
	    boost::mutex::scoped_lock lock( mutex_ );
	    if ( sum_empty_ )
	    {
		ERS_DEBUG( 1, "Final sum has been already published, notifying semaphore" );
		semaphore.post();
	    }
	    else
	    {
		ERS_DEBUG( 1, "Final sum is incomplete, waiting for " << ( providers_.size() - summed_providers_ ) << " more histogram to come" );
		stopped_ = true;
		semaphore_ = &semaphore;
	    }
	}

	const std::string & name () const
	{ return name_; }

      protected:

	virtual void set_sum( ISCallbackInfo & cb ) = 0;

	virtual void add_to_sum( ISCallbackInfo & cb ) = 0;

	virtual const ISInfo & get_sum( ) = 0;

	virtual bool type_matched( const ISType & type ) = 0;

      private:

	void publish( )
	{
	    try
            {
            	checkin( name_, get_sum() );
            }
            catch( daq::is::Exception & ex )
            {
            	ers::error( ex );
            }

	    for( gatherer::map<bool>::iterator ii = providers_.begin(); ii != providers_.end(); ++ii )
	    {
		ii->second = false;
	    }
	    summed_providers_ = 0;
	    sum_empty_ = true;

	    if ( stopped_ )
	    {
		semaphore_ -> post();
	    }
	}

	std::string name_;
	boost::mutex mutex_;
	gatherer::map<bool> providers_;
	bool stopped_;
	Semaphore * semaphore_;
	size_t summed_providers_;
	bool sum_empty_;
    };

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This template class implements the summing Algorithm. Template specialisations of this class
// can be used to define different algorithms for different IS types.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    template <class T>
    struct Algorithm;

    template <class T>
    struct Algorithm<oh::HistogramData<T> >
    {
	static void add( oh::HistogramData<T> & sum, oh::HistogramData<T> & obj )
	{
	    ERS_DEBUG( 3, "Histogram add Algorithm is called" );
	    for ( size_t i = 0; i < sum.get_bins_size(); ++i )
	    {
		sum.get_bins_array()[i] += obj.get_bins_array()[i];
	    }
	}
    };
    
    template < >
    struct Algorithm<oh::ProfileData>
    {
	static void add( oh::ProfileData & sum, oh::ProfileData & obj )
	{
	    ERS_DEBUG( 3, "Profile add Algorithm is called" );
	}
    };
    
    template < >
    struct Algorithm<ISInfoDynAny>
    {
	static void add( ISInfoDynAny & sum, ISInfoDynAny & obj )
	{
	    ERS_DEBUG( 3, "Generic IS info add Algorithm is called" );
	}
    };
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The implementation of the Sum object. It inherits from the SumObjectBase and implements its
// virtual functions. An appropriate IS info class has to be used as template parameter. 
// Template specialisation of the Algorithm class must exist for this IS type.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    template <class T>
    class SumObject : public SumObjectBase
    {
      public:

	SumObject( const IPCPartition & partition, const std::string server_name, const std::string & provider_name, const std::string & histogram_name )
	  : SumObjectBase( partition, oh::util::create_info_name( server_name, provider_name, histogram_name ) )
	{ ; }

	void add_to_sum( ISCallbackInfo & cb )
	{
	    cb.value( object_ );
            gatherer::Algorithm<T>::add( sum_, object_ );
	}

	void set_sum( ISCallbackInfo & cb )
	{
	    cb.value( sum_ );
	}

	const ISInfo & get_sum( )
	{
	    return sum_;
	}

	bool type_matched( const ISType & type )
	{
	     // second condition is used for the not yet initialized ISInfoDynAny
            return ( sum_.type() == type || sum_.type().compatibleWith( ISType::Null ) );
	}

      private:

	T sum_;
	T object_;
    };

    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The is the base class for a particular gatherer configuration. It subscribes to IS and forward
// callbacks to the relevant Sum objects.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class SumMakerBase : public ISInfoReceiver
    {
      public:

	SumMakerBase(	const IPCPartition & partition,
			const std::string & server_name,
			const std::string & provider_name,
        		const std::vector<std::string> & servers,
			const ISCriteria & criteria )
	  : ISInfoReceiver( partition ),
            server_name_( server_name ),
            provider_name_(provider_name )
	{
	    for ( size_t i = 0; i < servers.size(); ++i )
	    {
		try {
		    subscribe( servers[i], criteria, &SumMakerBase::receive, this );
		}
		catch ( daq::oh::Exception & ex ) {
		    ers::error( ex );
		}
	    }
        }
	
        virtual ~SumMakerBase() { ; } 
        
	void stopGathering( int timeout )
	{
	    ERS_LOG( "Stop command with the " << timeout << " seconds timeout has been received" );

	    std::unique_ptr<Semaphore> semaphore;
	    {
		boost::mutex::scoped_lock lock( mutex_ );

		semaphore.reset( new Semaphore( sums_.size(), timeout ) );
		for ( gatherer::map<SumObjectBase*>::iterator it = sums_.begin( ); it != sums_.end(); ++it )
		{
		    it->second->stop( *semaphore.get() );
		}
	    }

	    int errors = semaphore->wait( );

	    if ( errors )
	    {
		ERS_LOG( "Stopping by the " << timeout << " seconds timeout, " << errors << " sums are incomplete" );
	    }
	    else
	    {
		ERS_LOG( "Stopped cleanly" );
	    }
	}

      protected:
      
        virtual SumObjectBase * create_sum_object(	const std::string & server_name, 
							const std::string & provider_name,
							const std::string & object_name,
							ISCallbackInfo * cb )  = 0;
        
        virtual std::string get_object_name( const std::string & name ) = 0;
        
        virtual std::string get_provider_name( const std::string & name ) = 0;
     
      private:   
	void receive( ISCallbackInfo * cb )
	{
	    ERS_DEBUG( 0, "callback for the '" << cb->name() << "' object is received" );
            std::string object_name = get_object_name( cb->name() );
	    std::string provider_name = get_provider_name( cb->name() );

	    boost::mutex::scoped_lock lock( mutex_ );
	    gatherer::map<SumObjectBase*>::iterator it = sums_.find( object_name );
	    if ( it == sums_.end() )
	    {
		SumObjectBase * object = create_sum_object( server_name_, provider_name_, object_name, cb );
		it = (sums_.insert( std::make_pair( object_name, object ) ) ).first;
	    }

	    try
	    {
		it->second->add( provider_name, *cb );
	    }
	    catch( gatherer::BadObjectType & ex )
	    {
		ers::warning( ex );
	    }
	}

      private:
        std::string server_name_;
        std::string provider_name_;
	boost::mutex mutex_;
	gatherer::map<SumObjectBase*> sums_;
    };

    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The is the implementation of the SumMakerBase which can be used to sum histograms and profiles.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class HistogramSumMaker : public OHRootProvider, // is only used to register provider in the OH
			      public SumMakerBase
    {
      public:

	HistogramSumMaker(  const IPCPartition & partition,
			    const std::string & server_name,
			    const std::string & provider_name,
			    const std::vector<std::string> & servers,
			    const std::string & providers,
			    const std::string & histograms )
	  : OHRootProvider( partition, server_name, provider_name ),	// used only to register provider in the OH
	    SumMakerBase( partition, server_name, provider_name, servers, ~oh::Histogram::type() && oh::util::create_regex( providers, histograms ) )
	{ ; }

      private:
        SumObjectBase *  create_sum_object(	const std::string & server_name, 
						const std::string & provider_name,
						const std::string & object_name,
						ISCallbackInfo * cb ) 
        {
	    if ( cb->type() == oh::HistogramData<char>::type() )
		return new SumObject<oh::HistogramData<char> >( partition(), server_name, provider_name, object_name );
	    else if ( cb->type() == oh::HistogramData<short>::type() )
		return new SumObject<oh::HistogramData<short> >( partition(), server_name, provider_name, object_name );
	    else if ( cb->type() == oh::HistogramData<int>::type() )
		return new SumObject<oh::HistogramData<int> >( partition(), server_name, provider_name, object_name );
	    else if ( cb->type() == oh::HistogramData<float>::type() )
		return new SumObject<oh::HistogramData<float> >( partition(), server_name, provider_name, object_name );
	    else if ( cb->type() == oh::HistogramData<double>::type() )
		return new SumObject<oh::HistogramData<double> >( partition(), server_name, provider_name, object_name );
	    else if ( cb->type() == oh::ProfileData::type() )
		return new SumObject<oh::ProfileData >( partition(), server_name, provider_name, object_name );

	    throw gatherer::BadObjectType( ERS_HERE, object_name );
        } 
        
	std::string get_object_name( const std::string & name )
        {
            return oh::util::get_object_name( name );
        }
        
        std::string get_provider_name( const std::string & name )
        {
            return oh::util::get_provider_name( name );
        }
    };
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The is the implementation of the SumMakerBase which can be used to sum generic IS information
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class InformationSumMaker : public SumMakerBase
    {
      public:

	InformationSumMaker(	const IPCPartition & partition,
				const std::string & server_name,
				const std::string & provider_name,
				const std::vector<std::string> & servers,
				const std::string & providers,
				const std::string & histograms )
	  : SumMakerBase( partition, server_name, provider_name, servers, providers + "\\." + histograms )
	{ ; }

      private:
        SumObjectBase * create_sum_object(  const std::string & server_name, 
					    const std::string & provider_name,
					    const std::string & object_name,
					    ISCallbackInfo * ) 
        {
	    return new SumObject<ISInfoDynAny >( partition(), server_name, provider_name, object_name );
        } 
        
	std::string get_object_name( const std::string & name )
        {
            return oh::util::get_object_name( name );
        }
        
        std::string get_provider_name( const std::string & name )
        {
            return oh::util::get_provider_name( name );
        }
    };
}

int main( int argc, char ** argv )
{

    IPCCore::init( argc, argv );
        
    //
    // Get parameters from the command line
    //
    CmdArgStr	partition_name( 'p', "partition", "partition_name", "Partition name", CmdArg::isREQ );
    CmdArgStr	server_name( 's', "server", "server-name", "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr	provider_name( 'n', "provider", "provider-name", "Provider name", CmdArg::isREQ );
    
    CmdArgStrList	server_names( 'S', "servers", "[server-name ...]", "OH Server names", CmdArg::isREQ | CmdArg::isLIST );
    CmdArgStr		provider_names( 'P', "providers", "providers", "Regilar expression for the providers to be summed (Default is '.*')" );
    CmdArgStr		histogram_names( 'H', "histograms", "histograms", "Regilar expression for the histograms to be summed (Default is '.*')" );
    
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &server_names, &provider_names, &histogram_names, NULL );
        
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH Histogram display utility" );
    
    cmd.parse(arg_iter);
    
    //
    // Check command line parameters
    if( provider_names.isNULL( ) ) provider_names = ".*";
    if( histogram_names.isNULL( ) ) histogram_names = ".*";
    
    std::vector<std::string> servers;
    for ( size_t i=0; i < server_names.count(); i++ )
    {
    	servers.push_back( (const char*)server_names[i] );
    }
    
    gatherer::HistogramSumMaker hsm(	IPCPartition( (const char*)partition_name ),
    					(const char*)server_name,
    					(const char*)provider_name,
                        		servers,
                        		(const char*)provider_names,
                        		(const char*)histogram_names );
                        
    gatherer::InformationSumMaker ism(	IPCPartition( (const char*)partition_name ),
    					(const char*)server_name,
    					(const char*)provider_name,
                        		servers,
                        		(const char*)provider_names,
                        		(const char*)histogram_names );
                        
    ERS_LOG( "gatherer started in the partition \"" << (const char*)partition_name << "\"" );
    
    // is used to block the main thread. It will be unblocked by the Ctrl-C signal
    daq::ipc::signal::wait_for();
    
    hsm.stopGathering( 1 );
    ism.stopGathering( 1 );
                        
    return 0;
}

/*
    gatherer.cxx
    
    A simple utility which sums histograms.
    
    Serguei Kolos, Mar 2008
*/

#include <iostream>

#include <boost/thread.hpp>

#include <cmdl/cmdargs.h>

#include <ipc/core.h>
#include <ipc/signal.h>

#include <oh/OHSubscriber.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHRootProvider.h>

#include <ext/hash_map>

namespace __gnu_cxx
{
    template <> struct hash< std::string >
    {
	inline size_t operator() ( const std::string & x ) const
	{
	    return __stl_hash_string(x.c_str());
	}
    };
}

namespace gatherer
{
    template <class T>
    struct map : public __gnu_cxx::hash_map< std::string, T > {
	map( )
	{ ; }

	map( size_t n )
	  : __gnu_cxx::hash_map< std::string, T >( n )
	{ ; }
    };
}

class Semaphore
{
  public:
    Semaphore( int number, int timeout )
      : count_( 0 ),
        number_( number ),
        timeout_( timeout )
    { ; }
    
    int wait()
    {
        boost::mutex::scoped_lock lock( mutex_ );
        if( count_ < number_ )
        {
	    boost::xtime xt;
	    xtime_get( &xt, boost::TIME_UTC );
	    xt.sec += timeout_;
        
            condition_.timed_wait( lock, xt );
        }
        return ( number_ - count_ );
    }
    
    void post()
    {
        boost::mutex::scoped_lock lock( mutex_ );
        count_++;
        if ( count_ == number_ )
        {
            condition_.notify_one();
        }
    }
    
  private:
    int count_;
    int number_;
    int timeout_;
    boost::mutex mutex_;
    boost::condition condition_;
};

class SumHistogram : public OHRootProvider
{
  public:

    SumHistogram( const IPCPartition & partition, const std::string server_name, 
    		  const std::string & provider_name, const std::string & histogram_name )
      : OHRootProvider( partition, server_name, provider_name ),
        histogram_name_( histogram_name ),
        sum_( 0 ),
        stopped_( false ),
        semaphore_( 0 ),
        summed_providers_( 0 )
    { ; }
    
    void add( const std::string & provider, OHRootHistogram & hh )
    {
    	boost::mutex::scoped_lock lock( mutex_ );
    	gatherer::map<bool>::iterator it = providers_.find( provider );
        if ( it != providers_.end() )
        {
            if ( it->second )
            {
            	publish( );
            }
            it->second = true;
            ++summed_providers_;
        }
        else
        {
            providers_.insert( std::make_pair( provider, true ) );
        }
	
        if ( !sum_ )
        {
            sum_ = hh.histogram.release( );
        }
        else
        {
            sum_->Add( hh.histogram.get() );
        }
        
	if ( summed_providers_ == providers_.size() )
	{
	    publish();
	}
    }
    
    void stop( Semaphore & semaphore )
    {
        ERS_DEBUG( 1, "Stop command was received" );
        boost::mutex::scoped_lock lock( mutex_ );
        stopped_ = true;
    	semaphore_ = &semaphore;
    }
    
  private:
      
    void publish( )
    {
        OHRootProvider::publish( *sum_, histogram_name_ );

	for( gatherer::map<bool>::iterator ii = providers_.begin(); ii != providers_.end(); ++ii )
	{
	    ii->second = false;
	}
        summed_providers_ = 0;
        delete sum_;
        sum_ = 0;
        
        if ( stopped_ )
        {
	    semaphore_ -> post();
        }
    }

    std::string histogram_name_;
    boost::mutex mutex_;
    gatherer::map<bool> providers_;
    TH1 * sum_;
    bool stopped_;
    Semaphore * semaphore_;
    int summed_providers_;
};

class SumMaker : public OHRootReceiver
{
  public:

    SumMaker(	const IPCPartition & partition,
    		const std::string & server_name,
                const std::string & provider_name, 
                const std::vector<std::string> & servers,
                const OWLRegexp & providers,
                const OWLRegexp & histograms )
      : server_name_( server_name ),
        provider_name_( provider_name ),
        subscriber_( partition, *this )
    {
    	for ( size_t i = 0; i < servers.size(); ++i )
        {
	    try {
		subscriber_.subscribe( servers[i], providers, histograms );
	    }
	    catch ( daq::oh::Exception & ex ) {
		ers::error( ex );
	    }
	}
    }
    
    void stopGathering( int timeout )
    {
	ERS_LOG( "Stop command with the " << timeout << " seconds timeout has been received" );
        
        std::unique_ptr<Semaphore> semaphore;
        {
	    boost::mutex::scoped_lock lock( mutex_ );

	    semaphore.reset( new Semaphore( sums_.size(), timeout ) );
	    for ( gatherer::map<SumHistogram*>::iterator it = sums_.begin( ); it != sums_.end(); ++it )
	    {
		it->second->stop( *semaphore.get() );
	    }
        }
        
        int errors = semaphore->wait( );
        
	if ( errors )
        {
            ERS_LOG( "Stopping by the " << timeout << " seconds timeout, " << errors << " summs are incomplete" );
        }
        else
        {
            ERS_LOG( "Stopped cleanly" );
        }
    }
    
  private:
    void receive( OHRootHistogram & hh )
    {
    	std::string histogram = oh::util::get_object_name( hh.histogram->GetName() );
    	std::string provider = oh::util::get_provider_name( hh.histogram->GetName() );

    	boost::mutex::scoped_lock lock( mutex_ );
        gatherer::map<SumHistogram*>::iterator it = sums_.find( histogram );
        if ( it == sums_.end() )
        {
            it = (sums_.insert( std::make_pair( histogram, 
            	new SumHistogram( subscriber_.partition(), server_name_, provider_name_, histogram ) ) ) ).first;
        }
	it->second->add( provider, hh );
    }
    
  private:
    std::string	server_name_;
    std::string	provider_name_;
    boost::mutex mutex_;
    gatherer::map<SumHistogram*> sums_;
    OHSubscriber subscriber_;
};

int main( int argc, char ** argv )
{

    IPCCore::init( argc, argv );
        
    //
    // Get parameters from the command line
    //
    CmdArgStr	partition_name( 'p', "partition", "partition_name", "Partition name", CmdArg::isREQ );
    CmdArgStr	server_name( 's', "server", "server-name", "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr	provider_name( 'n', "provider", "provider-name", "Provider name", CmdArg::isREQ );
    
    CmdArgStrList	server_names( 'S', "servers", "[server-name ...]", "OH Server names", CmdArg::isREQ | CmdArg::isLIST );
    CmdArgStr		provider_names( 'P', "providers", "providers", "Regilar expression for the providers to be summed (Default is '.*')" );
    CmdArgStr		histogram_names( 'H', "histograms", "histograms", "Regilar expression for the histograms to be summed (Default is '.*')" );
    
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &server_names, &provider_names, &histogram_names, NULL );
        
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH Histogram display utility" );
    
    cmd.parse(arg_iter);
    
    //
    // Check command line parameters
    if( provider_names.isNULL( ) ) provider_names = ".*";
    if( histogram_names.isNULL( ) ) histogram_names = ".*";
    
    std::vector<std::string> servers;
    for ( size_t i=0; i < server_names.count(); i++ )
    {
    	servers.push_back( (const char*)server_names[i] );
    }
    
    SumMaker sm(	IPCPartition( (const char*)partition_name ),
    			(const char*)server_name,
    			(const char*)provider_name,
                        servers,
                        (const char*)provider_names,
                        (const char*)histogram_names );
                        
    ERS_LOG( "gatherer started in the partition \"" << (const char*)partition_name << "\"" );
    
    daq::ipc::signal::wait_for();

    sm.stopGathering( 1 );
                        
    return 0;
}
